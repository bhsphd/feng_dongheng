#include <iostream>
#include <math.h>
#include <ctime>
//Eigen 部分
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Geometry>
//#include <Eigen/Cholesky>
using namespace std;
using namespace Eigen;


int main(int argc, char *argv[])
{
    //1.定义
    Eigen::Matrix<float, 2, 3> matrix_23;  // 定义矩阵
    Eigen::Matrix<float, 3, 1> vd_3d; // 定义向量 Eigen::Matrix<double, 3,1>
    Vector3d v_3d;
    Matrix3d matrix_33 = Matrix3d::Zero(); //初始化为0矩阵
    MatrixXd matrix_x; //double ,动态矩阵
    //2.Eigen输入和输出
    matrix_23 << 1, 2, 3, 4, 5, 6; // 输入数据（初始化）
    cout << matrix_23;// 输出
    for(int i=0; i<2; i++){
        for(int j=0; j<3; j++){
            cout << matrix_23(i,j) << "\t";
        }
        cout << endl;
    }
    //３.矩阵运算
    v_3d << 4,5,6;
    vd_3d<< 3, 2, 1;
    // 同类型相乘
    Matrix<double, 2, 1> result = matrix_23.cast<double>() * v_3d;  //double*double
    cout << "result" << result;
    Matrix<float, 2, 1> result2 = matrix_23 * vd_3d; // float*float

    matrix_33 = Matrix3d::Random(); // 随机数矩阵
    cout << matrix_33 << endl;

    cout << matrix_33.transpose() << endl; //转置
    cout << matrix_33.sum() << endl; // 所有元素和
    cout << matrix_33.trace() << endl; // 迹
    cout << matrix_33.inverse() << endl; // 求逆
    cout << matrix_33.determinant() << endl; // 行列式
    // 4. 求矩阵征值(实对称矩阵)
    // 伴随矩阵求解器
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigen_solver(matrix_33.transpose()*matrix_33);
    cout << "Eigen values = \n" << eigen_solver.eigenvalues() << endl;
    cout << "Eigen vectors = \n" << eigen_solver.eigenvectors() << endl;

    // 5.解方程 A×x=b ：求逆运算量大，使用矩阵分解:QR
    // QR分解是将矩阵分解为一个正规正交矩阵与上三角矩阵乘积。Matlab 【Q R】=qr(A)
    Eigen::Matrix4d A;
    A << 2, -1, -1, 1,
            1, 1, -2, 1,
            4, -6, 2, -2,
            3, 6, -9, 7;
    Vector4d B(2, 4, 4, 9);
    Vector4d x = A.colPivHouseholderQr().solve(B); // QR分解法求解
    cout << "Qr:" << x << endl;


    // 6.
    Eigen::Matrix<double, 2, 1> vector(1,1);
    cout << "vector.x() :"<< vector.x() << endl << vector.y() << endl; // x表示第一个元素，y表示第二个元素
    cout << atan2(vector.x(), vector.y()) << endl;

    // 7.Rotation2D
      //using Rotation2D = Eigen::Rotation2D<float>;
//    Rotation2D=M_PI/4;
//    cout << Rotation2D.angle();
      Eigen::Rotation2D<double> Rotation2D(M_PI/4);
      cout << Rotation2D.angle() << endl;

      Eigen::Matrix<float, 2, 1> trans_ = Eigen::Matrix<float, 2, 1>::Identity();
      cout << trans_; //(1,0)







    cout << "Hello World!" << endl;
    return 0;
}
