cmake_minimum_required(VERSION 2.8)

project(g2o_test)
add_executable(${PROJECT_NAME} "main.cpp")
#message("project name:" ${PROJECT_NAME})

SET(CMAKE_CXX_FLAGS  "-std=c++11")
SET(CMAKE_BUILD_TYPE "Debug")
SET(CMAKE_CXX_COMPILER "g++")

#设定可执行二进制文件目录
SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#设定库文件编译目录
SET(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)
#将该目录作为链接目录
LINK_DIRECTORIES(${PROJECT_SOURCE_DIR}/lib)

#设定 .cmake文件路径
list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake_modules)

#包含G2O的头文件和库
find_package(G2O REQUIRED)
IF(G2O_FOUND)
    INCLUDE_DIRECTORIES(${G2O_INCLUDE_DIR})
    message("G2O lib is found:" ${G2O_INCLUDE_DIR})
ENDIF(G2O_FOUND)

#包含CSparse和Eigen库和头文件
find_package(Eigen3 REQUIRED)
find_package(CSparse REQUIRED)
INCLUDE_DIRECTORIES(${CSPARSE_INCLUDE_DIR})
INCLUDE_DIRECTORIES(${EIGEN3_INCLUDE_DIR})

#g2o_libs:变量，表示所有的库
SET(G2O_LIBS g2o_cli g2o_ext_freeglut_minimal g2o_simulator
    g2o_solver_slam2d_linear g2o_types_icp g2o_types_slam2d
    g2o_core g2o_interface g2o_solver_csparse g2o_solver_structure_only
    g2o_types_sba g2o_types_slam3d g2o_csparse_extension g2o_opengl_helper
    g2o_solver_dense g2o_stuff g2o_types_sclam2d g2o_parser
    g2o_solver_pcg g2o_types_data g2o_types_sim3 cxsparse)

#工程链接相应的库
target_link_libraries(${PROJECT_NAME} ${G2O_LIBS})
