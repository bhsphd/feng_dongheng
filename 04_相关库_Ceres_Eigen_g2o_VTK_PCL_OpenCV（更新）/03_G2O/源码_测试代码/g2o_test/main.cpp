#include <iostream>
#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/block_solver.h"
#include "g2o/core/factory.h"
#include "g2o/core/optimization_algorithm_levenberg.h"
#include "g2o/solvers/csparse/linear_solver_csparse.h"

#include "g2o/types/slam3d/vertex_se3.h" //d顶点
//#include "g2o/types/slam3d/edge_se3.h" //边
G2O_USE_TYPE_GROUP(slam3d) //

using namespace std;
using namespace g2o;
const int MAXITERATION = 50;

int main(int argc, char *argv[])
{
    cout << "hello g2o " << endl;
    //01_创建线性求解器     父类指针
    BlockSolverX::LinearSolverType* linearSolver =
            new LinearSolverCSparse<BlockSolverX::PoseMatrixType>();
    //01_在线性求解器的顶部创建块求解器
    BlockSolverX* blockSolver = new BlockSolverX(linearSolver); //线性求解器的顶部存在块求解器
    //01_创建优化算法优化
    OptimizationAlgorithmLevenberg* optimizationAlgorithm =
            new OptimizationAlgorithmLevenberg(blockSolver);

    //02_创建优化器
    SparseOptimizer optimizer;
    if(!optimizer.load("../data/sphere_bignoise_vertex3.g2o")){
        cout << "Error loading graph" << endl;
        return -1;
    }else{
        cout << "Loaded " << optimizer.vertices().size() << endl;  //输出顶点个数
        cout << "Loaded " << optimizer.edges().size() << endl; //输出边的条数
    }
    //03_优化
    // 固定第一条边：获取第1个顶点
    VertexSE3* firstRobotPose = dynamic_cast<VertexSE3*>(optimizer.vertex(0));
    firstRobotPose->setFixed(true);
    // 或者
    //optimizer.vertex(0)->setFixed(true);
    optimizer.setAlgorithm(optimizationAlgorithm);
    optimizer.setVerbose(true); //优化过程中的详细信息
    optimizer.initializeOptimization();
    cout << "Optimizing..." << endl;
    optimizer.optimize(MAXITERATION);
    cout << "done." << endl;
    optimizer.save("../data/sphere_after.g2o");
    cout << "Hello World!" << endl;
    return 0;
}

/*
 * 执行以上过程存在以下问题：
 * 1.默认的误差函数的形式是怎样的？误差如何存储为g2o格式
 * 2.块求解器是什么？
 * 3.迭代器初值是什么？如何设置？
*/














