#include "ceres/ceres.h"
#include "glog/logging.h"

using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;

//const T* const x// 01_不是数组，而是具体的一个数，可填多个
struct costFunctor1{
    template<typename T> bool operator()(const T* const x1,
                                         const T* const x2,
                                         T* residual) const { //03_ 必须是const
        residual[0] = x1[0] + T(10.) * x2[0]; //02_ residual都是0
        return true;
    }
};

struct costFunctor2{
    template<typename T> bool operator()(const T* const x3,
                                         const T* const x4,
                                         T* residual) const {
        residual[0] = T(std::sqrt(5)) * (x3[0] - x4[0]);//residual都是0
        return true;
    }
};


struct costFunctor3{
    template<typename T> bool operator()(const T* const x2,
                                         const T* const x3,
                                         T* residual) const{
        residual[0] = (x2[0] - T(2.)* x3[0]) * (x2[0] - T(2.)* x3[0]); //04_内部所有数值必须转化为T类型，如T(2.)
        return true;
    }
};

struct costFunctor4{
    template<typename T> bool operator()(const T* const x1,
                                         const T* const x4,
                                         T* residual) const {
        residual[0] = T(std::sqrt(10)) * (x1[0] - x4[0]) * (x1[0] - x4[0]);
        return true;
    }
};


int main(int argc, char** argv){

    double x1 = 3.0;
    double x2 = -1.0;
    double x3 = 0.0;
    double x4 = 1.0;

    //double x[4] = {x1, x2, x3, x4};

    Problem problem;

    CostFunction* costfunc1 = new AutoDiffCostFunction<costFunctor1, 1, 1, 1>(new costFunctor1); //
    CostFunction* costfunc2 = new AutoDiffCostFunction<costFunctor2, 1, 1, 1>(new costFunctor2);
    CostFunction* costfunc3 = new AutoDiffCostFunction<costFunctor3, 1, 1, 1>(new costFunctor3);
    CostFunction* costfunc4 = new AutoDiffCostFunction<costFunctor4, 1, 1, 1>(new costFunctor4);

    problem.AddResidualBlock(costfunc1, nullptr, &x1, &x2);
    problem.AddResidualBlock(costfunc2, nullptr, &x3, &x4);
    problem.AddResidualBlock(costfunc3, nullptr, &x2, &x3);
    problem.AddResidualBlock(costfunc4, nullptr, &x1, &x4);

    std::cout << "Initial x1 = " << x1
                << ", x2 = " << x2
                << ", x3 = " << x3
                << ", x4 = " << x4
                << "\n";

    //05_ Solver::Options将配置一些参数，配置如下：
    Solver::Options options;
    options.max_num_iterations = 100; // 最大迭代次数
    options.linear_solver_type = ceres::DENSE_QR; //QR分解
    options.minimizer_progress_to_stdout = true;

    Solver::Summary summary;
    Solve(options, &problem, &summary);

    std::cout << "Final x1 = " << x1
                << ", x2 = " << x2
                << ", x3 = " << x3
                << ", x4 = " << x4
                << "\n";
}
