#include <ros/ros.h>

int main(int argc, char ** argv)
{
	// 初始化ROS客户端：程序开始处调用一次该函数
	ros::init(argc, argv,"hello_ros"); 
	// 节点句柄，程序用于和ROS交互的主要机制：
	// 创建此对象将程序注册为ROS节点管理器的节点，程序中只创建一个NodeHandle对象
	ros::NodeHandle nh;
	ROS_INFO_STREAM("Hello, ROS!");
}
