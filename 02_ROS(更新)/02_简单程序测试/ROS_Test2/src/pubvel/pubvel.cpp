#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	ros::init(argc, argv, "publish_velocity");
	ros::NodeHandle nh; //注册为ROS节点管理器
	//创建发布者，发布消息类型：	geometry_msgs::Twist
	//向话题/turtle1/cmd_vel，消息序列大小为1000(消息数容纳量)
	ros::Publisher pub = nh.advertise<geometry_msgs::Twist>(
		"turtle1/cmd_vel", 1000);
	//随机数生成器
	srand(time(0));
	
	ros::Rate rate(2);
	while(ros::ok()){
		//如果节点运行良好
		geometry_msgs::Twist msg;   //Twist两个顶层域：linear和angular
		msg.linear.x = double(rand())/double(RAND_MAX);
		msg.angular.z = 2*double(rand())/double(RAND_MAX)-1;
		//其余值默认为0
		//发布消息
		pub.publish(msg);
		ROS_INFO_STREAM("Sending random velocity command:"
		<<"linear=" << msg.linear.x
		<< "angular=" << msg.angular.z);
		//延迟：阻止循环迭代速率超过指定速率
		rate.sleep();
		
	}
	



}
