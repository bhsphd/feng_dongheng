#include <ros/ros.h>
#include <turtlesim/Pose.h>
#include <iomanip> //for std::setprecision and std::fixed

//订阅程序
//回调函数：订阅者节点需要知道消息何时送达，将响应收到消息事件代码放在回调函数中
//收到一次消息，调用一次回调函数
void poseMessageReceived(const turtlesim::Pose& msg)
{
	ROS_INFO_STREAM(std::setprecision(2) << std::fixed
	<< "position=("<< msg.x << "," << msg.y << ")"
	<< "*direction=" << msg.theta);
}

int main(int argc, char** argv)
{
	//初始化ROS系统，建立节点实例
	ros::init(argc, argv, "subscribe_to_pose");
    //node_handle:节点句柄对象
	ros::NodeHandle nh;
	//创建ros::Subscriber对象
	//参数有三个：topic_name,queue_size,pointer_to_callback_function
	//topic_name:要订阅的话题名称
	//quene_size:订阅者接收消息的队列大小
	ros::Subscriber sub = nh.subscribe("turtle1/pose", 1000, &poseMessageReceived);
	//ros::spinOnce(); //ROS执行所有挂起的回调函数，将控制权返回
	//ros::ROS等待且执行回调函数，直到节点关机
	//while(ros::ok()){ros::spinOnce();}
	ros::spin();	
}
