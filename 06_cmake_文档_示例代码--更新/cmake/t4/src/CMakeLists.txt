AUX_SOURCE_DIRECTORY(. SRC_LIST)
LINK_DIRECTORIES(/home/fdh/文档/cmake_文档_示例代码/cmake/t3/lib_hello/build/lib) # 这行一定要在ADD_EXECUTABLE前面
ADD_EXECUTABLE(t4 ${SRC_LIST})
SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)

SET(CMAKE_PROJECT_DIR /home/fdh/文档/cmake_文档_示例代码/cmake) # cmake文件夹的位置
INCLUDE_DIRECTORIES(${CMAKE_PROJECT_DIR}/t3/lib_hello)
message("CMAKE_PROJECT_DIR" ${CMAKE_PROJECT_DIR}) #工程所在目录
TARGET_LINK_LIBRARIES(t4 hello) # 链接动态库
#TARGET_LINK_LIBRARIES(t4 libhello.a) # 链接静态库

#代码流程
#01 链接相应的库：提供库的目录
#02 增加可执行文件，并链接相应的目录
#03 设置可执行文件的输出目录
#04 设置cmake文件夹的位置
#05 包含相应的头文件
#06 链接动态库
