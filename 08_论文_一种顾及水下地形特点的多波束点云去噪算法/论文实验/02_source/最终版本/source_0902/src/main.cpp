﻿//==修改思路：
//==将所有保存过程全部舍掉，设置一个Hash用于存储<serchPointId, distance> : QHash<int, double>
//==使用数组进行存储

#include <pcl/kdtree/kdtree_flann.h>
#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <QDebug>
#include <QDateTime>
#include <vector>

#include "pcl/sample_consensus/ransac.h" //--------随机一致算法(Ransac)
#include "pcl/sample_consensus/sac_model_plane.h" //------随机一致模型(Ransac_model)
#include "PCL/sample_consensus/method_types.h"
#include "pcl/sample_consensus/model_types.h"
#include "pcl/segmentation/sac_segmentation.h"
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>
#include <QHash>

#include <math.h>
#define MAX_VALUE 0
#define PI 3.141593

/*
* 设置相关变量和参数
*/
double threshold = 2; //标准差倍数
int K=700; // 搜索点个数
double radius = 16;
QString _pcdFilePathInput = "../QDH_qifu.pcd"; // 文件路径
QString _pcdFilePathOutput = "../QDH_qifu_K_700_Stv1.5.pcd";
QString _pcdFilePathOutputFilter = "../QDH_qifu_Out_K_700_Stv1.5.pcd";
double ransac_threshold = 0.2; //==包含双边的阈值(不动)
int lessThan_Num_remove = 200; //==近邻域搜索点数小于这个数，则滤掉（不动）

double AngleThreshold = 45;//法矢量角度阈值
double distanceThreshold = 48;//法矢量距离阈值

QHash<int, int> Valid;
QHash<int, int> InnerFlag; //用于标记已经搜索过的内点
QHash<int, int> distanceInner;//标记陡坡处的点为内点


QHash<int, double> cloudPointFlag;//==用于保存距离

//==存储面片之间的法矢量关系==
typedef struct cloudPointSurfaceVector{
    //int surfaceID;  //面ID，用于拓扑关系
    double coefficient[4];  //存储矢量信息
    QHash<int, int> ID_pointID; //==<int, double>: <ID, 索引号>
}cloudPointSV;
QHash<int, cloudPointSV> _cloudPointSVHash;

struct XYZ{
    double x;
    double y;
    double z;
};

int countFlag = 0;
int q=0; //==统计面片数量

/*
* 辅助函数：
* 获取系统时间
* 显示预览点云
*/

//---获取系统时间---
void getCurrentTime(QString &str)
{
    QDateTime time = QDateTime::currentDateTime();
    str = time.toString("yyyy-MM-dd hh:mm:ss ddd");
}

//---显示预览点云--
boost::shared_ptr<pcl::visualization::PCLVisualizer>
simpleVis (pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud)
{
    // --------------------------------------------
    // -----Open 3D viewer and add point cloud-----
    // --------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->setBackgroundColor (0, 0, 0);
    viewer->addPointCloud<pcl::PointXYZ> (cloud, "sample cloud");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    //viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();
    return (viewer);
}

int main(int argc, char *argv[])
{
    // -------------------------输出当前时间----------
    QString str = "";
    getCurrentTime(str);
    qDebug() << "before InputCloud:" << str << "\n";

    /*---------------------------- step1:输入点云，创建kdtree--------------------------*/
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PCDReader reader;
    reader.read(_pcdFilePathInput.toStdString(), *cloud); // 点云读入cloud
    std::cout << *cloud << std::endl; // 打印输入点云的头部信息
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree; // 构建kdtree对象
    kdtree.setInputCloud(cloud); //------建立kd-tree索引

    /*----------------------------step2:使用FLANN函数对搜索点进行K近邻搜索----------------*/

    //---待搜索点---
    pcl::PointXYZ searchPoint;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointInner(new pcl::PointCloud<pcl::PointXYZ>); // 用于保存内点
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointOuter(new pcl::PointCloud<pcl::PointXYZ>); // 用于保存外点

    int m=0; //标记内点和外点数目
    int n=0;

    cloudPointInner->points.resize(cloud->size()*2);
    cloudPointOuter->points.resize(cloud->size()*2);

    for(int searchPointId = 0; searchPointId < cloud->size(); ++searchPointId)
    {
        searchPoint.x = cloud->points[searchPointId].x;
        searchPoint.y = cloud->points[searchPointId].y;
        searchPoint.z = cloud->points[searchPointId].z;

        //==判断searchPoint是否为有效点
        if(searchPoint.x == MAX_VALUE || searchPoint.y == MAX_VALUE || searchPoint.z == MAX_VALUE){
            continue;
        }
        if(InnerFlag.contains(searchPointId)) //==判断searchPoint是否被搜索过==
        {
            continue;
        }

        //==存储近邻域搜索到的点(有效点)，用来拟合平面
        pcl::PointCloud<pcl::PointXYZ>::Ptr kdtreeSearchedPoint(new pcl::PointCloud<pcl::PointXYZ>);
        //==使用FLANN函数进行近邻域搜索
        std::vector<int> pointIndex; // 存储索引号
        std::vector<float> pointSquaredDiatance; // 存储距离

        //==FLANN函数对Kd_tree近邻域半径搜索，最大值为K
        //kdtree.nearestKSearch(searchPoint, K, pointIndex, pointSquaredDiatance);//---保存搜索到点的索引号和对应的距离
        kdtree.radiusSearch(searchPoint, radius, pointIndex, pointSquaredDiatance, K); //半径radius，最多搜索K个点 0 is query point
        int K_ID = pointIndex.size(); //==搜索到点的索引号大小
        /*TODO：
* 改进：
*    搜索点数小于20，则默认是外点，当然如果数量小于100，使用ransac算法效果较差：
* 如何权衡点的密度与ransac算法需要的点的数量之间的关系。
*/
        if(K_ID < lessThan_Num_remove) // 问题：默认为外点，是否合适？？
        {
            for(int i=0; i<K_ID; ++i)
            {
                // K_ID小于50，此时kdtreeSearchedPoint中包含已经保存过的点
                if(!InnerFlag.contains(Valid.value(i))){

                    cloud->points[Valid.value(i)].x = MAX_VALUE;
                    cloud->points[Valid.value(i)].y = MAX_VALUE;
                    cloud->points[Valid.value(i)].z = MAX_VALUE;
                    n += 1;
                    cloudPointFlag.insert(Valid.value(i), 0);
                }
            }
            countFlag += 1;
            //Valid.clear();
            continue;
        }

        //==若K_ID大于等于10， 则用搜索到的点，填充kdtreeSearchedPoint---
        kdtreeSearchedPoint->width = K; //去除无效点之后：resize
        kdtreeSearchedPoint->height = 1;
        kdtreeSearchedPoint->is_dense = false; //内应当为true
        kdtreeSearchedPoint->points.resize(kdtreeSearchedPoint->width * kdtreeSearchedPoint->height);

        int K_ID_Flag = 0; //标记K_ID中的有效点个数
        //K_ID_Flag += 1;
        for(int i=0; i<K_ID; ++i) //==修改
        {
            if(cloud->points[pointIndex[i]].x != MAX_VALUE && cloud->points[pointIndex[i]].y != MAX_VALUE
                    && cloud->points[pointIndex[i]].z != MAX_VALUE)
            {
                kdtreeSearchedPoint->points[K_ID_Flag].x = cloud->points[pointIndex[i]].x;
                kdtreeSearchedPoint->points[K_ID_Flag].y = cloud->points[pointIndex[i]].y;
                kdtreeSearchedPoint->points[K_ID_Flag].z = cloud->points[pointIndex[i]].z;// 将搜索到的有效点保存至kdtreeSearchedPoint中

                Valid.insert(K_ID_Flag, pointIndex[i]); //K_ID_Flag=0, 对应相应的cloudPoint

                int _innerFlag = 0;
                InnerFlag.insert(pointIndex[i], _innerFlag); //==相同则更新_innerFlag
                _innerFlag += 1;

                K_ID_Flag += 1; // 标记K_ID中的有效点个数
            }
        }
        K_ID = K_ID_Flag; //标记搜索到的点（去除无效点） // 有效点个数
        kdtreeSearchedPoint->points.resize(K_ID);
        std::cout << "K_ID:" << K_ID << std::endl;

        //==点数小于阈值，无法计算拟合平面参数==
        if(K_ID < 4) // 问题：默认为外点，是否合适？？
        {
            for(int i=0; i<K_ID; ++i)
            {
                // K_ID小于50，此时kdtreeSearchedPoint中包含已经保存过的点
                if(!InnerFlag.contains(Valid.value(i))){

                    cloud->points[Valid.value(i)].x = MAX_VALUE;
                    cloud->points[Valid.value(i)].y = MAX_VALUE;
                    cloud->points[Valid.value(i)].z = MAX_VALUE;
                    n += 1;
                    cloudPointFlag.insert(Valid.value(i), 0);
                }
            }
            countFlag += 1;
            Valid.clear();
            continue;
        }

        /*------------------------- step3:建立ransac算法模型和对象，生成拟合平面----------------*/
        //---实例化ransac算法模型和对象---
        pcl::SampleConsensusModelPlane<pcl::PointXYZ>::Ptr
                model_p (new pcl::SampleConsensusModelPlane<pcl::PointXYZ> (kdtreeSearchedPoint));
        pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(model_p);
        //---算法流程
        ransac.setDistanceThreshold(ransac_threshold); // 设置阈值
        ransac.computeModel(); // 计算模型参数 ----速度较慢
        Eigen::VectorXf model_coefficient(4);
        ransac.getModelCoefficients(model_coefficient); //获取模型参数
        /*------------------------- step4:计算点到平面的距离-----------------------*/
        double a, b, c, d;
        a = model_coefficient[0];
        b = model_coefficient[1];
        c = model_coefficient[2];
        d = model_coefficient[3];
        std::vector<double> distancePointToPlane(K_ID); // 用于存储距离
        double A = sqrt(a*a + b*b + c*c);
        for(int i=0; i<K_ID; ++i)
        {
            distancePointToPlane[i] =
                    fabs((kdtreeSearchedPoint->points[i].x) * a + (kdtreeSearchedPoint->points[i].y) * b + (kdtreeSearchedPoint->points[i].z) * c + d);
            distancePointToPlane[i] = distancePointToPlane[i] / A ;
            //==有效点的距离==
            cloudPointFlag.insert(Valid.value(i), distancePointToPlane[i]);
        }
        /*------------------------- step5:用于保存面与面之间的拓扑关系:OK-----------------------*/
        cloudPointSV _cloudPointSV;
        _cloudPointSV.coefficient[0] = a;
        _cloudPointSV.coefficient[1] = b;
        _cloudPointSV.coefficient[2] = c;
        _cloudPointSV.coefficient[3] = d;
        _cloudPointSV.ID_pointID = Valid;//copy OK
        _cloudPointSVHash.insert(q, _cloudPointSV); //0 is first surface
        q++;
        Valid.clear();
    }

    /*--------------------------- step5: 计算均值和标准差----------------------*/
    //==删掉不合适的面和对应的点
    //---计算均值---
    double sum = 0.0; // 求和
    //double squareSum = 0.0; // 求平方和
    double sq_sum = 0.0;
    double mean = 0.0; // 记录均值
    double Stv = 0.0; // 记录标准差
    double variance = 0.0; //记录方差

    int cloudPointNoZeroCount = 0;
    for(int j=0; j<cloud->size(); ++j)
    {
        if(cloudPointFlag.value(j) == 0)//==distance=0.skip
        {
            continue;
        }
        sum += cloudPointFlag.value(j);
        sq_sum += cloudPointFlag.value(j)*cloudPointFlag.value(j);
        cloudPointNoZeroCount++;
    }
    mean = sum / (cloudPointNoZeroCount);
    //---计算方差---
    variance = sq_sum - sum*sum/cloudPointNoZeroCount;
    Stv = sqrt(variance/(cloudPointNoZeroCount-1));

    /*-------------------- step6: 判断searchPoint是否在指定标准差范围内----------*/

    double limitMin = 0.0; // 阈值下限
    double limitMax = 0.0; // 阈值上限

    limitMin = mean - threshold * Stv;
    limitMax = mean + threshold * Stv;//
    std::cout << "limitMax:" << limitMax << std::endl;

    /*-------------------- step6: 法矢量预判是否是离群面-----------------*/
    //增加法矢量预判
    //==计算角度Angel和distance，标记离群面（删掉）和陡坡（保留）
    for(int i=0; i<q-1; ++i)
    {
        //==计算角度（计算无误：达到想要的效果）
        double Angle = 0.0;
        double a, b, c, d, a1, b1, c1, d1;
        a = _cloudPointSVHash.value(i).coefficient[0];
        b = _cloudPointSVHash.value(i).coefficient[1];
        c = _cloudPointSVHash.value(i).coefficient[2];
        d = _cloudPointSVHash.value(i).coefficient[3];

        a1 = _cloudPointSVHash.value(i+1).coefficient[0];
        b1 = _cloudPointSVHash.value(i+1).coefficient[1];
        c1 = _cloudPointSVHash.value(i+1).coefficient[2];
        d1 = _cloudPointSVHash.value(i+1).coefficient[3];

        double inner_product = a*a1 + b*b1 + c*c1;//内积
        Angle = acos(inner_product);
        Angle = Angle/PI*180;
        if(Angle > 90){
            Angle = 180-Angle;
        }
        //==计算距离
        double distancePToP = 0;
        XYZ xyz[2];
        xyz[0].x = cloud->points[_cloudPointSVHash.value(i).ID_pointID.value(0)].x;
        xyz[0].y = cloud->points[_cloudPointSVHash.value(i).ID_pointID.value(0)].y;
        //xyz[0].z = cloud->points[_cloudPointSVHash.value(i).ID_pointID.value(0)].z;
        xyz[1].x = cloud->points[_cloudPointSVHash.value(i+1).ID_pointID.value(0)].x;
        xyz[1].y = cloud->points[_cloudPointSVHash.value(i+1).ID_pointID.value(0)].y;
        //xyz[1].z = cloud->points[_cloudPointSVHash.value(i+1).ID_pointID.value(0)].z;

        xyz[0].z = (-d-(a*xyz[0].x + b*xyz[0].y))/c;
        xyz[1].z = (-d1-(a1*xyz[1].x + b1*xyz[1].y))/c1;

//        distancePToP = (xyz[0].x-xyz[1].x)*(xyz[0].x-xyz[1].x) + (xyz[0].y-xyz[1].y)*(xyz[0].y-xyz[1].y) +
//                (xyz[0].z-xyz[1].z)*(xyz[0].z-xyz[1].z);
//        distancePToP = sqrt(distancePToP);

        //==计算投影取最大者
        double distance1 = 0.0;
        double distance2 = 0.0;
        distance1 = fabs(a*(xyz[0].x-xyz[1].x) + b*(xyz[0].y-xyz[1].y) + c*(xyz[0].z-xyz[1].z));
        distance2 = fabs(a*(xyz[0].x-xyz[1].x) + b*(xyz[0].y-xyz[1].y) + c*(xyz[0].z-xyz[1].z));
        if(distance1 > distance2){
            distancePToP = distance1;
        }else{
            distancePToP = distance2;
        }

        if(Angle > AngleThreshold)
        {
            //==外点
            if(distancePToP > distanceThreshold){

                if(_cloudPointSVHash.value(i).ID_pointID.size() > _cloudPointSVHash.value(i+1).ID_pointID.size()){
                    for(int j=0; j<_cloudPointSVHash.value(i+1).ID_pointID.size(); ++j){
                        cloudPointFlag.insert(_cloudPointSVHash.value(i+1).ID_pointID.value(j), 0);
                    }
                }else{
                    for(int j=0; j<_cloudPointSVHash.value(i).ID_pointID.size(); ++j){
                        cloudPointFlag.insert(_cloudPointSVHash.value(i).ID_pointID.value(j), 0);
                    }
                }
            }else{
                int distanceFlag = 0;
                for(int j=0; j<_cloudPointSVHash.value(i+1).ID_pointID.size(); ++j){
                    distanceInner.insert(_cloudPointSVHash.value(i+1).ID_pointID.value(j), distanceFlag);//插入索引号
                    distanceFlag++;
                }
            }
            i += 1;
        }
        //std::cout << "i:" << i << std::endl;
    }
    /*--------------------------------------------------------------------------*/
    for(int i=0; i<cloud->size(); ++i)
    {
        if(cloudPointFlag.value(i) == 0)
        {
            cloudPointOuter->points[n].x = cloud->points[i].x;
            cloudPointOuter->points[n].y = cloud->points[i].y;
            cloudPointOuter->points[n].z = cloud->points[i].z;
            n += 1;
            continue;
        }
        //==添加==
        if(distanceInner.contains(i)){
            //==当前点为陡坡点（保存为内点）
            cloudPointInner->points[m].x = cloud->points[i].x;
            cloudPointInner->points[m].y  = cloud->points[i].y;
            cloudPointInner->points[m].z  = cloud->points[i].z;
            m += 1;
            continue;
        }
        //==添加===
        if(cloudPointFlag.value(i) > limitMin && cloudPointFlag.value(i) < limitMax)
        {
            //==判断当前点，是否已经写入cloudPointInner
            cloudPointInner->points[m].x = cloud->points[i].x;
            cloudPointInner->points[m].y  = cloud->points[i].y;
            cloudPointInner->points[m].z  = cloud->points[i].z;
            m += 1;

        }else{
            //==searchPoint进行搜索时，有可能导致已经确认过的内点，再次确认为外点
            cloudPointOuter->points[n].x = cloud->points[i].x;
            cloudPointOuter->points[n].y = cloud->points[i].y;
            cloudPointOuter->points[n].z = cloud->points[i].z;
            n += 1;
        }
    }

    std::cout << "m:" << m << endl;
    std::cout << "n:" << n << endl;

    cloudPointInner->width = m;
    cloudPointInner->height = 1;
    cloudPointInner->is_dense = false;
    cloudPointInner->points.resize(m);

    cloudPointOuter->width = n;
    cloudPointOuter->height = 1;
    cloudPointOuter->is_dense = false;
    cloudPointOuter->points.resize(n);

    /*------------------------- step7: 将点云写入文件----------------*/
    pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> (_pcdFilePathOutput.toStdString(), *cloudPointInner, true);
    writer.write<pcl::PointXYZ> (_pcdFilePathOutputFilter.toStdString(), *cloudPointOuter, true);

    // -------------------------输出当前时间----------
    getCurrentTime(str);
    qDebug() << "After InputCloud:" << str << "\n";

    /*------------------------- step: 使用VTK库显示点云----------------*/
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
    viewer = simpleVis(cloudPointInner);
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }

    return 0;
}
