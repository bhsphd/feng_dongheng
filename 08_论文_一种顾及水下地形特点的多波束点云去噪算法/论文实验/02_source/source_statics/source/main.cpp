﻿//#include <QCoreApplication>


#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <QDateTime>
#include <QDebug>

//---获取系统时间---
void getCurrentTime(QString &str)
{
    QDateTime time = QDateTime::currentDateTime();
    str = time.toString("yyyy-MM-dd hh:mm:ss ddd");
    //qDebug() << str;
}

int main (int argc, char** argv)
{
    // -------------------------输出当前时间----------
    QString str = "";
    getCurrentTime(str);
    qDebug() << "before InputCloud:" << str << "\n";

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
    // 填入点云数据
    pcl::PCDReader reader;
    // 把路径改为自己存放文件的路径
    reader.read<pcl::PointXYZ> ("../GQD_QPS - Cloud.pcd", *cloud);
    std::cout << "Cloud before filtering: " << std::endl;
    std::cout << *cloud << std::endl;
    // 创建滤波器对象
    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
    sor.setInputCloud (cloud);
    sor.setMeanK (50);
    sor.setStddevMulThresh (2.0);
    sor.filter (*cloud_filtered);
    std::cerr << "Cloud after filtering: " << std::endl;
    std::cerr << *cloud_filtered << std::endl;
    pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> ("../GQD_QPS - Cloud_Statistic50.pcd", *cloud_filtered, true);
    sor.setNegative (true);
    sor.filter (*cloud_filtered);
    writer.write<pcl::PointXYZ> ("../GQD_QPS - Cloud_Statistic50_filtered.pcd", *cloud_filtered, true);

    // -------------------------输出当前时间----------
    str = "";
    getCurrentTime(str);
    qDebug() << "before InputCloud:" << str << "\n";
    return (0);
}
