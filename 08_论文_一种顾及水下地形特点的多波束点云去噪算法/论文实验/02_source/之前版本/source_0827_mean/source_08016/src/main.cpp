﻿
//==此方法的时间复杂度太高，无法再短期内输出结果
//==考虑之前的设计方案，对已经搜索过的点，不再进行重复搜索，不同的是，将距离加入Hash表进行计算，而非在函数内计算

#include <pcl/kdtree/kdtree_flann.h>
#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <QDebug>
#include <QDateTime>
#include <vector>

#include "pcl/sample_consensus/ransac.h" //--------随机一致算法(Ransac)
#include "pcl/sample_consensus/sac_model_plane.h" //------随机一致模型(Ransac_model)
#include "PCL/sample_consensus/method_types.h"
#include "pcl/sample_consensus/model_types.h"
#include "pcl/segmentation/sac_segmentation.h"
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>
#include <QHash>

#include <math.h>
#define MAX_VALUE 0

/*
* 设置相关变量和参数
*/
double threshold = 1.5; //标准差倍数
int K=100; // 搜索点个数
//double radius = 10;
QString _pcdFilePathInput = "../QDH_qifu.pcd"; // 文件路径
QString _pcdFilePathOutput = "../QDH_qifu_K_700_Stv1.5.pcd";
QString _pcdFilePathOutputFilter = "../QDH_qifu_Out_K_700_Stv1.5.pcd";
double ransac_threshold = 0.2;
QHash<int, int> Valid;

/*
* 辅助函数：
* 获取系统时间
* 显示预览点云
*/
//==问题（27）修改：标记为内点，但是下次使用ransac算法时，继续使用

//---获取系统时间---
void getCurrentTime(QString &str)
{
    QDateTime time = QDateTime::currentDateTime();
    str = time.toString("yyyy-MM-dd hh:mm:ss ddd");
    //qDebug() << str;
}
int q=0;

//---显示预览点云--
boost::shared_ptr<pcl::visualization::PCLVisualizer>
simpleVis (pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud)
{
    // --------------------------------------------
    // -----Open 3D viewer and add point cloud-----
    // --------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->setBackgroundColor (0, 0, 0);
    viewer->addPointCloud<pcl::PointXYZ> (cloud, "sample cloud");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    //viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();
    return (viewer);
}

int main(int argc, char *argv[])
{
    /*---------------------------- step1:输入点云，创建kdtree--------------------------*/
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PCDReader reader;
    reader.read(_pcdFilePathInput.toStdString(), *cloud); // 点云读入cloud
    std::cout << *cloud << std::endl; // 打印输入点云的头部信息
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree; // 构建kdtree对象
    kdtree.setInputCloud(cloud); //------建立kd-tree索引

    /*----------------------------step2:使用FLANN函数对搜索点进行K近邻搜索----------------*/
    // -------------------------输出当前时间----------
    QString str = "";
    getCurrentTime(str);
    qDebug() << "before InputCloud:" << str << "\n";

    //---待搜索点---
    pcl::PointXYZ searchPoint;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointInner(new pcl::PointCloud<pcl::PointXYZ>); // 用于保存内点
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointOuter(new pcl::PointCloud<pcl::PointXYZ>); // 用于保存外点

    int m=0; //标记内点和外点数目
    int n=0;

    cloudPointInner->points.resize(cloud->size());
    cloudPointOuter->points.resize(cloud->size());

    for(int searchPointId = 0; searchPointId < cloud->size(); ++searchPointId)
    {
        searchPoint.x = cloud->points[searchPointId].x;
        searchPoint.y = cloud->points[searchPointId].y;
        searchPoint.z = cloud->points[searchPointId].z;

        //==存储近邻域搜索到的点(有效点)，用来拟合平面
        pcl::PointCloud<pcl::PointXYZ>::Ptr kdtreeSearchedPoint(new pcl::PointCloud<pcl::PointXYZ>);
        //==使用FLANN函数进行近邻域搜索
        std::vector<int> pointIndex; // 存储索引号
        std::vector<float> pointSquaredDiatance; // 存储距离

        kdtree.nearestKSearch(searchPoint, K, pointIndex, pointSquaredDiatance);//---保存搜索到点的索引号和对应的距离
        //kdtree.radiusSearch(searchPoint, radius, pointIndex, pointSquaredDiatance, K); //半径radius，最多搜索K个点 0 is query point
        int K_ID = pointIndex.size(); //==搜索到点的索引号大小

        kdtreeSearchedPoint->width = K_ID; //去除无效点之后：
        kdtreeSearchedPoint->height = 1;
        kdtreeSearchedPoint->is_dense = false;
        kdtreeSearchedPoint->points.resize(kdtreeSearchedPoint->width * kdtreeSearchedPoint->height);

        int K_ID_Flag = 0;
        for(int i=0; i<K_ID; ++i)
        {
                kdtreeSearchedPoint->points[K_ID_Flag].x = cloud->points[pointIndex[i]].x;
                kdtreeSearchedPoint->points[K_ID_Flag].y = cloud->points[pointIndex[i]].y;
                kdtreeSearchedPoint->points[K_ID_Flag].z = cloud->points[pointIndex[i]].z;// 将搜索到的有效点保存至kdtreeSearchedPoint中
        }        
        //kdtreeSearchedPoint->points.resize(K_ID);

        std::cout << "K_ID:" << K_ID << endl;

        if(K_ID < 4)
        {
            Valid.insert(searchPointId, 0);
            continue;
        }
        /*------------------------- step3:建立ransac算法模型和对象，生成拟合平面----------------*/
        //---实例化ransac算法模型和对象---
        pcl::SampleConsensusModelPlane<pcl::PointXYZ>::Ptr
                model_p (new pcl::SampleConsensusModelPlane<pcl::PointXYZ> (kdtreeSearchedPoint));
        pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(model_p);
        //---算法流程
        ransac.setDistanceThreshold(ransac_threshold); // 设置阈值
        ransac.computeModel(); // 计算模型参数 ----速度较慢
        Eigen::VectorXf model_coefficient(4);
        ransac.getModelCoefficients(model_coefficient); //获取模型参数
        /*------------------------- step4:计算点到平面的距离-----------------------*/
        double a, b, c, d;
        a = model_coefficient[0];
        b = model_coefficient[1];
        c = model_coefficient[2];
        d = model_coefficient[3];
        std::vector<double> distancePointToPlane(K_ID); // 用于存储距离
        double A = sqrt(a*a + b*b + c*c);
        for(int i=0; i<K_ID; ++i)
        {
            distancePointToPlane[i] =
                    fabs((kdtreeSearchedPoint->points[i].x) * a + (kdtreeSearchedPoint->points[i].y) * b + (kdtreeSearchedPoint->points[i].z) * c + d);
            distancePointToPlane[i] = distancePointToPlane[i] / A ;
        }

        double sum1 = 0.0; // 求和
        double mean1 = 0.0; // 记录均值
        for(int j=0; j<distancePointToPlane.size(); ++j){
            sum1 += distancePointToPlane[j];
        }
        mean1 = sum1 / (distancePointToPlane.size());

        Valid.insert(searchPointId, mean1);
    }


        /*--------------------------- step5: 计算均值和标准差----------------------*/
        //---计算均值---
        double sum = 0.0; // 求和
        double sq_sum = 0.0;
        double mean = 0.0; // 记录均值
        double Stv = 0.0; // 记录标准差
        double variance = 0.0; //记录方差
        //size_t dataSize = value.size();
        for(int i=0; i<cloud->size(); ++i){
            sum += Valid.value(i);
            sq_sum += Valid.value(i) * Valid.value(i);
        }
        mean = sum / cloud->size();
        variance = sq_sum - sum*sum/cloud->size();
        Stv = sqrt(variance/(cloud->size()-1));

        /*-------------------- step6: 判断searchPoint是否在指定标准差范围内----------*/

        double limitMin = 0.0; // 阈值下限
        double limitMax = 0.0; // 阈值上限

        limitMin = mean - threshold * Stv;
        limitMax = mean + threshold * Stv;
        for(int i=0; i<cloud->size(); ++i)
        {
            if(Valid.value(i)>limitMin && Valid.value(i) < limitMax)
            {
                cloudPointInner->points[m].x = cloud->points[i].x;
                cloudPointInner->points[m].y  = cloud->points[i].y;
                cloudPointInner->points[m].z  = cloud->points[i].z;
                m += 1;
            }

            else{

                cloudPointOuter->points[n].x = cloud->points[i].x;
                cloudPointOuter->points[n].y = cloud->points[i].y;
                cloudPointOuter->points[n].z = cloud->points[i].z;
                n += 1;
            }
        }

    std::cout << "m:" << m << endl;
    std::cout << "n:" << n << endl;
    cloudPointInner->width = m;
    cloudPointInner->height = 1;
    cloudPointInner->is_dense = false;
    cloudPointInner->points.resize(m);

    cloudPointOuter->width = n;
    cloudPointOuter->height = 1;
    cloudPointOuter->is_dense = false;
    cloudPointOuter->points.resize(n);

    /*------------------------- step7: 将点云写入文件----------------*/
    pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> (_pcdFilePathOutput.toStdString(), *cloudPointInner, true);
    writer.write<pcl::PointXYZ> (_pcdFilePathOutputFilter.toStdString(), *cloudPointOuter, true);

    // -------------------------输出当前时间----------
    //QString str = "";
    getCurrentTime(str);
    qDebug() << "After InputCloud:" << str << "\n";

    /*------------------------- step: 使用VTK库显示点云----------------*/
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
    viewer = simpleVis(cloudPointInner);
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }

    return 0;
}
