﻿#ifndef RANSAC_FILTER_H
#define RANSAC_FILTER_H

#include <QObject>
//#include <QCoreApplication>
#include <pcl/kdtree/kdtree_flann.h>
#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <QDebug>
#include <QDateTime>
#include <vector>

#include "pcl/sample_consensus/ransac.h" //--------随机一致算法(Ransac)
#include "pcl/sample_consensus/sac_model_plane.h" //------随机一致模型(Ransac_model)
#include "PCL/sample_consensus/method_types.h"
#include "pcl/sample_consensus/model_types.h"
#include "pcl/segmentation/sac_segmentation.h"
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>

#include <math.h>

class Ransac_Filter : public QObject
{
    Q_OBJECT

public:
    explicit Ransac_Filter(QObject *parent = 0);

public:
    void getCurrentTime(QString &str); // 显示当前系统时间

    boost::shared_ptr<pcl::visualization::PCLVisualizer>
    simpleVis (pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud); // 显示点云

    void createKdtree(const QString _pcdFilePathInput,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud); // 创建kdtree索引

    //--设定规则，从Cloud中选择searchPoint--seachPoint执行FLANNSearchPoint函数和剩下的三个函数。

    //--使用FLANN函数近邻域搜索--保存搜索到的点--
    void FLANNSearchPoint(const pcl::PointXYZ searchPoint_Point,
                          pcl::PointCloud<pcl::PointXYZ>::Ptr &kdtreeSearchedPoint);

    //--使用Ransac算法--对搜索到的点--拟合平面--输出拟合参数
    Eigen::VectorXf RansacPlaneFitting(const pcl::PointCloud<pcl::PointXYZ>::Ptr &kdtreeSearchedPoint);

    //--对搜索到的点云进行滤波--并保存内外点--
    void RansacFilter(Eigen::VectorXf model_coefficient_Ransac,
                      const pcl::PointCloud<pcl::PointXYZ>::Ptr &kdtreeSearchedPoint,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr &cloudPointInner_Ransac,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr &cloudPointOuter_Ransac);
    //--重置cloudPointInner和cloudPointOuter并输出到点云文件
    void writeToPCDFile(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloudPointInner_Write,
                        pcl::PointCloud<pcl::PointXYZ>::Ptr &cloudPointOuter_Write,
                        QString &_pcdFilePathInlier_Write,
                        QString &_pcdFilePathOuter_Write);

signals:

private slots:

private:
   pcl::PointCloud<pcl::PointXYZ>::Ptr cloud;
   double threshold = 1; //标准差倍数
   int K=500; // 搜索点个数
   QString _pcdFilePathInput = "../QDH_qifu.pcd"; // 文件路径
   QString _pcdFilePathInlier = "../QDH_qifu_Inlier_100_Stv1.pcd";
   QString _pcdFilePathOuter = "../QDH_qifu_Outer_100_Stv1.pcd";
   pcl::PointXYZ searchPoint;  // 存储当前搜索点信息
   pcl::PointCloud<pcl::PointXYZ>::Ptr kdtreeSearchedPoint;// 用于保存搜索到的点
   pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointInner; //用于存储内点
   pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointOuter; //用于存储外点

   pcl::KdTreeFLANN<pcl::PointXYZ> kdtree; // 构建kdtree对象
   Eigen::VectorXf model_coefficient; //用于存储拟合平面模型参数

   unsigned int m=0; // 记录inlier和outlier点云数目
   unsigned int n=0;
};

#endif // RANSAC_FILTER_H
