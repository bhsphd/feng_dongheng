﻿#include "Ransac_Filter.h"

Ransac_Filter::Ransac_Filter(QObject *parent)
    : QObject(parent)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);// 创建点云对象
    pcl::PointCloud<pcl::PointXYZ>::Ptr kdtreeSearchedPoint(new pcl::PointCloud<pcl::PointXYZ>); // 保存搜索到的点
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointInner(new pcl::PointCloud<pcl::PointXYZ>); // 保存内点
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointOuter(new pcl::PointCloud<pcl::PointXYZ>); // 保存外点
    createKdtree(_pcdFilePathInput, cloud); //01 cloud保存kdtree索引
    //--预设cloudPointInner和cloudPointOuter大小
    cloudPointInner->points.resize(cloud->size());
    cloudPointOuter->points.resize(cloud->size());

    //todo --how to select searchPoint
    FLANNSearchPoint(searchPoint, kdtreeSearchedPoint); //02 执行结束后，kdtreeSearchedPoint存储搜索到的点云信息(K+1)

    model_coefficient = RansacPlaneFitting(kdtreeSearchedPoint); // 03 执行结束后，返回拟合模型参数

    RansacFilter(model_coefficient, kdtreeSearchedPoint,
                 cloudPointInner, cloudPointOuter); // 04 对点云区分内点和外点

    writeToPCDFile(cloudPointInner, cloudPointOuter, _pcdFilePathInlier, _pcdFilePathOuter);

}

void Ransac_Filter::getCurrentTime(QString &str)
{
    QDateTime time = QDateTime::currentDateTime();
    str = time.toString("yyyy-MM-dd hh:mm:ss ddd");
}

boost::shared_ptr<pcl::visualization::PCLVisualizer> Ransac_Filter::simpleVis(pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud)
{
    // --------------------------------------------
    // -----Open 3D viewer and add point cloud-----
    // --------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->setBackgroundColor (0, 0, 0);
    viewer->addPointCloud<pcl::PointXYZ> (cloud, "sample cloud");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    //viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();
    return (viewer);
}

void Ransac_Filter::createKdtree(const QString _pcdFilePathInput,
                                 pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud)
{
    pcl::PCDReader reader;
    reader.read(_pcdFilePathInput.toStdString(), *cloud); // 点云读入cloud
    std::cout << *cloud << std::endl; // 打印输入点云的头部信息
    kdtree.setInputCloud(cloud); //------建立kd-tree索引
}

//--使用FLANN函数近邻域搜索--保存搜索到的点--
/*/--searchPoint.x = cloud->points[searchPointId].x;
searchPoint.y = cloud->points[searchPointId].y;
searchPoint.z = cloud->points[searchPointId].z;
*/
void Ransac_Filter::FLANNSearchPoint(const pcl::PointXYZ searchPoint_Point,
                                     pcl::PointCloud<pcl::PointXYZ>::Ptr &kdtreeSearchedPoint)
{
    //---使用FLANN函数进行近邻域搜索---
    std::vector<int> pointIndex(K); // 存储索引
    std::vector<float> pointSquaredDiatance(K); // 存储距离
    //---用搜索到的点，填充kdtreeSearchedPoint---
    kdtreeSearchedPoint->width = K + 1;
    kdtreeSearchedPoint->height = 1;
    kdtreeSearchedPoint->is_dense = false;
    kdtreeSearchedPoint->points.resize(kdtreeSearchedPoint->width * kdtreeSearchedPoint->height);
    //---FLANN函数对Kd_tree进行近邻域搜索，搜索K个点---
    kdtree.nearestKSearch(searchPoint_Point, K, pointIndex, pointSquaredDiatance);//---保存搜索到点的索引号和对应的距离
    for(int i=0; i<pointIndex.size(); ++i) //-----根据cloud的索引号输出相应的坐标值
    {
        //---用搜索到的点填充kdtreeSearchedPoint---
        kdtreeSearchedPoint->points[i].x = cloud->points[pointIndex[i]].x;
        kdtreeSearchedPoint->points[i].y = cloud->points[pointIndex[i]].y;
        kdtreeSearchedPoint->points[i].z = cloud->points[pointIndex[i]].z;
    }
    //---添加待搜索点---
    kdtreeSearchedPoint->points[K].x = searchPoint_Point.x;
    kdtreeSearchedPoint->points[K].y = searchPoint_Point.y;
    kdtreeSearchedPoint->points[K].z = searchPoint_Point.z;
}

//--使用Ransac算法--对搜索到的点--拟合平面--输出拟合参数
Eigen::VectorXf Ransac_Filter::RansacPlaneFitting(const pcl::PointCloud<pcl::PointXYZ>::Ptr &kdtreeSearchedPoint)
{
    pcl::SampleConsensusModelPlane<pcl::PointXYZ>::Ptr
            model_p (new pcl::SampleConsensusModelPlane<pcl::PointXYZ> (kdtreeSearchedPoint));
    pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(model_p);
    //---算法流程
    ransac.setDistanceThreshold(0.05); // 设置阈值
    ransac.computeModel(); // 计算模型参数 ----速度较慢
    Eigen::VectorXf model_coefficient_F(4);
    ransac.getModelCoefficients(model_coefficient_F); //获取模型参数
    return model_coefficient_F;
}

void Ransac_Filter::RansacFilter(Eigen::VectorXf model_coefficient_Ransac,
                                 const pcl::PointCloud<pcl::PointXYZ>::Ptr &kdtreeSearchedPoint,
                                 pcl::PointCloud<pcl::PointXYZ>::Ptr &cloudPointInner_Ransac,
                                 pcl::PointCloud<pcl::PointXYZ>::Ptr &cloudPointOuter_Ransac)
{
    /*------------------------- 计算点到平面的距离 ----------------*/
    double a, b, c, d;
    a = model_coefficient_Ransac[0];
    b = model_coefficient_Ransac[1];
    c = model_coefficient_Ransac[2];
    d = model_coefficient_Ransac[3];
    std::vector<double> distancePointToPlane(kdtreeSearchedPoint->size()); // 用于存储点to平面的距离
    double A = sqrt(a*a + b*b + c*c);
    for(unsigned int i=0; i<kdtreeSearchedPoint->size(); ++i)
    {
        distancePointToPlane[i] =
                fabs((kdtreeSearchedPoint->points[i].x) * a + (kdtreeSearchedPoint->points[i].y) * b + (kdtreeSearchedPoint->points[i].z) * c - d);
        distancePointToPlane[i] = distancePointToPlane[i] / A ;
    }

    /*/------------------------- 计算均值和标准差---------------------*/
    double sum = 0.0; // 求和
    double squareSum = 0.0; // 求平方和
    double mean = 0.0; // 记录均值
    double Stv = 0.0; // 记录标准差
    //size_t dataSize = value.size();
    for(unsigned int j=0; j<distancePointToPlane.size(); ++j){
        sum += distancePointToPlane[j];
    }
    mean = sum / (K+1);
    // compute Std
    for(unsigned int k=0; k<distancePointToPlane.size(); ++k)
    {
        squareSum += (distancePointToPlane[k]-mean)*(distancePointToPlane[k]-mean);
    }
    Stv = sqrt(squareSum/(K+1));

    /*-------------------- 判断searchPoint是否在指定标准差范围内（改进）---------- */
/*改进方向：
* 判断500个点是否在指定标准差范围内，如果是，则标记为内点，如果否，则标记为外点
* 优势：可以在大幅度上提高程序的执行效率；
* 缺点：认定内外点的方式相比第一种方式差；
*/
    double limitMin = 0.0; // 阈值下限
    double limitMax = 0.0; // 阈值上限
    // int k=0;
//    unsigned int m=0;
//    unsigned int n=0;
    limitMin = mean - threshold * Stv;
    limitMax = mean + threshold * Stv;
    for(int i=0; i<K+1; ++i)
    {
        //...todo....分别保存内点和外点...
        if(distancePointToPlane[i] >limitMin && distancePointToPlane[i] < limitMax){
            cloudPointInner_Ransac->points[m].x = searchPoint.x;
            cloudPointInner_Ransac->points[m].y  = searchPoint.y;
            cloudPointInner_Ransac->points[m].z  = searchPoint.z;
            m += 1;
        }else{
            cloudPointOuter_Ransac->points[n].x = searchPoint.x;
            cloudPointOuter_Ransac->points[n].y  = searchPoint.y;
            cloudPointOuter_Ransac->points[n].z  = searchPoint.z;
            n += 1;
        }
    }
}

void Ransac_Filter::writeToPCDFile(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloudPointInner_Write,
                                   pcl::PointCloud<pcl::PointXYZ>::Ptr &cloudPointOuter_Write,
                                   QString &_pcdFilePathInlier_Write,
                                   QString &_pcdFilePathOuter_Write)
{
    cloudPointInner_Write->width = m;
    cloudPointInner_Write->height = 1;
    cloudPointInner_Write->is_dense = false;
    cloudPointInner_Write->points.resize(m);

    cloudPointOuter_Write->width = n;
    cloudPointOuter_Write->height = 1;
    cloudPointOuter_Write->is_dense = false;
    cloudPointOuter_Write->points.resize(n);

    /*------------------------- step7: 将点云写入文件----------------*/
    pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> (_pcdFilePathInlier_Write.toStdString(), *cloudPointInner_Write, true);
    writer.write<pcl::PointXYZ> (_pcdFilePathOuter_Write.toStdString(), *cloudPointOuter_Write, true);
}
