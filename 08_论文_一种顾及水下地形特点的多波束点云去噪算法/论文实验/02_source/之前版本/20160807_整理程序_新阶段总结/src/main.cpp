﻿//#include <QCoreApplication>
#include <pcl/kdtree/kdtree_flann.h>
#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <QDebug>
#include <QDateTime>
#include <vector>

#include "pcl/sample_consensus/ransac.h" //--------随机一致算法(Ransac)
#include "pcl/sample_consensus/sac_model_plane.h" //------随机一致模型(Ransac_model)
#include "PCL/sample_consensus/method_types.h"
#include "pcl/sample_consensus/model_types.h"
#include "pcl/segmentation/sac_segmentation.h"
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>

#include <math.h>
#define MAX_VALUE 0

/*
 * 设置相关变量和参数
*/
double threshold = 1; //标准差倍数
int K=500; // 搜索点个数
double radius = 5;
QString _pcdFilePathInput = "../QDH_qifu.pcd"; // 文件路径
QString _pcdFilePathOutput = "../QDH_qifu_K_500_Stv1.pcd";
QString _pcdFilePathOutputFilter = "../QDH_qifu_Out_K_500_Stv1.pcd";
double ransac_threshold = 0.4;

/*
 * 辅助函数：
 * 获取系统时间
 * 显示点云
*/

//---获取系统时间---
void getCurrentTime(QString &str)
{
    QDateTime time = QDateTime::currentDateTime();
    str = time.toString("yyyy-MM-dd hh:mm:ss ddd");
    //qDebug() << str;
}
int q=0;
// 显示点云

boost::shared_ptr<pcl::visualization::PCLVisualizer>
simpleVis (pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud)
{
    // --------------------------------------------
    // -----Open 3D viewer and add point cloud-----
    // --------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->setBackgroundColor (0, 0, 0);
    viewer->addPointCloud<pcl::PointXYZ> (cloud, "sample cloud");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    //viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();
    return (viewer);
}

/*问题（2）
 * 保存的内点，存在标记的点，造成内点无法预览，无法看到效果
 */

/*问题（3）
 * 半径搜索，会造成数据结果发生很多空洞；
 * 考虑进行点数搜索
 */

/*问题（4）
 * 点数搜索必然面临一系列问题：
 * 比如，每次固定寻找500个点，需要严格控制已经标记的点，防止标记的点进入内点
 */

/*问题（5）
 * 标记searchedPoint
 */

int main(int argc, char *argv[])
{
    /*---------------------------- step1:输入点云，创建kdtree--------------------------*/
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
//    QString _pcdFilePath = "../fenduan01.pcd"; // 文件路径
    pcl::PCDReader reader;
    reader.read(_pcdFilePathInput.toStdString(), *cloud); // 点云读入cloud
    std::cout << *cloud << std::endl; // 打印输入点云的头部信息
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree; // 构建kdtree对象
    kdtree.setInputCloud(cloud); //------建立kd-tree索引

    /*----------------------------step2:使用FLANN函数对搜索点进行K近邻搜索----------------*/
    // -------------------------输出当前时间----------
    QString str = "";
    getCurrentTime(str);
    qDebug() << "before InputCloud:" << str << "\n";

    //---待搜索点---
    pcl::PointXYZ searchPoint;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointInner(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointOuter(new pcl::PointCloud<pcl::PointXYZ>);

    int m=0; //标记内点和外点
    int n=0;

    cloudPointInner->points.resize(cloud->size());
    cloudPointOuter->points.resize(cloud->size());
    //std::cout << cloud->size();
    for(int searchPointId = 0; searchPointId < cloud->size(); ++searchPointId)
    {
        //        searchPoint.x = 4942200.0;
        //        searchPoint.y = 8926899.0;
        //        searchPoint.z = 157200.1;

        //int searchPointId = 0;//
        searchPoint.x = cloud->points[searchPointId].x;
        searchPoint.y = cloud->points[searchPointId].y;
        searchPoint.z = cloud->points[searchPointId].z;


        cloud->points[searchPointId].x = MAX_VALUE;
        cloud->points[searchPointId].y = MAX_VALUE;
        cloud->points[searchPointId].z = MAX_VALUE;

/*----------------------------------------------------------------------------------------------------------------*/
        /*----------------------------------------------------------------------------------------------------------------*/
        if(searchPoint.x == MAX_VALUE && searchPoint.y == MAX_VALUE && searchPoint.z == MAX_VALUE){
            continue;
        }
        //如果searchPoint无效，跳出本次循环，重新确定searchPoint

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//        std:: cout << "searchPoint.x:" << searchPoint.x << endl
//                   << "searchPoint.y:" << searchPoint.y << endl
//                   << "searchPoint.z" << searchPoint.z << endl;
        /*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

        //---存储近邻域搜索到的点
        pcl::PointCloud<pcl::PointXYZ>::Ptr kdtreeSearchedPoint(new pcl::PointCloud<pcl::PointXYZ>);
        //---使用FLANN函数进行近邻域搜索---
        std::vector<int> pointIndex; // 存储索引
        std::vector<float> pointSquaredDiatance; // 存储距离

        //---FLANN函数对Kd_tree进行近邻域搜索，搜索K个点---
        //kdtree.nearestKSearch(searchPoint, K, pointIndex, pointSquaredDiatance);//---保存搜索到点的索引号和对应的距离

        kdtree.radiusSearch(searchPoint, radius, pointIndex, pointSquaredDiatance, K); //半径radius，最多搜索K个点
        //具体kdtree源码未知，根据点云的值建立好索引，所以搜索到的点可能是已经标记过的点
        //（由于并未重新建立kdtree索引，因而，不再对这些点进行判断）
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        //std::cout << "pointIndex.size():" << pointIndex.size() << endl;
        //std::cout << "pointSquaredDiatance.size():" << pointSquaredDiatance.size() << endl;
        int K_ID = pointIndex.size();
        //kdtreeSearchedPoint->points.resize(K_ID+1);
        //std::cout << "K_ID:" << K_ID << endl;
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        // 由于搜索到的点既有内点，也有外点，同时存在已经标记过的点，所以需要对是否标记过的点进行判断，做到不增不减
 // 问题（5）
 // 如果K_ID小于50，则默认搜索到的点为内点，但是，下次搜索的时候，存在以下可能：
 /*
  * 虽然控制住searchPoint和搜索到的点都不包含标记的点，但是，
  * 对于数目小于50的点，却没有控制，导致下次有可能搜索到，做法：
  * 同样对这些点进行标记，下次将搜索不到。

*/
/*TODO：
 * 问题(6):
 * 搜索到点数小于50，则默认为是内点，跳出本次循环，将会导致Outer出现一个洞一个洞的情形
 * ？？？searchPoint认为是内点，是否合适
 * 改进：
 *    搜索点数小于20，则默认是外点，当然如果数量小于100，使用ransac算法效果较差：
 * 如何权衡点的密度与ransac算法需要的点的数量之间的关系。
*/
        if(K_ID < 50) //判断：搜索到的点数小于一定数目，则默认为是外点，跳出本次循环（本来就稀少）
        {
            for(int i=0; i<K_ID; ++i){
                if(cloud->points[pointIndex[i]].x != MAX_VALUE && cloud->points[pointIndex[i]].y != MAX_VALUE
                        && cloud->points[pointIndex[i]].z != MAX_VALUE)
                {
                    cloudPointOuter->points[n].x = cloud->points[pointIndex[i]].x;
                    cloudPointOuter->points[n].y = cloud->points[pointIndex[i]].y;
                    cloudPointOuter->points[n].z = cloud->points[pointIndex[i]].z;

                    //标记确认过的点
                    cloud->points[pointIndex[i]].x = MAX_VALUE;
                    cloud->points[pointIndex[i]].y = MAX_VALUE;
                    cloud->points[pointIndex[i]].z = MAX_VALUE; //保存后，将有效点标记为无效点
                    n += 1;
                }
            }
            //==标记serachedPoint？？为外点
            cloudPointOuter->points[n].x = searchPoint.x;
            cloudPointOuter->points[n].y = searchPoint.y;
            cloudPointOuter->points[n].z = searchPoint.z;
            //==标记searchPoint为无效点（已经标记）
            n += 1;
            continue;
        }

        //--若K_ID大于等于20， 则用搜索到的点，填充kdtreeSearchedPoint---
        kdtreeSearchedPoint->width = K + 1; //去除无效点之后：resize
        kdtreeSearchedPoint->height = 1;
        kdtreeSearchedPoint->is_dense = false;
        kdtreeSearchedPoint->points.resize(kdtreeSearchedPoint->width * kdtreeSearchedPoint->height);


        int K_ID_Flag = 0;//标记K_ID中的有效点个数
        //K_ID_Flag += 1;
        for(int i=0; i<K_ID; ++i)
        {   //保存检索到的有效点至kdtreeSearchedPoint中
            if(cloud->points[pointIndex[i]].x != MAX_VALUE && cloud->points[pointIndex[i]].y != MAX_VALUE
                    && cloud->points[pointIndex[i]].z != MAX_VALUE)
            {
                kdtreeSearchedPoint->points[K_ID_Flag].x = cloud->points[pointIndex[i]].x;
                kdtreeSearchedPoint->points[K_ID_Flag].y = cloud->points[pointIndex[i]].y;
                kdtreeSearchedPoint->points[K_ID_Flag].z = cloud->points[pointIndex[i]].z;// 将搜索到的有效点保存至kdtreeSearchedPoint中
                K_ID_Flag += 1; // 标记K_ID中的有效点个数

                //标记确认过的点
                cloud->points[pointIndex[i]].x = MAX_VALUE;
                cloud->points[pointIndex[i]].y = MAX_VALUE;
                cloud->points[pointIndex[i]].z = MAX_VALUE; //搜索过的点标记为无效点标记为无效点
            }
        }
        K_ID = K_ID_Flag; //标记搜索到的点（去除无效点） // 有效点个数
        //==保存searchPoint到kdtreeSearchedPoint（均为有效点）

        kdtreeSearchedPoint->points[K_ID].x = searchPoint.x; // 均为有效点
        kdtreeSearchedPoint->points[K_ID].y = searchPoint.y;
        kdtreeSearchedPoint->points[K_ID].z = searchPoint.z;
        kdtreeSearchedPoint->points.resize(K_ID + 1);

        std::cout << "K_ID:" << K_ID << endl; // K_ID: 搜索到的有效点个数（不包含searchPoint）

        //(1)问题：K_ID>=50，但是无效点的个数可能接近于K_ID，这样将会导致kdtreeSearchedPoint点数减少，从而无法计算ransac的模型参数
        // 修正思路：如果点数小于值  10 ，则默认这点点为内点，结束本次循环，此处判断kdtreeSearchedPoint计算拟合参数是否有效
        //--
 //--问题6：需要处理searchedPoint
        if(K_ID < 10) // 点云比较密集，但是有效的点云比较稀疏 //17次
        {
            for(int i=0; i<K_ID + 1; ++i)
            {
                cloudPointOuter->points[n].x = kdtreeSearchedPoint->points[i].x;
                cloudPointOuter->points[n].y  = kdtreeSearchedPoint->points[i].y;
                cloudPointOuter->points[n].z  = kdtreeSearchedPoint->points[i].z;

                //==标记为外点（已经标记）
                n += 1;
            }
            continue;
        }

        /*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/


        /*------------------------- step3:建立ransac算法模型和对象，生成拟合平面----------------*/
        //---实例化ransac算法模型和对象---
        pcl::SampleConsensusModelPlane<pcl::PointXYZ>::Ptr
                model_p (new pcl::SampleConsensusModelPlane<pcl::PointXYZ> (kdtreeSearchedPoint));
        pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(model_p);
        //---算法流程
        ransac.setDistanceThreshold(ransac_threshold); // 设置阈值
        ransac.computeModel(); // 计算模型参数 ----速度较慢
        Eigen::VectorXf model_coefficient(4);
        ransac.getModelCoefficients(model_coefficient); //获取模型参数
        //    std::cout << "model_coefficient:" << model_coefficient[0] << std::endl
        //              << model_coefficient[1] << std::endl
        //              << model_coefficient[2] << std::endl
        //              << model_coefficient[3] << std::endl; //打印模型参数

        /*------------------------- step4:计算点到平面的距离----------------*/
        //model_coefficient[0] * model_coefficient[0];
        double a, b, c, d;
        a = model_coefficient[0];
        b = model_coefficient[1];
        c = model_coefficient[2];
        d = model_coefficient[3];
        std::vector<double> distancePointToPlane(K_ID + 1); // 用于存储距离
        //double A = sqrt(a*a + b*b + c*c);
        for(unsigned int i=0; i<K_ID+1; ++i)
        {
            distancePointToPlane[i] =
                    fabs((kdtreeSearchedPoint->points[i].x) * a + (kdtreeSearchedPoint->points[i].y) * b + (kdtreeSearchedPoint->points[i].z) * c - d);
            //distancePointToPlane[i] = distancePointToPlane[i] / A ;
        }
//        std::cout << "Ransac: a:" << a << std::endl
//                  << "b:" << b << std::endl
//                  << "c: " << c << std::endl
//                  << "d:" << d << std::endl;



        //    std::vector<double>::iterator it = distancePointToPlane.begin();
        //    for(;it!=distancePointToPlane.end();++it){
        //        std::cout << "distance:" <<*it << std::endl;
        //    }
        /*/------------------------- step5: 计算均值和标准差---------------------*/
        // compute mean
        double sum = 0.0; // 求和
        double squareSum = 0.0; // 求平方和
        double mean = 0.0; // 记录均值
        double Stv = 0.0; // 记录标准差
        //size_t dataSize = value.size();
        for(unsigned int j=0; j<distancePointToPlane.size(); ++j){
            sum += distancePointToPlane[j];
        }
        mean = sum / (K_ID+1);
        // compute Std
        for(int i=0; i<distancePointToPlane.size(); ++i)
        {
            squareSum += (distancePointToPlane[i]-mean)*(distancePointToPlane[i]-mean);
        }
        Stv = sqrt(squareSum/(K_ID+1));


//        cout << Stv << endl
//             << mean << endl;
        //    std::cout << "mean:" << mean << std::endl
        //              << "stv:" << Stv << std::endl;

        /*-------------------- step6: 判断searchPoint是否在指定标准差范围内----------*/
        //std::vector<int> allInliers;
        //std::vector<int> allOutliers;

        double limitMin = 0.0; // 阈值下限
        double limitMax = 0.0; // 阈值上限
        // int k=0;
        limitMin = mean - threshold * Stv;
        limitMax = mean + threshold * Stv;

/*----------------------------------------------------------------------------------------------------------------*/
        /*----------------------------------------------------------------------------------------------------------------*/
/*
 * vector超限：out of vector
 * 原因分析：
 * 遍历下一个点时，同样会搜索K个点，虽然跳过searchPoint，但是导致会重复保存很多点
 * 这些点的坐标为（0，0，0）
 *
 *
*/
        //std::cout << "distancePointToPlane.size():" << distancePointToPlane.size() << endl; //正确 K_ID+1
            for(int i=0; i<distancePointToPlane.size(); ++i)
            {
                if(distancePointToPlane[i] >limitMin && distancePointToPlane[i] < limitMax)
                {
                    cloudPointInner->points[m].x = kdtreeSearchedPoint->points[i].x;
                    cloudPointInner->points[m].y  = kdtreeSearchedPoint->points[i].y;
                    cloudPointInner->points[m].z  = kdtreeSearchedPoint->points[i].z;
                    m += 1;
                    //==已经标记为外点
                    //std::cout << "m:" << m << endl;
                }else{
                        cloudPointOuter->points[n].x = kdtreeSearchedPoint->points[i].x;
                        cloudPointOuter->points[n].y  = kdtreeSearchedPoint->points[i].y;
                        cloudPointOuter->points[n].z  = kdtreeSearchedPoint->points[i].z;
                        n += 1;
                }
                    //std::cout << "n:" << n << endl;
            }


        /*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

//        q+=1;
//        std::cout << q << std::endl;
    }
    std::cout << "m:" << m << endl;
    std::cout << "n:" << n << endl;
    cloudPointInner->width = m;
    cloudPointInner->height = 1;
    cloudPointInner->is_dense = false;
    cloudPointInner->points.resize(m);

    cloudPointOuter->width = n;
    cloudPointOuter->height = 1;
    cloudPointOuter->is_dense = false;
    cloudPointOuter->points.resize(n);

    //==滤掉000
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointInner_Last(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointOuter_Last(new pcl::PointCloud<pcl::PointXYZ>);
    cloudPointInner_Last->width = m;
    cloudPointInner_Last->height = 1;
    cloudPointInner_Last->is_dense = false;
    cloudPointInner_Last->points.resize(m);

    int cloudPointInner_Last_Flag = 0;
    int cloudPointOuter_Last_Flag = 0;

    cloudPointOuter_Last->width = n;
    cloudPointOuter_Last->height = 1;
    cloudPointOuter_Last->is_dense = false;
    cloudPointOuter_Last->points.resize(n);
//==TODO：注意，此处一定要使用cloudPointInner_Last_Flag标记点，否则将导致中间点用（0，0，0）填充
    //==判断是否等于0貌似没有效果 问题（7）
    for(int i=0; i<m; ++i)
    {
        if(cloudPointInner->points[i].x != MAX_VALUE && cloudPointInner->points[i].y != MAX_VALUE
                && cloudPointInner->points[i].z != MAX_VALUE)
        {
            cloudPointInner_Last->points[cloudPointInner_Last_Flag].x = cloudPointInner->points[i].x;
            cloudPointInner_Last->points[cloudPointInner_Last_Flag].y = cloudPointInner->points[i].y;
            cloudPointInner_Last->points[cloudPointInner_Last_Flag].z = cloudPointInner->points[i].z;
        }
        cloudPointInner_Last_Flag += 1;
    }
    cloudPointInner_Last->points.resize(cloudPointInner_Last_Flag);
    std::cout << "cloudPointInner_Last_Flag:" << cloudPointInner_Last_Flag << endl;

    for(int i=0; i<n; ++i)
    {
        if(cloudPointOuter->points[i].x != MAX_VALUE && cloudPointOuter->points[i].y != MAX_VALUE
                && cloudPointOuter->points[i].z != MAX_VALUE)
        {
            cloudPointOuter_Last->points[cloudPointOuter_Last_Flag].x = cloudPointOuter->points[i].x;
            cloudPointOuter_Last->points[cloudPointOuter_Last_Flag].y = cloudPointOuter->points[i].y;
            cloudPointOuter_Last->points[cloudPointOuter_Last_Flag].z = cloudPointOuter->points[i].z;
        }
        cloudPointOuter_Last_Flag += 1;
    }
    cloudPointOuter_Last->points.resize(cloudPointOuter_Last_Flag);
    std::cout << "cloudPointOuter_Last_Flag:" << cloudPointOuter_Last_Flag << endl;


    /*------------------------- step7: 将点云写入文件----------------*/
    pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> (_pcdFilePathOutput.toStdString(), *cloudPointInner_Last, true);
    writer.write<pcl::PointXYZ> (_pcdFilePathOutputFilter.toStdString(), *cloudPointOuter_Last, true);

    // -------------------------输出当前时间----------
    //QString str = "";
    getCurrentTime(str);
    qDebug() << "After InputCloud:" << str << "\n";

    /*------------------------- step: 使用VTK库显示点云----------------*/
    //std::vector<int> inliers2;
    //ransac.getInliers(inliers2); //获取内点索引
    //pcl::PointCloud<pcl::PointXYZ>::Ptr final (new pcl::PointCloud<pcl::PointXYZ>);
    //copy all inliers to final(索引号)
    //pcl::copyPointCloud<pcl::PointXYZ>(*kdtreeSearchedPoint, inliers2, *final);

    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
    viewer = simpleVis(cloudPointInner);
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }

    return 0;
}
