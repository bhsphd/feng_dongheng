﻿//#include <QCoreApplication>
#include <pcl/kdtree/kdtree_flann.h>

#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <QDebug>
#include <QDateTime>
#include <vector>

#include "pcl/sample_consensus/ransac.h" //--------随机一致算法(Ransac)
#include "pcl/sample_consensus/sac_model_plane.h" //------随机一致模型(Ransac_model)
#include "PCL/sample_consensus/method_types.h"
#include "pcl/sample_consensus/model_types.h"
#include "pcl/segmentation/sac_segmentation.h"
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>

// 显示点云
boost::shared_ptr<pcl::visualization::PCLVisualizer>
simpleVis (pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud)
{
  // --------------------------------------------
  // -----Open 3D viewer and add point cloud-----
  // --------------------------------------------
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->setBackgroundColor (0, 0, 0);
  viewer->addPointCloud<pcl::PointXYZ> (cloud, "sample cloud");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
  //viewer->addCoordinateSystem (1.0);
  viewer->initCameraParameters ();
  return (viewer);
}

// 使用ransac算法 对搜索到的点拟合平面 计算拟合平面参数
// 使用ransac算法，对搜索到的点拟合平面，并计算点到平面的距离了，去除距离超过一倍方差的点。

// 计算搜索到的点到ransac拟合平面之间的距离
std::vector<double> pointToRansacPlaneDistance(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud,
                                Eigen::VectorXf &model_coefficients)
{
    std::vector<double> distancePointToPlane;
    distancePointToPlane.resize(cloud->points.size());
    double a, b, c, d;
    a = model_coefficients[0];
    b = model_coefficients[1];
    c = model_coefficients[2];
    d = model_coefficients[3];
    double A = sqrt(a*a + b*b + c*c);
    for(unsigned int i=0; i<cloud->size(); ++i)
    {
        distancePointToPlane[i] =
                abs(cloud->points[i].x * a + cloud->points[i].y * b + cloud->points[i].z * c + d);
        distancePointToPlane[i] = distancePointToPlane[i] / A ;
    }
    return distancePointToPlane;
}

// 计算标准方差
double computeStd(std::vector<double> &value)
{
    // compute mean
    double sum = 0.0;
    double squareSum = 0.0;
    double mean = 0.0;
    double Std = 0.0;
    size_t dataSize = value.size();
    for(unsigned int i=0; i<dataSize; ++i){
        sum += value[i];
    }
    mean = sum / dataSize;
    // compute Std
    for(unsigned int j=0; j<dataSize; ++j)
    {
        squareSum += (value[j]-mean)*(value[j]-mean);
    }
    Std = sqrt(squareSum/dataSize);

    // 遍历所有距离，标记距离小于一倍标准差的点
    return Std;
 }

// 去除一倍方差以外的点云
void filterCloudPoint(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud,
                      Eigen::VectorXf &model_coefficients)
{
    std::vector<double> distancePointToPlane;
    distancePointToPlane = pointToRansacPlaneDistance(cloud, model_coefficients);
    double Std = computeStd(distancePointToPlane);

    if();
}


using namespace std;
int main(int argc, char *argv[])
{
    //QCoreApplication a(argc, argv);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr kdtreeSearchedPoint(new pcl::PointCloud<pcl::PointXYZ>);
    //-------用于存储FLANN函数近邻域搜索到的点
    QString _pcdFilePath = "../fenduan0Cloud002.pcd";
    pcl::PCDReader reader;
    reader.read(_pcdFilePath.toStdString(), *cloud);
    std::cout << *cloud << std::endl;
// 创建kdtree
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
    kdtree.setInputCloud(cloud); //------说明：已经建立kd-tree索引

    pcl::PointXYZ searchPoint;
    searchPoint.x = 4942200.0;
    searchPoint.y = 8926899.0;
    searchPoint.z = 157200.1;
// 使用FLANN函数进行近邻域搜索
    int K=500;
    std::vector<int> pointIndex(K);
    std::vector<float> pointSquaredDiatance(K);
    std::vector<int> inliers; //---用于存储ransac算法得到的局内点索引---
//--------用搜索到的点，填充kdtreeSearchedPoint------
    kdtreeSearchedPoint->width = K;
    kdtreeSearchedPoint->height = 1;
    kdtreeSearchedPoint->is_dense = false;
    kdtreeSearchedPoint->points.resize(kdtreeSearchedPoint->width * kdtreeSearchedPoint->height);
//--------FLANN函数对Kd_tree进行近邻域搜索，搜索K个点----------------
    kdtree.nearestKSearch(searchPoint, K, pointIndex, pointSquaredDiatance);//---保存搜索到点的索引号和对应的距离
    for(int i=0; i<pointIndex.size(); ++i) //-----根据cloud的索引号输出相应的坐标值
    {
        std::cout << " " << cloud->points[pointIndex[i]].x
                  << " " << cloud->points[pointIndex[i]].y
                  << " " << cloud->points[pointIndex[i]].z << std::endl
        << "squred distance:" << pointSquaredDiatance[i] << std::endl;
//--------把Kd_tree搜索到的K个点，存至kdtreeSearchedPoint中-------------
        kdtreeSearchedPoint->points[i].x = cloud->points[pointIndex[i]].x;
        kdtreeSearchedPoint->points[i].y = cloud->points[pointIndex[i]].y;
        kdtreeSearchedPoint->points[i].z = cloud->points[pointIndex[i]].z;

        std::cout << std::endl << "存储点云" << std::endl
                  << kdtreeSearchedPoint->points[i].x << " "
                  << kdtreeSearchedPoint->points[i].y << " "
                  << kdtreeSearchedPoint->points[i].z << std::endl;
    }

//-----------将model输入到ransac算法中----------
    std::vector<int> inliers2;
    pcl::SampleConsensusModelPlane<pcl::PointXYZ>::Ptr
            model_p (new pcl::SampleConsensusModelPlane<pcl::PointXYZ> (kdtreeSearchedPoint));
//-----------------------step: 构建ModelPlane和RandomSample对象
 //   3parts:(1)输入Threshold (2)计算model (3)
    pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(model_p);
    ransac.setDistanceThreshold (5);
    ransac.computeModel();

    Eigen::VectorXf model_coefficient;
    ransac.getModelCoefficients(model_coefficient); //获得模型参数
    std::cout << "model_coefficient:" << model_coefficient[0] << std::endl
              << model_coefficient[1] << std::endl
              << model_coefficient[2] << std::endl
              << model_coefficient[3] << std::endl;
    ransac.getInliers(inliers2); //存储内点


    std::cout << inliers2.size();
    std::cout << kdtreeSearchedPoint->size();
    // copies all inliers of the model computed to another PointCloud
/*---------------------step：显示测试点云------------------*/
    pcl::PointCloud<pcl::PointXYZ>::Ptr final (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::copyPointCloud<pcl::PointXYZ>(*kdtreeSearchedPoint, inliers2, *final);

    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
    viewer = simpleVis(kdtreeSearchedPoint);
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }


    //------------tester-------------

    return 0;
}
