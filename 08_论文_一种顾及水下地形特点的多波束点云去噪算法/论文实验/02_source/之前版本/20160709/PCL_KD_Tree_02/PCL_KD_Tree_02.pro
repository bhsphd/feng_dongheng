#-------------------------------------------------
#
# Project created by QtCreator 2016-06-30T16:21:25
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = PCL_KD_Tree_02
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

INCLUDEPATH += C:\PCL\include\pcl-1.8
LIBS += -LC:\PCL\lib -lpcl_common_debug -lpcl_io_debug -lpcl_kdtree_debug \
-lpcl_io_ply_debug -lpcl_sample_consensus_debug -lpcl_segmentation_debug \
-lpcl_visualization_debug

#boost1.5.9
INCLUDEPATH += E:\wondowslib\boost_1_59_0
LIBS += -LE:\wondowslib\boost_1_59_0\stage\lib -llibboost_thread-vc120-mt-gd-1_59

#Eigen3.2.8
INCLUDEPATH += E:\wondowslib\Eigen3.2.8

#FLANN
INCLUDEPATH += E:\wondowslib\FLANN\include
LIBS += -LE:\wondowslib\FLANN\lib

#VTK6.3.0
INCLUDEPATH += C:\VTK\include\vtk-6.3
LIBS += -LC:\VTK\lib -lvtkalglib-6.3 -lvtkChartsCore-6.3 -lvtkCommonColor-6.3 \
-lvtkCommonComputationalGeometry-6.3 -lvtkCommonCore-6.3 -lvtkCommonDataModel-6.3 -lvtkCommonExecutionModel-6.3\
-lvtkCommonMath-6.3 -lvtkCommonMisc-6.3 -lvtkCommonSystem-6.3 -lvtkCommonTransforms-6.3 \
-lvtkDICOMParser-6.3 -lvtkDomainsChemistry-6.3 -lvtkexoIIc-6.3 -lvtkexpat-6.3 -lvtkFiltersAMR-6.3 \
-lvtkFiltersCore-6.3 -lvtkFiltersExtraction-6.3 -lvtkFiltersFlowPaths-6.3 -lvtkFiltersGeneral-6.3 \
-lvtkFiltersGeneric-6.3 -lvtkFiltersGeometry-6.3 -lvtkFiltersHybrid-6.3 -lvtkFiltersHyperTree-6.3 \
-lvtkFiltersImaging-6.3 -lvtkFiltersModeling-6.3 -lvtkFiltersParallel-6.3 -lvtkFiltersParallelImaging-6.3 \
-lvtkFiltersProgrammable-6.3 -lvtkFiltersSelection-6.3 -lvtkFiltersSMP-6.3 -lvtkFiltersSources-6.3 \
-lvtkFiltersStatistics-6.3 -lvtkFiltersTexture-6.3 -lvtkFiltersVerdict-6.3 -lvtkfreetype-6.3 -lvtkftgl-6.3 \
-lvtkGeovisCore-6.3 -lvtkgl2ps-6.3  -lvtkGUISupportQt-6.3 -lvtkGUISupportQtOpenGL-6.3 -lvtkhdf5_hl-6.3 \
-lvtkhdf5-6.3 -lvtkInfovisCore-6.3 -lvtkInfovisLayout-6.3 -lvtkInteractionImage-6.3 -lvtkInteractionStyle-6.3 \
-lvtkInteractionWidgets-6.3 -lvtkIOAMR-6.3 -lvtkIOCore-6.3 -lvtkIOEnSight-6.3 -lvtkIOExodus-6.3 -lvtkIOExport-6.3 \
-lvtkIOGeometry-6.3 -lvtkIOImage-6.3 -lvtkIOImport-6.3 -lvtkIOInfovis-6.3 -lvtkIOLegacy-6.3 -lvtkproj4-6.3 \
-lvtkRenderingAnnotation-6.3 -lvtkRenderingContext2D-6.3 -lvtkRenderingContextOpenGL-6.3 -lvtkRenderingCore-6.3 \
-lvtkRenderingFreeType-6.3 -lvtkRenderingGL2PS-6.3 -lvtkRenderingImage-6.3 -lvtkRenderingOpenGL-6.3 -lvtkViewsGeovis-6.3 \
-lvtkViewsCore-6.3 -lvtkViewsContext2D-6.3 -lvtkViewsInfovis-6.3 -lvtkzlib-6.3
