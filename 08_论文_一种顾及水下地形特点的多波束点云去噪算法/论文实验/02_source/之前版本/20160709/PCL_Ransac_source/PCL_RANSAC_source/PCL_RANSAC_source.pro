QT += core
QT -= gui

CONFIG += c++11

TARGET = PCL_RANSAC_source
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    sac.cpp \
    sac_model_circle.cpp \
    sac_model_circle3d.cpp \
    sac_model_cone.cpp \
    sac_model_cylinder.cpp \
    sac_model_line.cpp \
    sac_model_normal_parallel_plane.cpp \
    sac_model_normal_plane.cpp \
    sac_model_normal_sphere.cpp \
    sac_model_parallel_line.cpp \
    sac_model_plane.cpp \
    sac_model_registration.cpp \
    sac_model_sphere.cpp \
    sac_model_stick.cpp
INCLUDEPATH += C:\Users\kevien\Desktop\pcl-pcl-1.8.0rc2\sample_consensus\include
#boost1.5.9
INCLUDEPATH += E:\wondowslib\boost_1_59_0
LIBS += -LE:\wondowslib\boost_1_59_0\stage\lib -llibboost_thread-vc120-mt-gd-1_59

#Eigen3.2.8
INCLUDEPATH += E:\wondowslib\Eigen3.2.8

#FLANN
INCLUDEPATH += E:\wondowslib\FLANN\include
LIBS += -LE:\wondowslib\FLANN\lib

HEADERS += \
    lmeds.hpp \
    mlesac.hpp \
    msac.hpp \
    prosac.hpp \
    ransac.hpp \
    rmsac.hpp \
    rransac.hpp \
    sac_model_circle.hpp \
    sac_model_circle3d.hpp \
    sac_model_cone.hpp \
    sac_model_cylinder.hpp \
    sac_model_line.hpp \
    sac_model_normal_parallel_plane.hpp \
    sac_model_normal_plane.hpp \
    sac_model_normal_sphere.hpp \
    sac_model_parallel_line.hpp \
    sac_model_parallel_plane.hpp \
    sac_model_perpendicular_plane.hpp \
    sac_model_plane.hpp \
    sac_model_registration.hpp \
    sac_model_registration_2d.hpp \
    sac_model_sphere.hpp \
    sac_model_stick.hpp \
    boost.h \
    eigen.h \
    lmeds.h \
    method_types.h \
    mlesac.h \
    model_types.h \
    msac.h \
    prosac.h \
    ransac.h \
    rmsac.h \
    rransac.h \
    sac.h \
    sac_model.h \
    sac_model_circle.h \
    sac_model_circle3d.h \
    sac_model_cone.h \
    sac_model_cylinder.h \
    sac_model_line.h \
    sac_model_normal_parallel_plane.h \
    sac_model_normal_plane.h \
    sac_model_normal_sphere.h \
    sac_model_parallel_line.h \
    sac_model_parallel_plane.h \
    sac_model_perpendicular_plane.h \
    sac_model_plane.h \
    sac_model_registration.h \
    sac_model_registration_2d.h \
    sac_model_sphere.h \
    sac_model_stick.h \
    impl/lmeds.hpp \
    impl/mlesac.hpp \
    impl/msac.hpp \
    impl/prosac.hpp \
    impl/ransac.hpp \
    impl/rmsac.hpp \
    impl/rransac.hpp \
    impl/sac_model_circle.hpp \
    impl/sac_model_circle3d.hpp \
    impl/sac_model_cone.hpp \
    impl/sac_model_cylinder.hpp \
    impl/sac_model_line.hpp \
    impl/sac_model_normal_parallel_plane.hpp \
    impl/sac_model_normal_plane.hpp \
    impl/sac_model_normal_sphere.hpp \
    impl/sac_model_parallel_line.hpp \
    impl/sac_model_parallel_plane.hpp \
    impl/sac_model_perpendicular_plane.hpp \
    impl/sac_model_plane.hpp \
    impl/sac_model_registration.hpp \
    impl/sac_model_registration_2d.hpp \
    impl/sac_model_sphere.hpp \
    impl/sac_model_stick.hpp
INCLUDEPATH += C:\PCL\include\pcl-1.8
LIBS += -LC:\PCL\lib -lpcl_common_debug -lpcl_io_debug -lpcl_kdtree_debug \
-lpcl_io_ply_debug -lpcl_sample_consensus_debug -lpcl_segmentation_debug \
-lpcl_visualization_debug
