﻿//#include <QCoreApplication>
#include <pcl/kdtree/kdtree_flann.h>
#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <QDebug>
#include <QDateTime>
#include <vector>

#include "pcl/sample_consensus/ransac.h" //--------随机一致算法(Ransac)
#include "pcl/sample_consensus/sac_model_plane.h" //------随机一致模型(Ransac_model)
#include "PCL/sample_consensus/method_types.h"
#include "pcl/sample_consensus/model_types.h"
#include "pcl/segmentation/sac_segmentation.h"
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>

#include <math.h>

/*
 * 设置相关变量和参数
*/
double threshold = 1; //标准差倍数
int K=150; // 搜索点个数
QString _pcdFilePathInput = "../QDH_qifu.pcd"; // 文件路径
QString _pcdFilePathOutput = "../QDH_qifu_Inlier_100_Stv1.5.pcd";
QString _pcdFilePathOutputFilter = "../QDH_qifu_Outer_100_Stv1.5.pcd";

/*
 * 辅助函数：
 * 获取系统时间
 * 显示点云
*/

//---获取系统时间---
void getCurrentTime(QString &str)
{
    QDateTime time = QDateTime::currentDateTime();
    str = time.toString("yyyy-MM-dd hh:mm:ss ddd");
    //qDebug() << str;
}
int q=0;
// 显示点云
boost::shared_ptr<pcl::visualization::PCLVisualizer>
simpleVis (pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud)
{
    // --------------------------------------------
    // -----Open 3D viewer and add point cloud-----
    // --------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->setBackgroundColor (0, 0, 0);
    viewer->addPointCloud<pcl::PointXYZ> (cloud, "sample cloud");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    //viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();
    return (viewer);
}
int main(int argc, char *argv[])
{
    /*---------------------------- step1:输入点云，创建kdtree--------------------------*/
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
//    QString _pcdFilePath = "../fenduan01.pcd"; // 文件路径
    pcl::PCDReader reader;
    reader.read(_pcdFilePathInput.toStdString(), *cloud); // 点云读入cloud
    std::cout << *cloud << std::endl; // 打印输入点云的头部信息
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree; // 构建kdtree对象
    kdtree.setInputCloud(cloud); //------建立kd-tree索引
    //std::vector<int> Indices_const;  ????如何解决
    //pcl::KdTree<pcl::PointXYZ>::IndicesConstPtr Indices_const = kdtree.getIndices();
    //std::cout << "Indices_const: " << Indices_const->size() << std::endl;
    //std::cout << "Indices_const: " << Indices_const << std::endl;
    /*---------------------------- step2:使用FLANN函数对搜索点进行K近邻搜索----------------*/
    // -------------------------输出当前时间----------
    QString str = "";
    getCurrentTime(str);
    qDebug() << "before InputCloud:" << str << "\n";

    //---待搜索点---
    pcl::PointXYZ searchPoint;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointInner(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointOuter(new pcl::PointCloud<pcl::PointXYZ>);
    int m=0;
    int n=0;

    cloudPointInner->points.resize(cloud->size());
    cloudPointOuter->points.resize(cloud->size());
    for(int searchPointId = 0; searchPointId < cloud->points.size(); ++searchPointId)
    {
        //        searchPoint.x = 4942200.0;
        //        searchPoint.y = 8926899.0;
        //        searchPoint.z = 157200.1;

        //int searchPointId = 0;
        searchPoint.x = cloud->points[searchPointId].x;
        searchPoint.y = cloud->points[searchPointId].y;
        searchPoint.z = cloud->points[searchPointId].z;


        //---存储近邻域搜索到的点
        pcl::PointCloud<pcl::PointXYZ>::Ptr kdtreeSearchedPoint(new pcl::PointCloud<pcl::PointXYZ>);
        //---使用FLANN函数进行近邻域搜索---
        std::vector<int> pointIndex(K); // 存储索引
        std::vector<float> pointSquaredDiatance(K); // 存储距离
        //---用搜索到的点，填充kdtreeSearchedPoint---
        kdtreeSearchedPoint->width = K + 1;
        kdtreeSearchedPoint->height = 1;
        kdtreeSearchedPoint->is_dense = false;
        kdtreeSearchedPoint->points.resize(kdtreeSearchedPoint->width * kdtreeSearchedPoint->height);
        //---FLANN函数对Kd_tree进行近邻域搜索，搜索K个点---
        kdtree.nearestKSearch(searchPoint, K, pointIndex, pointSquaredDiatance);//---保存搜索到点的索引号和对应的距离
        for(int i=0; i<pointIndex.size(); ++i) //-----根据cloud的索引号输出相应的坐标值
        {
            //        std::cout << " " << cloud->points[pointIndex[i]].x
            //                  << " " << cloud->points[pointIndex[i]].y
            //                  << " " << cloud->points[pointIndex[i]].z << std::endl
            //        << "squred distance:" << pointSquaredDiatance[i] << std::endl;
            //---用搜索到的点填充kdtreeSearchedPoint---
            kdtreeSearchedPoint->points[i].x = cloud->points[pointIndex[i]].x;
            kdtreeSearchedPoint->points[i].y = cloud->points[pointIndex[i]].y;
            kdtreeSearchedPoint->points[i].z = cloud->points[pointIndex[i]].z;

            //        std::cout << std::endl << "存储点云" << std::endl
            //                  << kdtreeSearchedPoint->points[i].x << " "
            //                  << kdtreeSearchedPoint->points[i].y << " "
            //                  << kdtreeSearchedPoint->points[i].z << std::endl;
        }
        //---添加待搜索点---
        kdtreeSearchedPoint->points[K].x = searchPoint.x;
        kdtreeSearchedPoint->points[K].y = searchPoint.y;
        kdtreeSearchedPoint->points[K].z = searchPoint.z;
        /*------------------------- step3:建立ransac算法模型和对象，生成拟合平面----------------*/
        //---实例化ransac算法模型和对象---
        pcl::SampleConsensusModelPlane<pcl::PointXYZ>::Ptr
                model_p (new pcl::SampleConsensusModelPlane<pcl::PointXYZ> (kdtreeSearchedPoint));
        pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(model_p);
        //---算法流程
        ransac.setDistanceThreshold(0.05); // 设置阈值
        ransac.computeModel(); // 计算模型参数 ----速度较慢
        Eigen::VectorXf model_coefficient(4);
        ransac.getModelCoefficients(model_coefficient); //获取模型参数
        //    std::cout << "model_coefficient:" << model_coefficient[0] << std::endl
        //              << model_coefficient[1] << std::endl
        //              << model_coefficient[2] << std::endl
        //              << model_coefficient[3] << std::endl; //打印模型参数

        /*------------------------- step4:计算点到平面的距离----------------*/
        //model_coefficient[0] * model_coefficient[0];
        double a, b, c, d;
        a = model_coefficient[0];
        b = model_coefficient[1];
        c = model_coefficient[2];
        d = model_coefficient[3];
        std::vector<double> distancePointToPlane(kdtreeSearchedPoint->size()); // 用于存储距离
        double A = sqrt(a*a + b*b + c*c);
        for(unsigned int i=0; i<kdtreeSearchedPoint->size(); ++i)
        {
            distancePointToPlane[i] =
                    fabs((kdtreeSearchedPoint->points[i].x) * a + (kdtreeSearchedPoint->points[i].y) * b + (kdtreeSearchedPoint->points[i].z) * c - d);
            distancePointToPlane[i] = distancePointToPlane[i] / A ;
        }
//        std::cout << "Ransac: a:" << a << std::endl
//                  << "b:" << b << std::endl
//                  << "c: " << c << std::endl
//                  << "d:" << d << std::endl;



        //    std::vector<double>::iterator it = distancePointToPlane.begin();
        //    for(;it!=distancePointToPlane.end();++it){
        //        std::cout << "distance:" <<*it << std::endl;
        //    }
        /*/------------------------- step5: 计算均值和标准差---------------------*/
        // compute mean
        double sum = 0.0; // 求和
        double squareSum = 0.0; // 求平方和
        double mean = 0.0; // 记录均值
        double Stv = 0.0; // 记录标准差
        //size_t dataSize = value.size();
        for(unsigned int j=0; j<distancePointToPlane.size(); ++j){
            sum += distancePointToPlane[j];
        }
        mean = sum / (K+1);
        // compute Std
        for(unsigned int k=0; k<distancePointToPlane.size(); ++k)
        {
            squareSum += (distancePointToPlane[k]-mean)*(distancePointToPlane[k]-mean);
        }
        Stv = sqrt(squareSum/(K+1));
//        cout << Stv << endl
//             << mean << endl;
        //    std::cout << "mean:" << mean << std::endl
        //              << "stv:" << Stv << std::endl;

        /*-------------------- step6: 判断searchPoint是否在指定标准差范围内----------*/
        //std::vector<int> allInliers;
        //std::vector<int> allOutliers;

        double limitMin = 0.0; // 阈值下限
        double limitMax = 0.0; // 阈值上限
        // int k=0;
        limitMin = mean - threshold * Stv;
        limitMax = mean + threshold * Stv;
        if(distancePointToPlane[K] >limitMin && distancePointToPlane[K] < limitMax){
            cloudPointInner->points[m].x = searchPoint.x;
            cloudPointInner->points[m].y  = searchPoint.y;
            cloudPointInner->points[m].z  = searchPoint.z;
            m += 1;
        }else{
            cloudPointOuter->points[n].x = searchPoint.x;
            cloudPointOuter->points[n].y  = searchPoint.y;
            cloudPointOuter->points[n].z  = searchPoint.z;
            n += 1;
        }
        //q+=1;
        //std::cout << q << std::endl;
    }
    std::cout << m << endl;
    std::cout << n << endl;
    cloudPointInner->width = m;
    cloudPointInner->height = 1;
    cloudPointInner->is_dense = false;
    cloudPointInner->points.resize(m);

    cloudPointOuter->width = n;
    cloudPointOuter->height = 1;
    cloudPointOuter->is_dense = false;
    cloudPointOuter->points.resize(n);

    /*------------------------- step7: 将点云写入文件----------------*/
    pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> (_pcdFilePathOutput.toStdString(), *cloudPointInner, true);
    writer.write<pcl::PointXYZ> (_pcdFilePathOutputFilter.toStdString(), *cloudPointOuter, true);

    // -------------------------输出当前时间----------
    //QString str = "";
    getCurrentTime(str);
    qDebug() << "After InputCloud:" << str << "\n";

    /*------------------------- step: 使用VTK库显示点云----------------*/
    //std::vector<int> inliers2;
    //ransac.getInliers(inliers2); //获取内点索引
    //pcl::PointCloud<pcl::PointXYZ>::Ptr final (new pcl::PointCloud<pcl::PointXYZ>);
    //copy all inliers to final(索引号)
    //pcl::copyPointCloud<pcl::PointXYZ>(*kdtreeSearchedPoint, inliers2, *final);

    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
    viewer = simpleVis(cloudPointInner);
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }

    return 0;
}
