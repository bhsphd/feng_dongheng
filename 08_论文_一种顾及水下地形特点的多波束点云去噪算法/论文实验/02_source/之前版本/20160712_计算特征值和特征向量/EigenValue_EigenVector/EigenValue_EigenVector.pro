QT += core
QT -= gui

CONFIG += c++11

TARGET = EigenValue_EigenVector
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

#Eigen3.2.8
INCLUDEPATH += E:\wondowslib\Eigen3.2.8
