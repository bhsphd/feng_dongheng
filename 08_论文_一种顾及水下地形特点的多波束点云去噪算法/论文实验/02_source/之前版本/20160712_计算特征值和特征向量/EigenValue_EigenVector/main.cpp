#include <QCoreApplication>
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

using namespace Eigen;
using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    Matrix3d A;
    A << 1,2,3,4,5,6,7,8,9;
    cout << "A:" << endl << A << endl << endl;
    EigenSolver<Matrix3d> es(A);

    // Matrix3d 等价于 Matrix3d(double, 3, 3):3乘以3矩阵，每个元素为double
    Matrix3d D = es.pseudoEigenvalueMatrix();
    Matrix3d V = es.pseudoEigenvectors();
    cout << "pseudo-eigenvalueD: " << endl
         << D << endl;
    cout << "pseudo-eigenvectorV: " << endl
         << V << endl;

    // 获取矩阵的特征值，并存储为EigenValue[3]
    double eigenValue[3];
    eigenValue[0] = D(0, 0);
    eigenValue[1] = D(1, 1);
    eigenValue[2] = D(2, 2);
    // 保存最小特征值对应的特征向量
    double a, b, c, d;
    if(eigenValue[0] < eigenValue[1] && eigenValue[0] < eigenValue[2]){
        a = V(0, 0);
        b = V(1, 0);
        c = V(2, 0);
    }else if(eigenValue[1] < eigenValue[0] && eigenValue[1] < eigenValue[2]){
        a = V(0, 1);
        b = V(1, 1);
        c = V(2, 1);
    }else{
        a = V(0, 2);
        b = V(1, 2);
        c = V(2, 2);
    }
    cout << "a:" << a << endl << "b:" << b
         << endl << "c:" << c << endl;

    return app.exec();
}
