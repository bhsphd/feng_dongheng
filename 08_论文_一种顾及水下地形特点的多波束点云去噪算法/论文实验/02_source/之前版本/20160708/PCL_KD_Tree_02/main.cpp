﻿//#include <QCoreApplication>
#include <pcl/kdtree/kdtree_flann.h>

#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <QDebug>
#include <QDateTime>
#include <vector>

#include "pcl/sample_consensus/ransac.h" //--------随机一致算法(Ransac)
#include "pcl/sample_consensus/sac_model_plane.h" //------随机一致模型(Ransac_model)
#include "PCL/sample_consensus/method_types.h"
#include "pcl/sample_consensus/model_types.h"
#include "pcl/segmentation/sac_segmentation.h"
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>

// 显示点云
boost::shared_ptr<pcl::visualization::PCLVisualizer>
simpleVis (pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud)
{
  // --------------------------------------------
  // -----Open 3D viewer and add point cloud-----
  // --------------------------------------------
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->setBackgroundColor (0, 0, 0);
  viewer->addPointCloud<pcl::PointXYZ> (cloud, "sample cloud");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
  //viewer->addCoordinateSystem (1.0);
  viewer->initCameraParameters ();
  return (viewer);
}

// 使用ransac算法 对搜索到的点拟合平面 计算拟合平面参数
// 使用ransac算法，对搜索到的点拟合平面，并计算点到平面的距离了，去除距离超过一倍方差的点。



using namespace std;
int main(int argc, char *argv[])
{
    //QCoreApplication a(argc, argv);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr kdtreeSearchedPoint(new pcl::PointCloud<pcl::PointXYZ>);
    //-------用于存储FLANN函数近邻域搜索到的点
    QString _pcdFilePath = "../fenduan0Cloud002.pcd";
    pcl::PCDReader reader;
    reader.read(_pcdFilePath.toStdString(), *cloud);
    std::cout << *cloud << std::endl;
// 创建kdtree
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
    kdtree.setInputCloud(cloud); //------说明：已经建立kd-tree索引

    pcl::PointXYZ searchPoint;
    searchPoint.x = 4942200.0;
    searchPoint.y = 8926899.0;
    searchPoint.z = 157200.1;
// 使用FLANN函数进行近邻域搜索
    int K=500;
    std::vector<int> pointIndex(K);
    std::vector<float> pointSquaredDiatance(K);
    std::vector<int> inliers; //---用于存储ransac算法得到的局内点索引---
//--------用搜索到的点，填充kdtreeSearchedPoint------
    kdtreeSearchedPoint->width = K;
    kdtreeSearchedPoint->height = 1;
    kdtreeSearchedPoint->is_dense = false;
    kdtreeSearchedPoint->points.resize(kdtreeSearchedPoint->width * kdtreeSearchedPoint->height);
//--------FLANN函数对Kd_tree进行近邻域搜索，搜索K个点----------------
    kdtree.nearestKSearch(searchPoint, K, pointIndex, pointSquaredDiatance);//---保存搜索到点的索引号和对应的距离
    for(int i=0; i<pointIndex.size(); ++i) //-----根据cloud的索引号输出相应的坐标值
    {
        std::cout << " " << cloud->points[pointIndex[i]].x
                  << " " << cloud->points[pointIndex[i]].y
                  << " " << cloud->points[pointIndex[i]].z << std::endl
        << "squred distance:" << pointSquaredDiatance[i] << std::endl;
//--------把Kd_tree搜索到的K个点，存至kdtreeSearchedPoint中-------------
        kdtreeSearchedPoint->points[i].x = cloud->points[pointIndex[i]].x;
        kdtreeSearchedPoint->points[i].y = cloud->points[pointIndex[i]].y;
        kdtreeSearchedPoint->points[i].z = cloud->points[pointIndex[i]].z;

        std::cout << std::endl << "存储点云" << std::endl
                  << kdtreeSearchedPoint->points[i].x << " "
                  << kdtreeSearchedPoint->points[i].y << " "
                  << kdtreeSearchedPoint->points[i].z << std::endl;
    }

    //qDebug() << pointIndex.size();
//-----------创建针对平面模型的对象----------
    //pcl::SampleConsensusModelPlane<pcl::PointXYZ>::Ptr
    //        model_p(new pcl::SampleConsensusModelPlane<pcl::PointXYZ>(kdtreeSearchedPoint));
//-----------将model输入到ransac算法中----------
    //pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(model_p);
    //ransac.setDistanceThreshold(0.01);
    //ransac.computeModel();// 进行随机参数估计
    //ransac.getInliers(inliers); // 将局内点存入inliers
    std::vector<int> inliers2;
    pcl::SampleConsensusModelPlane<pcl::PointXYZ>::Ptr
            model_p (new pcl::SampleConsensusModelPlane<pcl::PointXYZ> (kdtreeSearchedPoint));

    pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(model_p);
    ransac.setDistanceThreshold (5);
    ransac.computeModel();
    ransac.getInliers(inliers2);

    std::cout << inliers2.size();
    std::cout << kdtreeSearchedPoint->size();
    // copies all inliers of the model computed to another PointCloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr final (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::copyPointCloud<pcl::PointXYZ>(*kdtreeSearchedPoint, inliers2, *final);

    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
    viewer = simpleVis(kdtreeSearchedPoint);
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }


    //------------tester-------------

    return 0;
}
