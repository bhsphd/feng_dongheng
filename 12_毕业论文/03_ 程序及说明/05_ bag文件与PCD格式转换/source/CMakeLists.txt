cmake_minimum_required(VERSION 2.8.3)
project(bag_read)

SET( CMAKE_CXX_FLAGS "-std=c++11")
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
    roscpp
    std_msgs
    sensor_msgs
    laser_geometry
    tf
    pcl_ros
    rosbag
)

#find_package(PCL REQUIRED COMPONENTS common io)
#find_package(Boost REQUIRED COMPONENTS system)
#include_directories(${PROJECT_NAME} SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES bag_read
#  CATKIN_DEPENDS roscpp
#  DEPENDS system_lib
)
include_directories(
# include
  ${catkin_INCLUDE_DIRS}
)
 add_executable(${PROJECT_NAME} src/read.cpp)

 target_link_libraries(${PROJECT_NAME}
   ${catkin_LIBRARIES}
 )

