#include <ros/ros.h>

#include <string>
#include <iostream>

//rosbag header
#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <std_msgs/Int32.h>
#include <std_msgs/String.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/PointCloud2.h>
#include <boost/foreach.hpp>

#include <pcl_ros/point_cloud.h>
//#include <pcl/point_types>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/io/pcd_io.h>
#define foreach BOOST_FOREACH
using namespace std;
int main(int argc, char **argv)
{
  ros::init(argc, argv, "read");
  ros::NodeHandle nh;
  rosbag::Bag bag;
  bag.open("./scan_matched.bag",rosbag::bagmode::Read);


  pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPoint(new pcl::PointCloud<pcl::PointXYZ>);
  cloudPoint->points.resize(10000000);
  int m = 0;
//int sum =0;
  rosbag::View view(bag);
  foreach (rosbag::MessageInstance const msg, view)
  {
      //cout << msg.getTime() << endl; //输出时间
      //cout << sum << endl;
      //sum += 1;
    if(msg.isType<sensor_msgs::PointCloud2>())
    {
      sensor_msgs::PointCloud2::ConstPtr input = msg.instantiate<sensor_msgs::PointCloud2>();
      if(input == nullptr){
          break;
      }else{
          pcl::PointCloud<pcl::PointXYZ> cloud;
          pcl::fromROSMsg(*input, cloud);
          //cout << m << endl;
          int cloudsize = cloud.size();
          for(int i=0; i<cloudsize; ++i){
              cloudPoint->points[m].x = cloud.points[i].x;
              cloudPoint->points[m].y = cloud.points[i].y;
              cloudPoint->points[m].z = cloud.points[i].z;
              m += 1;
          }
      }
    }
  }
  cloudPoint->width = m;
  cloudPoint->height = 1;
  cloudPoint->is_dense = false;
  cloudPoint->points.resize(m);
  pcl::PCDWriter writer;
  writer.write<pcl::PointXYZ> (string("./write_pcd_test.pcd"), *cloudPoint, true);

  bag.close();
  ROS_INFO("Hello world!");
}

