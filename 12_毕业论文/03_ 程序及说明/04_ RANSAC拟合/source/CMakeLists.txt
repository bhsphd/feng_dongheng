cmake_minimum_required(VERSION 2.8.3)

project(cylinder_segmentation)
set(CMAKE_CXX_FLAGS "-std=c++11")
find_package(PCL REQUIRED COMPONENTS)

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_executable (cylinder_segmentation cylinder_segmentation.cpp)
target_link_libraries (cylinder_segmentation ${PCL_LIBRARIES})
