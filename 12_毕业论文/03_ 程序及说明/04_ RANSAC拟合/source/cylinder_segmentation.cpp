#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <string>
using namespace std;
string baseName = "../cy01";

typedef pcl::PointXYZ PointT;

int main (int argc, char** argv)
{
    // All the objects needed
    pcl::PCDReader reader;
    pcl::PassThrough<PointT> pass;
    pcl::NormalEstimation<PointT, pcl::Normal> ne;
    pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg;
    pcl::PCDWriter writer;
    pcl::ExtractIndices<PointT> extract;
    pcl::ExtractIndices<pcl::Normal> extract_normals;
    pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());

    // Datasets
    pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>); //读取点云数据
    pcl::PointCloud<PointT>::Ptr cloud_filtered (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<PointT>::Ptr cloud_filtered2 (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals2 (new pcl::PointCloud<pcl::Normal>);
    pcl::ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients), coefficients_cylinder (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_plane (new pcl::PointIndices), inliers_cylinder (new pcl::PointIndices);


    size_t a[2] = {0, 0};

    // Read in the cloud data //读取点云数据
    for(size_t i = 0; i<2; ++i){
        reader.read (string(baseName + "_" + to_string(i) + ".pcd"), *cloud_filtered);
        std::cerr << "PointCloud has: " << cloud_filtered->points.size () << " data points." << std::endl;

        // Build a passthrough filter to remove spurious NaNs //去除粗差
//        pass.setInputCloud (cloud);
//        pass.setFilterFieldName ("z");
//        pass.setFilterLimits (0, 5);
//        pass.filter (*cloud_filtered);
//        std::cerr << "PointCloud after filtering has: " << cloud_filtered->points.size () << " data points." << std::endl;

        // Estimate point normals //估计每个点的表面法线
        ne.setSearchMethod (tree);
        ne.setInputCloud (cloud_filtered);
        ne.setKSearch (50);
        ne.compute (*cloud_normals);

        //投影，令z=0； //与不投影拟合的圆心一致
          for(size_t i=0; i<cloud_filtered->points.size(); ++i){
              cloud_filtered->points[i].z = 0;
          }

        // 读取点云，并分片，根据z值
        //step1: 计算z方向值，将其均分为5部分 //生成5个文件
        //step2: 提取内环和外环点云 //生成 10 个文件 //手动
        //step3: 遍历10个文件，分别调用如下函数，输出每个文件的结果  序号：内环与半径/外环与半径/  //根据文件名称1_1 和 1_2


        // Create the segmentation object for cylinder segmentation and set all the parameters
        // 拟合圆柱体
        seg.setOptimizeCoefficients (true);
        seg.setModelType (pcl::SACMODEL_CIRCLE2D); //拟合2D圆
        seg.setMethodType (pcl::SAC_RANSAC);
        seg.setNormalDistanceWeight (0.1); //半径上限 0.1
        seg.setMaxIterations (10000);
        seg.setDistanceThreshold (0.5); //点到模型距离阈值//调大 0.05
        seg.setRadiusLimits (0, 0.5); //最大/最小半径 0 0.1
        seg.setInputCloud (cloud_filtered);
        seg.setInputNormals (cloud_normals);

        // Obtain the cylinder inliers and coefficients
        seg.segment (*inliers_cylinder, *coefficients_cylinder);
        std::cerr << "Cylinder coefficients: " << *coefficients_cylinder << std::endl;

        // Write the cylinder inliers to disk
        extract.setInputCloud (cloud_filtered);
        extract.setIndices (inliers_cylinder);
        extract.setNegative (false);
        pcl::PointCloud<PointT>::Ptr cloud_cylinder (new pcl::PointCloud<PointT> ());
        extract.filter (*cloud_cylinder);
        if (cloud_cylinder->points.empty ())
            std::cerr << "Can't find the cylindrical component." << std::endl;
        else
        {
            //cout << "Num:"<< i << endl;
            std::cerr << "PointCloud representing the cylindrical component: " << cloud_cylinder->points.size () << " data points." << std::endl;
            writer.write (string(baseName + "_" + to_string(i) + "_cylinder.pcd"), *cloud_cylinder, false);
        }
        //cloud_filtered->points.clear();
        //a[i] = ;
    }
    // 输出距离值
    cout << "distance:" ;
    return (0);
}
