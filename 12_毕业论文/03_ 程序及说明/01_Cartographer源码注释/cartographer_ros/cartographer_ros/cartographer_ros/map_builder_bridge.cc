/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "map_builder_bridge.h"

#include "assets_writer.h"
#include "msg_conversion.h"
#include "occupancy_grid.h"
#include "cartographer_ros_msgs/TrajectorySubmapList.h"

namespace cartographer_ros {

MapBuilderBridge::MapBuilderBridge(const NodeOptions& options,
                                   tf2_ros::Buffer* const tf_buffer)
    : options_(options),
      map_builder_(options.map_builder_options), //inter:init
      tf_buffer_(tf_buffer) {}

int MapBuilderBridge::AddTrajectory(
    const std::unordered_set<string>& expected_sensor_ids, //
    const string& tracking_frame) {

  //添加一条轨迹，并返回其索引号// 根据sensor
  const int trajectory_id = map_builder_.AddTrajectoryBuilder(
      expected_sensor_ids, options_.trajectory_builder_options);
  LOG(INFO) << "Added trajectory with ID '" << trajectory_id << "'.";

  CHECK_EQ(sensor_bridges_.count(trajectory_id), 0); //空

  sensor_bridges_[trajectory_id] = //键-值对
      cartographer::common::make_unique<SensorBridge>(
          tracking_frame, options_.lookup_transform_timeout_sec, tf_buffer_,
          map_builder_.GetTrajectoryBuilder(trajectory_id));
  return trajectory_id;
}

void MapBuilderBridge::FinishTrajectory(const int trajectory_id) {
  LOG(INFO) << "Finishing trajectory with ID '" << trajectory_id << "'...";

  CHECK_EQ(sensor_bridges_.count(trajectory_id), 1);  //完成后数目为1
  map_builder_.FinishTrajectory(trajectory_id);  //标志该轨迹已经完成，期望不再接受其他数据
  map_builder_.sparse_pose_graph()->RunFinalOptimization(); //运行优化--优化该条轨迹
  sensor_bridges_.erase(trajectory_id); //优化后sensor_bridges_删除该条轨迹
}

void MapBuilderBridge::WriteAssets(const string& stem) {
  std::vector<::cartographer::mapping::TrajectoryNode> trajectory_nodes;
  for (const auto& single_trajectory :
       map_builder_.sparse_pose_graph()->GetTrajectoryNodes()) {
    trajectory_nodes.insert(trajectory_nodes.end(), single_trajectory.begin(),
                            single_trajectory.end());
  }
  if (trajectory_nodes.empty()) {
    LOG(WARNING) << "No data was collected and no assets will be written.";
  } else {
    LOG(INFO) << "Writing assets with stem '" << stem << "'...";
    if (options_.map_builder_options.use_trajectory_builder_2d()) {
      Write2DAssets(
          trajectory_nodes, options_.map_frame,
          options_.trajectory_builder_options.trajectory_builder_2d_options()
              .submaps_options(),
          stem);
    }

    if (options_.map_builder_options.use_trajectory_builder_3d()) {
      Write3DAssets(
          trajectory_nodes,
          options_.trajectory_builder_options.trajectory_builder_3d_options()
              .submaps_options()
              .high_resolution(),
          stem);
    }
  }
}

bool MapBuilderBridge::HandleSubmapQuery(
    cartographer_ros_msgs::SubmapQuery::Request& request,
    cartographer_ros_msgs::SubmapQuery::Response& response) {
  // 判断是否存在error
  cartographer::mapping::proto::SubmapQuery::Response response_proto;
  const std::string error = map_builder_.SubmapToProto(
      request.trajectory_id, request.submap_index, &response_proto);
  if (!error.empty()) {
    LOG(ERROR) << error;
    return false;
  }

  response.submap_version = response_proto.submap_version();
  response.cells.insert(response.cells.begin(), response_proto.cells().begin(),
                        response_proto.cells().end());
  response.width = response_proto.width();
  response.height = response_proto.height();
  response.resolution = response_proto.resolution();
  response.slice_pose = ToGeometryMsgPose(
      cartographer::transform::ToRigid3(response_proto.slice_pose()));
  return true;
}
// inter:获取优化后的所有submap()
cartographer_ros_msgs::SubmapList MapBuilderBridge::GetSubmapList() {

  cartographer_ros_msgs::SubmapList submap_list;
  submap_list.header.stamp = ::ros::Time::now(); //现在的ros时间
  submap_list.header.frame_id = options_.map_frame; //坐标系为map
  //
  for (int trajectory_id = 0;
       trajectory_id < map_builder_.num_trajectory_builders(); //2个
       ++trajectory_id) {
    // 获取坐标转换关系：一条轨迹对应多个子图submap(submaps)
    const std::vector<cartographer::transform::Rigid3d> submap_transforms =
        map_builder_.sparse_pose_graph()->GetSubmapTransforms(trajectory_id);
    //
    const cartographer::mapping::Submaps* submaps =
        map_builder_.GetTrajectoryBuilder(trajectory_id)->submaps();
    CHECK_LE(submap_transforms.size(), submaps->size());

/*******trajectory_submap_list：很多submap********/
    cartographer_ros_msgs::TrajectorySubmapList trajectory_submap_list;
    for (size_t submap_index = 0; submap_index != submap_transforms.size();
         ++submap_index) {
      cartographer_ros_msgs::SubmapEntry submap_entry;
      submap_entry.submap_version = submaps->Get(submap_index)->num_range_data; //插入submap的per scan数目
      submap_entry.pose = ToGeometryMsgPose(submap_transforms[submap_index]);
      trajectory_submap_list.submap.push_back(submap_entry);
    }
    //
    submap_list.trajectory.push_back(trajectory_submap_list);
  }
  return submap_list;
}

std::unique_ptr<nav_msgs::OccupancyGrid>
MapBuilderBridge::BuildOccupancyGrid() {
  CHECK(options_.map_builder_options.use_trajectory_builder_2d())
      << "Publishing OccupancyGrids for 3D data is not yet supported"; // 0.3.0支持

  std::vector<::cartographer::mapping::TrajectoryNode> trajectory_nodes;
  for (const auto& single_trajectory :
       map_builder_.sparse_pose_graph()->GetTrajectoryNodes()) {
    trajectory_nodes.insert(trajectory_nodes.end(), single_trajectory.begin(),
                            single_trajectory.end());
  }
  std::unique_ptr<nav_msgs::OccupancyGrid> occupancy_grid;
  if (!trajectory_nodes.empty()) {
    occupancy_grid =
        cartographer::common::make_unique<nav_msgs::OccupancyGrid>();

    // 根据所有的trajectory_node构建2DOccupancy网格
    BuildOccupancyGrid2D(
        trajectory_nodes, options_.map_frame,
        options_.trajectory_builder_options.trajectory_builder_2d_options()
            .submaps_options(),
        occupancy_grid.get());
  }
  return occupancy_grid;
}

/************************返回所有TrajectoryStates*************************/
std::unordered_map<int, MapBuilderBridge::TrajectoryState>
MapBuilderBridge::GetTrajectoryStates() {
// <int, trajctoryState>: int:轨迹id(2个laser即存在两条轨迹)，trajectoryState：轨迹状态
  std::unordered_map<int, TrajectoryState> trajectory_states;
  for (const auto& entry : sensor_bridges_) {
    const int trajectory_id = entry.first; //轨迹id
    const SensorBridge& sensor_bridge = *entry.second; //sensor_bridge

    // 所谓的trajectoryBuilder提供一些接口，可用于获取数据处理的结果
    const cartographer::mapping::TrajectoryBuilder* const trajectory_builder =
        map_builder_.GetTrajectoryBuilder(trajectory_id);

    // 当前帧的处理后的pose+点云(什么坐标系下？？)
    const cartographer::mapping::TrajectoryBuilder::PoseEstimate pose_estimate =
        trajectory_builder->pose_estimate();

    if (cartographer::common::ToUniversal(pose_estimate.time) < 0) {
      continue;
    }
//published_frame
    trajectory_states[trajectory_id] = {
        pose_estimate,
        map_builder_.sparse_pose_graph()->GetLocalToGlobalTransform(
            trajectory_id), //inter:获取轨迹id到Global的坐标转换关系
        sensor_bridge.tf_bridge().LookupToTracking(pose_estimate.time,
                                                   options_.published_frame)}; //发布的frame:base_link;Idendity
  }
  return trajectory_states;
}

// inter：return unique_ptr(sensor_bridge):已经分配内存
SensorBridge* MapBuilderBridge::sensor_bridge(const int trajectory_id) {
  return sensor_bridges_.at(trajectory_id).get();  //关于轨迹id的sensor_bridge？sensor_bridge与trajectory关系？
}

}  // namespace cartographer_ros
