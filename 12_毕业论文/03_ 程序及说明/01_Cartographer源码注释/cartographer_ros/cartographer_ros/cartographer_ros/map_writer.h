#ifndef MAP_WRITER_H
#define MAP_WRITER_H
#include <string>

#include "nav_msgs/OccupancyGrid.h"

namespace cartographer_ros {

// Writes the given 'occupancy_grid' as 'stem'.pgm and 'stem'.yaml.
void WriteOccupancyGridToPgmAndYaml(
    const ::nav_msgs::OccupancyGrid& occupancy_grid, const std::string& stem);

}  // namespace cartographer_ros

#endif // MAP_WRITER_H
