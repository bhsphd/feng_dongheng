#ifndef ASSETS_WRITER_H
#define ASSETS_WRITER_H
#include <string>
#include <vector>

#include "cartographer/mapping/trajectory_node.h"
#include "node_options.h"

namespace cartographer_ros {

// Writes a trajectory proto and an occupancy grid.
void Write2DAssets(
    const std::vector<::cartographer::mapping::TrajectoryNode>&
        trajectory_nodes,
    const string& map_frame,
    const ::cartographer::mapping_2d::proto::SubmapsOptions& submaps_options,
    const std::string& stem);

// Writes X-ray images, trajectory proto, and PLY files from the
// 'trajectory_nodes'. The filenames will all start with 'stem'.
void Write3DAssets(const std::vector<::cartographer::mapping::TrajectoryNode>&
                       trajectory_nodes,
                   const double voxel_size, const std::string& stem);

}  // namespace cartographer_ros
#endif // ASSETS_WRITER_H
