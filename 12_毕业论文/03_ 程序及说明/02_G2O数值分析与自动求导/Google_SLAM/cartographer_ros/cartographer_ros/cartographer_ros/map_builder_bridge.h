/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CARTOGRAPHER_ROS_MAP_BUILDER_BRIDGE_H_
#define CARTOGRAPHER_ROS_MAP_BUILDER_BRIDGE_H_

#include <memory>
#include <unordered_map>
#include <unordered_set>

#include "cartographer/mapping/map_builder.h"
#include "cartographer/mapping/proto/trajectory_builder_options.pb.h"
#include "node_options.h"
#include "sensor_bridge.h"
#include "tf_bridge.h"
#include "cartographer_ros_msgs/SubmapEntry.h"
#include "cartographer_ros_msgs/SubmapList.h"
#include "cartographer_ros_msgs/SubmapQuery.h"
#include "nav_msgs/OccupancyGrid.h"

// 地图构建MapBuilder转换
namespace cartographer_ros {

class MapBuilderBridge {
 public:
  // 轨迹状态
  //inter 获得轨迹的当前状态(也就是当前帧的状态)
  struct TrajectoryState {
    cartographer::mapping::TrajectoryBuilder::PoseEstimate pose_estimate; //pose-cloudpoint
    // locat to map的变换
    cartographer::transform::Rigid3d local_to_map; // map坐标系为global坐标系；global坐标系为pose_graph优化后的坐标系(误差)
    // tracking_frame到publish坐标系
    std::unique_ptr<cartographer::transform::Rigid3d> published_to_tracking; //发布坐标系到tracking
  };

  MapBuilderBridge(const NodeOptions& options, tf2_ros::Buffer* tf_buffer);

  MapBuilderBridge(const MapBuilderBridge&) = delete;
  MapBuilderBridge& operator=(const MapBuilderBridge&) = delete;

  // 添加一条轨迹，并返回其索引号；（对应一个sensor_bridge--Hash表）
  int AddTrajectory(const std::unordered_set<string>& expected_sensor_ids,
                    const string& tracking_frame);
  // 当轨迹构建完成时，标记该轨迹完成，运行优化并删除该条轨迹
  void FinishTrajectory(int trajectory_id);
  void WriteAssets(const string& stem);

  // 回调函数：Request和Response：Service服务：给request一些参数，返回值保存在response中
  bool HandleSubmapQuery(
      cartographer_ros_msgs::SubmapQuery::Request& request,
      cartographer_ros_msgs::SubmapQuery::Response& response);

  // 展示Submap-submaps-trajectory-trajectorySubmapList-submapList-等相关逻辑
  cartographer_ros_msgs::SubmapList GetSubmapList();
  std::unique_ptr<nav_msgs::OccupancyGrid> BuildOccupancyGrid(); //构建2D导航格网
  std::unordered_map<int, TrajectoryState> GetTrajectoryStates(); // 获取轨迹状态，填充struct

  SensorBridge* sensor_bridge(int trajectory_id); //获取关于轨迹id的sensor_bridge// ？？多个

 private:
  const NodeOptions options_; //Top-level 参数

  cartographer::mapping::MapBuilder map_builder_; //地图构建(底层代码)：包含多条轨迹构建器
  tf2_ros::Buffer* const tf_buffer_; //坐标转换与追踪
  // inter：sensor_bridge//存储全局的TfBridge,每条轨迹的TfBridge是不一样的
  std::unordered_map<int, std::unique_ptr<SensorBridge>> sensor_bridges_;
};

}  // namespace cartographer_ros

#endif  // CARTOGRAPHER_ROS_MAP_BUILDER_BRIDGE_H_
