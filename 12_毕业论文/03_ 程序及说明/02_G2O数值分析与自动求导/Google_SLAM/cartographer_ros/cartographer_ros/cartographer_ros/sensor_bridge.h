/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CARTOGRAPHER_ROS_SENSOR_BRIDGE_H_
#define CARTOGRAPHER_ROS_SENSOR_BRIDGE_H_

#include "cartographer/mapping/trajectory_builder.h"  //调用2D/3D SLAM算法构建轨迹
#include "cartographer/transform/rigid_transform.h"   //刚体变换：平移，旋转等，3D和2D
#include "cartographer/transform/transform.h"     // 姿态角，角-轴和四元数变换； Eigen与proto转换
#include "tf_bridge.h"   //坐标转换--'frame_id'(imu_link)  to  'tracking_frame_'(base_link)
#include "geometry_msgs/Transform.h" // 消息类型的转换：平移+旋转（ROS内部）
#include "geometry_msgs/TransformStamped.h" //+时间戳/坐标系等信息
#include "nav_msgs/OccupancyGrid.h"
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/Imu.h"  //传感器msgs imu
#include "sensor_msgs/LaserScan.h" //LaserScan: per scan
#include "sensor_msgs/MultiEchoLaserScan.h" //类型两者一致
#include "sensor_msgs/PointCloud2.h"

namespace cartographer_ros {
// 将ROS messages 转换为sensor/msgs(tracking frame)坐标系下，调用底层函数
// Converts ROS messages into SensorData in tracking frame for the MapBuilder.
// inter:根据轨迹id 和topic类型构建
class SensorBridge {
 public:
  explicit SensorBridge(
      const string& tracking_frame, double lookup_transform_timeout_sec,
      tf2_ros::Buffer* tf_buffer, // 获取坐标转换关系使用
      ::cartographer::mapping::TrajectoryBuilder* trajectory_builder);//调用底层函数

  SensorBridge(const SensorBridge&) = delete;
  SensorBridge& operator=(const SensorBridge&) = delete;

  void HandleOdometryMessage(const string& sensor_id,
                             const nav_msgs::Odometry::ConstPtr& msg);
  void HandleImuMessage(const string& sensor_id,
                        const sensor_msgs::Imu::ConstPtr& msg);
  void HandleLaserScanMessage(const string& sensor_id,
                              const sensor_msgs::LaserScan::ConstPtr& msg);
  void HandleMultiEchoLaserScanMessage(
      const string& sensor_id,
      const sensor_msgs::MultiEchoLaserScan::ConstPtr& msg);
  void HandlePointCloud2Message(const string& sensor_id, //topic
                                const sensor_msgs::PointCloud2::ConstPtr& msg);

  const TfBridge& tf_bridge() const;

 private:
  void HandleRangefinder(const string& sensor_id,
                         const ::cartographer::common::Time time,
                         const string& frame_id,
                         const ::cartographer::sensor::PointCloud& ranges);

  const TfBridge tf_bridge_; //存储全局的TfBridge
  ::cartographer::mapping::TrajectoryBuilder* const trajectory_builder_;
};

}  // namespace cartographer_ros

#endif  // CARTOGRAPHER_ROS_SENSOR_BRIDGE_H_
