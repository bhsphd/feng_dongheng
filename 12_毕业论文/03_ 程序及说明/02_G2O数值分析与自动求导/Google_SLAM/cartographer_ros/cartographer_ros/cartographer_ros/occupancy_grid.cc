/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "occupancy_grid.h"

#include "cartographer/common/port.h"
#include "cartographer/mapping_2d/probability_grid.h"
#include "cartographer/mapping_2d/range_data_inserter.h"
#include "time_conversion.h"
#include "glog/logging.h"

namespace {
// (1)点云坐标转换，转换到pose坐标系(rangeData)，用于建图;(2)扩展边界
Eigen::AlignedBox2f ComputeMapBoundingBox(
    const std::vector<::cartographer::mapping::TrajectoryNode>&
        trajectory_nodes) { //内存中的数据量不小，Cartographer如何处理？
  Eigen::AlignedBox2f bounding_box(Eigen::Vector2f::Zero());
  for (const auto& node : trajectory_nodes) {
    const auto& data = *node.constant_data; //struct ConstantData//包含点云数据
    ::cartographer::sensor::RangeData range_data; //RangeData(origin; misses; returns)
    // 存在3D数据优先处理3D，否则处理2D
    if (!data.range_data_3d.returns.empty()) {
      // 数据不为空
      range_data = ::cartographer::sensor::TransformRangeData(
          ::cartographer::sensor::Decompress(data.range_data_3d), //解压缩：精度
          node.pose.cast<float>());
    } else {
      // 2D
      range_data = ::cartographer::sensor::TransformRangeData(
          data.range_data_2d, node.pose.cast<float>());//转换到pose坐标系（从tracking坐标系，用于建图）
    }
    bounding_box.extend(range_data.origin.head<2>());
    for (const Eigen::Vector3f& hit : range_data.returns) {
      bounding_box.extend(hit.head<2>());
    }
    for (const Eigen::Vector3f& miss : range_data.misses) {
      bounding_box.extend(miss.head<2>());
    }
  }
  return bounding_box;
}

}  // namespace

namespace cartographer_ros {
// ROS建图流程：2D
void BuildOccupancyGrid2D(
    const std::vector<::cartographer::mapping::TrajectoryNode>&
        trajectory_nodes, //
    const string& map_frame, //get带入
    const ::cartographer::mapping_2d::proto::SubmapsOptions& submaps_options, //
    ::nav_msgs::OccupancyGrid* const occupancy_grid) { //导航功能包
  CHECK(!trajectory_nodes.empty());
  namespace carto = ::cartographer;
  //using carto = ::cartographer;
  const carto::mapping_2d::MapLimits map_limits =
      ComputeMapLimits(submaps_options.resolution(), trajectory_nodes);
  carto::mapping_2d::ProbabilityGrid probability_grid(map_limits); //概率网格
  carto::mapping_2d::RangeDataInserter range_data_inserter(
      submaps_options.range_data_inserter_options()); //proto
  for (const auto& node : trajectory_nodes) {
    //遍历all node
    CHECK(node.constant_data->range_data_3d.returns.empty());  // No 3D yet.
    range_data_inserter.Insert(
        carto::sensor::TransformRangeData(node.constant_data->range_data_2d,
                                          node.pose.cast<float>()),
        &probability_grid);
  } //点云插入概率网格（TODO：重复如何解决）

  // TODO(whess): Compute the latest time of in 'trajectory_nodes'.
//  occupancy_grid->header.stamp = ToRos(trajectory_nodes.back().time());
//  occupancy_grid->header.frame_id = map_frame;
//  occupancy_grid->info.map_load_time = occupancy_grid->header.stamp;
  //latest time is the last trajectory_nodes
  occupancy_grid->header.stamp = ToRos(trajectory_nodes.back().time());
  occupancy_grid->header.frame_id = map_frame;
  occupancy_grid->info.map_load_time = occupancy_grid->header.stamp;

  // 获取offset cell_limits 和 resolution 与导航功能包对接
  Eigen::Array2i offset;
  carto::mapping_2d::CellLimits cell_limits;
  probability_grid.ComputeCroppedLimits(&offset, &cell_limits);
  const double resolution = probability_grid.limits().resolution();

  occupancy_grid->info.resolution = resolution;
  occupancy_grid->info.width = cell_limits.num_y_cells;
  occupancy_grid->info.height = cell_limits.num_x_cells;

  // 不太懂，结合底层源码
  occupancy_grid->info.origin.position.x =
      probability_grid.limits().max().x() -
      (offset.y() + cell_limits.num_y_cells) * resolution;
  occupancy_grid->info.origin.position.y =
      probability_grid.limits().max().y() -
      (offset.x() + cell_limits.num_x_cells) * resolution;
  occupancy_grid->info.origin.position.z = 0.;
  occupancy_grid->info.origin.orientation.w = 1.;
  occupancy_grid->info.origin.orientation.x = 0.;
  occupancy_grid->info.origin.orientation.y = 0.;
  occupancy_grid->info.origin.orientation.z = 0.;

  occupancy_grid->data.resize(cell_limits.num_x_cells * cell_limits.num_y_cells,
                              -1);
  for (const Eigen::Array2i& xy_index :
       carto::mapping_2d::XYIndexRangeIterator(cell_limits)) {
    if (probability_grid.IsKnown(xy_index + offset)) {
      const int value = carto::common::RoundToInt(
          (probability_grid.GetProbability(xy_index + offset) -
           carto::mapping::kMinProbability) *
          100. /
          (carto::mapping::kMaxProbability - carto::mapping::kMinProbability));
      CHECK_LE(0, value);
      CHECK_GE(100, value);
      occupancy_grid->data[(cell_limits.num_x_cells - xy_index.x()) *
                               cell_limits.num_y_cells -
                           xy_index.y() - 1] = value;
    }
  }// 遍历概率网格，附上概率值(以node为单位--全局见图)
}

::cartographer::mapping_2d::MapLimits ComputeMapLimits(
    const double resolution,
    const std::vector<::cartographer::mapping::TrajectoryNode>&
        trajectory_nodes) {
  Eigen::AlignedBox2f bounding_box = ComputeMapBoundingBox(trajectory_nodes);
  // Add some padding to ensure all rays are still contained in the map after
  // discretization. 离散化后，填充，确保所有的rays依然包含在map中。
  const float kPadding = 3.f * resolution; //3倍分辨率：
  bounding_box.min() -= kPadding * Eigen::Vector2f::Ones();
  bounding_box.max() += kPadding * Eigen::Vector2f::Ones();
  const Eigen::Vector2d pixel_sizes =
      bounding_box.sizes().cast<double>() / resolution;//包含多少个像素（2维）
  return ::cartographer::mapping_2d::MapLimits(
      resolution, bounding_box.max().cast<double>(),
      ::cartographer::mapping_2d::CellLimits(
          ::cartographer::common::RoundToInt(pixel_sizes.y()),
          ::cartographer::common::RoundToInt(pixel_sizes.x())));
}

}  // namespace cartographer_ros
