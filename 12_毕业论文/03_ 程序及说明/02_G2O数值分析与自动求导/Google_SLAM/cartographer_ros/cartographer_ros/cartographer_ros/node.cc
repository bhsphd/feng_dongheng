/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "node.h"

#include <chrono>
#include <string>
#include <vector>

#include "Eigen/Core"
#include "cartographer/common/make_unique.h"
#include "cartographer/common/port.h"
#include "cartographer/common/time.h"
#include "cartographer/mapping/proto/submap_visualization.pb.h"
#include "cartographer/mapping/sparse_pose_graph.h"
#include "cartographer/sensor/point_cloud.h"
#include "cartographer/transform/rigid_transform.h"
#include "cartographer/transform/transform.h"
#include "msg_conversion.h"
#include "sensor_bridge.h"
#include "tf_bridge.h"
#include "time_conversion.h"
#include "glog/logging.h"
#include "nav_msgs/Odometry.h"
#include "ros/serialization.h"
#include "sensor_msgs/PointCloud2.h"
#include "tf2_eigen/tf2_eigen.h"

namespace cartographer_ros {

namespace carto = ::cartographer;

using carto::transform::Rigid3d;

constexpr int kLatestOnlyPublisherQueueSize = 1;  //缓存队列为1(实时性)

Node::Node(const NodeOptions& options, tf2_ros::Buffer* const tf_buffer)
    : options_(options), map_builder_bridge_(options_, tf_buffer)  //map_builder_bridge_直接构建
{}

Node::~Node() {
  {
    carto::common::MutexLocker lock(&mutex_);
    terminating_ = true;
  }
  if (occupancy_grid_thread_.joinable()) {
    occupancy_grid_thread_.join(); //
  }
}

// inter: 初始化，发布topic
void Node::Initialize() {
  carto::common::MutexLocker lock(&mutex_); //互斥锁
//inter:01_ submap_list
  submap_list_publisher_ =
      node_handle_.advertise<::cartographer_ros_msgs::SubmapList>(
          kSubmapListTopic, kLatestOnlyPublisherQueueSize); //node_handle_ //发布topic--kSubmapListTopic

  submap_query_server_ = node_handle_.advertiseService(    //node_handle_ //发布服务
      kSubmapQueryServiceName, &Node::HandleSubmapQuery, this);

  // inter:02_ 2D发布map:map
  if (options_.map_builder_options.use_trajectory_builder_2d()) {
    occupancy_grid_publisher_ =
        node_handle_.advertise<::nav_msgs::OccupancyGrid>(
            kOccupancyGridTopic, kLatestOnlyPublisherQueueSize, //发布topic:map（开启线程发布地图）
            true /* latched */);
    occupancy_grid_thread_ =
        std::thread(&Node::SpinOccupancyGridThreadForever, this);
  }

  // inter:03_ 发布scan_matched_points2(实时处理后的点云)
  scan_matched_point_cloud_publisher_ =
      node_handle_.advertise<sensor_msgs::PointCloud2>(   //node_handle_ //发布topic--scan_matched_points2
          kScanMatchedPointCloudTopic, kLatestOnlyPublisherQueueSize);
// inter: 04_ 发布submap数据
  wall_timers_.push_back(node_handle_.createWallTimer(  //node_handle_ //创建定时器，定时发送submaplist
      ::ros::WallDuration(options_.submap_publish_period_sec), //0.3
      &Node::PublishSubmapList, this));

  wall_timers_.push_back(node_handle_.createWallTimer( //node_handle_ //创建定时器，定时发送轨迹状态
      ::ros::WallDuration(options_.pose_publish_period_sec), //周期：5*10-3s
                                                       //定时调用回调函数callback
      &Node::PublishTrajectoryStates, this));
}

::ros::NodeHandle* Node::node_handle() { return &node_handle_; }

MapBuilderBridge* Node::map_builder_bridge() { return &map_builder_bridge_; } //已经初始化

bool Node::HandleSubmapQuery(
    ::cartographer_ros_msgs::SubmapQuery::Request& request,
    ::cartographer_ros_msgs::SubmapQuery::Response& response) {
  carto::common::MutexLocker lock(&mutex_);
  return map_builder_bridge_.HandleSubmapQuery(request, response);
}

void Node::PublishSubmapList(const ::ros::WallTimerEvent& unused_timer_event) {
  carto::common::MutexLocker lock(&mutex_);
  // inter:获取所有优化后的submap
  submap_list_publisher_.publish(map_builder_bridge_.GetSubmapList());
}

// inter:001 important:
// 函数主要获取TrajectoryStates（从map_builder中获取）
void Node::PublishTrajectoryStates(const ::ros::WallTimerEvent& timer_event) {
  carto::common::MutexLocker lock(&mutex_);
// inter 01_ 获取所有轨迹状态；比如2条轨迹点云直接拼接
  for (const auto& entry : map_builder_bridge_.GetTrajectoryStates()) {
    /**********************************************/
    // inter:获取当前时刻实时处理后的点云和姿态: trajectory_state
    const auto& trajectory_state = entry.second;

    // time
    geometry_msgs::TransformStamped stamped_transform;
    stamped_transform.header.stamp = ToRos(trajectory_state.pose_estimate.time);

    // inter:量测更新后的pose：在tracking_frame下
    const auto& tracking_to_local = trajectory_state.pose_estimate.pose;
    const Rigid3d tracking_to_map =
        trajectory_state.local_to_map * tracking_to_local;

    // We only publish a point cloud if it has changed. It is not needed at high
    // frequency, and republishing it would be computationally wasteful. //
    if (trajectory_state.pose_estimate.time !=
        last_scan_matched_point_cloud_time_) {
      scan_matched_point_cloud_publisher_.publish(
            ToPointCloud2Message(
          carto::common::ToUniversal(trajectory_state.pose_estimate.time),
          options_.tracking_frame,
              // inter:发布相对于tracking_frame的坐标（相对坐标）
          carto::sensor::TransformPointCloud(
              trajectory_state.pose_estimate.point_cloud, //当前状态估计的点云 in tracking_frame:已经更新到tracking_frame坐标系下
              tracking_to_local.inverse().cast<float>()))
            );

//      carto::sensor::PointCloud result = carto::sensor::TransformPointCloud(
//            trajectory_state.pose_estimate.point_cloud, //当前状态估计的点云 in tracking_frame:已经更新到tracking_frame坐标系下
//            tracking_to_local.inverse().cast<float>()
//          );
//      LOG(INFO) << "x:=" << result.at(0).x() << " y:=" <<
//                   result.at(0).y() <<
//                   " z:="<< result.at(0).z();
      //LOG(INFO) << "tracking_to_local: " << tracking_to_local.DebugString();// 测试值不变fixed
//      LOG(INFO) << "x:=" << trajectory_state.pose_estimate.point_cloud.at(0).x() << " y:=" <<
//                   trajectory_state.pose_estimate.point_cloud.at(0).y() <<
//                   " z:="<< trajectory_state.pose_estimate.point_cloud.at(0).z();

      last_scan_matched_point_cloud_time_ = trajectory_state.pose_estimate.time;
    } else {
      // If we do not publish a new point cloud, we still allow time of the
      // published poses to advance.
      stamped_transform.header.stamp = ros::Time::now();
    }

    // inter: odom_frame
    if (trajectory_state.published_to_tracking != nullptr) {
      if (options_.provide_odom_frame) {
        std::vector<geometry_msgs::TransformStamped> stamped_transforms;

        stamped_transform.header.frame_id = options_.map_frame;
        // TODO(damonkohler): 'odom_frame' and 'published_frame' must be
        // per-trajectory to fully support the multi-robot use case.
        stamped_transform.child_frame_id = options_.odom_frame;
        stamped_transform.transform =
            ToGeometryMsgTransform(trajectory_state.local_to_map);
        stamped_transforms.push_back(stamped_transform);

        stamped_transform.header.frame_id = options_.odom_frame;
        stamped_transform.child_frame_id = options_.published_frame;
        stamped_transform.transform = ToGeometryMsgTransform(
            tracking_to_local * (*trajectory_state.published_to_tracking));
        stamped_transforms.push_back(stamped_transform);

        tf_broadcaster_.sendTransform(stamped_transforms);
      } else {
        stamped_transform.header.frame_id = options_.map_frame;
        stamped_transform.child_frame_id = options_.published_frame;
        stamped_transform.transform = ToGeometryMsgTransform(
            tracking_to_map * (*trajectory_state.published_to_tracking));
        tf_broadcaster_.sendTransform(stamped_transform);
      }
    }
  }
}

void Node::SpinOccupancyGridThreadForever() {
  for (;;) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1000)); //sleep 1s
    {
      carto::common::MutexLocker lock(&mutex_);
      if (terminating_) { //false:终止
        return;
      }
    }
    if (occupancy_grid_publisher_.getNumSubscribers() == 0) {
      continue; // 订阅者个数=0，结束本次循环
    }
    const auto occupancy_grid = map_builder_bridge_.BuildOccupancyGrid(); //构建2D地图
    if (occupancy_grid != nullptr) {
      occupancy_grid_publisher_.publish(*occupancy_grid); //发布map
    }
  }
}

}  // namespace cartographer_ros
