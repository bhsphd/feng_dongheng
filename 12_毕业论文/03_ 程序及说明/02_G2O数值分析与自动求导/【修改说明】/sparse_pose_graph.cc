/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "cartographer/mapping_3d/sparse_pose_graph.h"

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <sstream>
#include <string>

#include "Eigen/Eigenvalues"
#include "cartographer/common/make_unique.h"
#include "cartographer/common/math.h"
#include "cartographer/mapping/sparse_pose_graph/proto/constraint_builder_options.pb.h"
#include "cartographer/sensor/compressed_point_cloud.h"
#include "cartographer/sensor/voxel_filter.h"
#include "glog/logging.h"

/********************************/
#include <ctime>
/********************************/

namespace cartographer {
namespace mapping_3d {

SparsePoseGraph::SparsePoseGraph(
    const mapping::proto::SparsePoseGraphOptions& options,
    common::ThreadPool* thread_pool) //线程数目
//01_优化问题构建；02_约束构建
    : options_(options),
      optimization_problem_(options_.optimization_problem_options(),
                            sparse_pose_graph::OptimizationProblem::FixZ::kNo), //不固定z
      constraint_builder_(options_.constraint_builder_options(), thread_pool) {}

SparsePoseGraph::~SparsePoseGraph() {
  WaitForAllComputations(); //
  common::MutexLocker locker(&mutex_);
  CHECK(scan_queue_ == nullptr);
}

void SparsePoseGraph::GrowSubmapTransformsAsNeeded(
    const std::vector<const Submap*>& insertion_submaps) {
  CHECK(!insertion_submaps.empty());
  const mapping::SubmapId first_submap_id = GetSubmapId(insertion_submaps[0]);
  const int trajectory_id = first_submap_id.trajectory_id;
  CHECK_GE(trajectory_id, 0);
  const auto& submap_data = optimization_problem_.submap_data();
  if (insertion_submaps.size() == 1) {
    // If we don't already have an entry for the first submap, add one.
    CHECK_EQ(first_submap_id.submap_index, 0);
    if (static_cast<size_t>(trajectory_id) >= submap_data.size() ||
        submap_data[trajectory_id].empty()) {
      optimization_problem_.AddSubmap(trajectory_id,
                                      transform::Rigid3d::Identity());
    }
    return;
  }
  CHECK_EQ(2, insertion_submaps.size());
  const int next_submap_index = submap_data.at(trajectory_id).size();
  // CHECK that we have a index for the second submap.
  const mapping::SubmapId second_submap_id = GetSubmapId(insertion_submaps[1]);
  CHECK_EQ(second_submap_id.trajectory_id, trajectory_id);
  CHECK_LE(second_submap_id.submap_index, next_submap_index);
  // Extrapolate if necessary.
  if (second_submap_id.submap_index == next_submap_index) {
    const auto& first_submap_pose =
        submap_data.at(trajectory_id).at(first_submap_id.submap_index).pose;
    optimization_problem_.AddSubmap(
        trajectory_id, first_submap_pose *
                           insertion_submaps[0]->local_pose.inverse() *
                           insertion_submaps[1]->local_pose); //调整
  }
}

//01_ 当前数据作为node，添加进入轨迹id,并为当前node分配一个nodeId,轨迹数目+1
//02_ 若待匹配submap不存在，则添加进入轨迹id，并为当前submap分配一个submapId
//03_ 判断当前轨迹id是否已经进行采样，若无则采样，采样频率为0.003
//04_ 进入工作队列处理
void SparsePoseGraph::AddScan(
    common::Time time, //时间
        const sensor::RangeData& range_data_in_tracking, //per scan(相对tracking_frame坐标系原点）
    const transform::Rigid3d& pose, //当前载体的POS_预测值
    const kalman_filter::PoseCovariance& covariance,//协方差矩阵
        const int trajectory_id, //轨迹id
    const Submap* const matching_submap, //倒数第2个submap
    const std::vector<const Submap*>& insertion_submaps //倒数第2个和倒数第1个submap
        ) {
    //获取上一时刻local坐标系到global坐标系的变换，乘以当前时刻pose，获得优化后POS的预测值
  const transform::Rigid3d optimized_pose(
      GetLocalToGlobalTransform(trajectory_id) * pose);

  // {scan, pose}->trajectory_node
  common::MutexLocker locker(&mutex_);
  // inter:点云
  constant_node_data_.push_back(mapping::TrajectoryNode::ConstantData{
      time, sensor::RangeData{Eigen::Vector3f::Zero(), {}, {}},
      sensor::Compress(range_data_in_tracking), trajectory_id, //当前帧点云，轨迹id
      transform::Rigid3d::Identity()}); //POS默认是单位阵

  // inter：return // NodeId
//  int trajectory_id; //轨迹index
//  int node_index; //一共包含多少个节点
  trajectory_nodes_.Append(
      trajectory_id, //
      mapping::TrajectoryNode{&constant_node_data_.back(), optimized_pose}); //轨迹node(数据+POS)

  ++num_trajectory_nodes_;
  trajectory_connectivity_.Add(trajectory_id);

  // inter:无当前submap，创建一个，并添加进入submap_states_和submap_ids_
  if (submap_ids_.count(insertion_submaps.back()) == 0) {
    //inter:为当前submap创建submapId
    const mapping::SubmapId submap_id =
        submap_states_.Append(trajectory_id, SubmapState()); //空则创建，return new
    //inter：返回submapState
    submap_ids_.emplace(insertion_submaps.back(), submap_id);
    submap_states_.at(submap_id).submap = insertion_submaps.back();
  }

  // inter: 三目表达式，若insertion_submaps.front()->finished=true，则返回insertion_submaps.front()
  //否则return nullptr.

  // inter:获取两个submap中已经完成的submap，也就是第一个，若第一个都没有完成，则就没有完成的，用于判断
  const Submap* const finished_submap =
      insertion_submaps.front()->finished ? insertion_submaps.front() : nullptr;

  // Make sure we have a sampler for this trajectory. //若当前轨迹id没有进行采样，则采样。采样频率0.003
  if (!global_localization_samplers_[trajectory_id]) {
    global_localization_samplers_[trajectory_id] =
        common::make_unique<common::FixedRatioSampler>(
            options_.global_sampling_ratio()); //以固定采样率对node采样 0.003
  }




  /********************************************************************************************/
  // inter：callback，添加当前数据进入工作队列，并调用函数ComputeConstraintsForScan进行处理
  AddWorkItem([=]() REQUIRES(mutex_) {
    ComputeConstraintsForScan(matching_submap, //倒数第2个
                              insertion_submaps,//倒数第2个和倒数第1个submap
                              finished_submap, //倒数第2个(已完成)
                              pose, //载体的POS
                              covariance); //finished_submap为此轨迹的第一个submap
  }); //执行addWorkItem()函数

  /********************************************************************************************/
  /********************************************************************************************/
}

void SparsePoseGraph::AddWorkItem(std::function<void()> work_item) {
    // 若队列中回调函数为空，则调用callback；若队列中callback不为空，则添加当前的进入work_item;
  if (scan_queue_ == nullptr) {
    work_item();
  } else {
    scan_queue_->push_back(work_item);
  }
}

void SparsePoseGraph::AddImuData(const int trajectory_id, common::Time time,
                                 const Eigen::Vector3d& linear_acceleration,
                                 const Eigen::Vector3d& angular_velocity) {
  common::MutexLocker locker(&mutex_);
  AddWorkItem([=]() REQUIRES(mutex_) {
    optimization_problem_.AddImuData(trajectory_id, time, linear_acceleration,
                                     angular_velocity);
  });
}
// inter:01_ 计算约束//node与所有的submap_id
void SparsePoseGraph::ComputeConstraint(const mapping::NodeId& node_id,
                                        const mapping::SubmapId& submap_id) {
  const transform::Rigid3d inverse_submap_pose =
      optimization_problem_.submap_data()
          .at(submap_id.trajectory_id)
          .at(submap_id.submap_index)
          .pose.inverse(); //submap_pose的逆

  const transform::Rigid3d initial_relative_pose =
      inverse_submap_pose * optimization_problem_.node_data()
                                .at(node_id.trajectory_id)
                                .at(node_id.node_index)
                                .point_cloud_pose; //submap与node的边

  std::vector<mapping::TrajectoryNode> submap_nodes; //all nodes
  for (const mapping::NodeId& submap_node_id :
       submap_states_.at(submap_id).node_ids)
      //遍历submap_id包含的所有node
  {
    submap_nodes.push_back(mapping::TrajectoryNode{
        trajectory_nodes_.at(submap_node_id).constant_data, //点云数据
        inverse_submap_pose * trajectory_nodes_.at(submap_node_id).pose});
  } //submap中节点的pose_ 与submap_pose间的关系(边)

  // Only globally match against submaps not in this trajectory.
  if (node_id.trajectory_id != submap_id.trajectory_id &&
      global_localization_samplers_[node_id.trajectory_id]->Pulse()) {
    // In this situation, 'initial_relative_pose' is:
    // 不同轨迹间的submap与node，调用Global约束
    // submap <- global map 2 <- global map 1 <- tracking
    //               (agreeing on gravity)
    //
    // Since they possibly came from two disconnected trajectories, the only
    // guaranteed connection between the tracking and the submap frames is
    // an agreement on the direction of gravity. Therefore, excluding yaw,
    // 'initial_relative_pose.rotation()' is a good estimate of the relative
    // orientation of the point cloud in the submap frame. Finding the correct
    // yaw component will be handled by the matching procedure in the
    // FastCorrelativeScanMatcher, and the given yaw is essentially ignored.//回环检测
      // submap与node可能来自两条不相连的轨迹，所以tracking_frame和submap_frame间能够连接的保证是
      // 重力方向一致，因此，node和submap的匹配仅需要在yaw方向生成角度。
    constraint_builder_.MaybeAddGlobalConstraint(
        submap_id, submap_states_.at(submap_id).submap, node_id,
        &trajectory_nodes_.at(node_id).constant_data->range_data_3d.returns,
        submap_nodes, initial_relative_pose.rotation(),
        &trajectory_connectivity_);
  } else {
      //连通的轨迹
    const bool scan_and_submap_trajectories_connected =
        reverse_connected_components_.count(node_id.trajectory_id) > 0 &&
        reverse_connected_components_.count(submap_id.trajectory_id) > 0 &&
        reverse_connected_components_.at(node_id.trajectory_id) ==
            reverse_connected_components_.at(submap_id.trajectory_id);
    if (node_id.trajectory_id == submap_id.trajectory_id ||
        scan_and_submap_trajectories_connected) {
        //轨迹联通(一条轨迹，回环检测)
      constraint_builder_.MaybeAddConstraint(
          submap_id, //submap的索引号
                  submap_states_.at(submap_id).submap, //submap
                  node_id, //节点的索引号
          &trajectory_nodes_.at(node_id).constant_data->range_data_3d.returns, //压缩后点云
          submap_nodes, //submap对应的所有的nodes
                  initial_relative_pose //初始化的POS
                  );
    }
  }
}
// inter:02_
void SparsePoseGraph::ComputeConstraintsForOldScans(const Submap* submap) {
  const auto submap_id = GetSubmapId(submap);
  const auto& submap_state = submap_states_.at(submap_id);

  const auto& node_data = optimization_problem_.node_data();
  for (size_t trajectory_id = 0; trajectory_id != node_data.size();
       ++trajectory_id) {
    for (size_t node_index = 0; node_index != node_data[trajectory_id].size();
         ++node_index) {
      const mapping::NodeId node_id{static_cast<int>(trajectory_id),
                                    static_cast<int>(node_index)};
      if (submap_state.node_ids.count(node_id) == 0) {
        ComputeConstraint(node_id, submap_id);
      }
    }
  }
}

// inter:03_ 程序入口 //点云数据处理核心函数（计算约束）//inter:00_
void SparsePoseGraph::ComputeConstraintsForScan(
    const Submap* matching_submap, //倒数第2个
    std::vector<const Submap*> insertion_submaps, //倒数第2个和倒数第1个
    const Submap* finished_submap,
        const transform::Rigid3d& pose, //per scan的pose
    const kalman_filter::PoseCovariance& covariance) {
    //
  GrowSubmapTransformsAsNeeded(insertion_submaps);
  //inter：倒数第2个submap的ID(轨迹+submap的索引号)
  const mapping::SubmapId matching_id = GetSubmapId(matching_submap);
  //inter：当前节点的pose
  const transform::Rigid3d optimized_pose =
      optimization_problem_.submap_data()
          .at(matching_id.trajectory_id)
          .at(matching_id.submap_index)
          .pose * //上一时刻submap的最优估计值
      matching_submap->local_pose.inverse() * pose;
    //当前时刻submapPOS的估计值//误差//

  /******************测试****************
  LOG(INFO) << optimized_pose; //测试结果：POS值
  LOG(INFO) << "pose:" << pose; //测试结果：POS值//
  //多个submap后，两者的值不一致。
  LOG(INFO) << matching_id.submap_index; //索引号落后1个;开始是0,
  LOG(INFO) <<       optimization_problem_.submap_data()
                     .at(matching_id.trajectory_id)
                     .at(matching_id.submap_index)
                     .pose; //
  //transform::Rigid3d lp = matching_submap->local_pose.inverse() * pose;
  //LOG(INFO) << matching_submap->local_pose.inverse() * pose;

  if(finished_submap != nullptr){
      const mapping::SubmapId matching_id_f = GetSubmapId(finished_submap);
      LOG(INFO) << "matching_id_f:"<< matching_id_f.submap_index;
  }
  LOG(INFO) << "matching_id:"<< matching_id.submap_index; //索引号落后1个
    LOG(INFO) << optimization_problem_.node_data().size(); //轨迹条数：只有一条轨迹
    LOG(INFO) << matching_id.trajectory_id ; //轨迹id一直是0

    LOG(INFO) << optimization_problem_.node_data()
                 .at(matching_id.trajectory_id)
                 .size();
    size_t m = static_cast<size_t>(matching_id.trajectory_id) <
            optimization_problem_.node_data().size()
        ? static_cast<int>(optimization_problem_.node_data()
                               .at(matching_id.trajectory_id)
                               .size())
        : 0;

    LOG(INFO) << m ; //每帧算一个node
  /*****************测试******************/

  /********************************01_ 添加轨迹节点********************************************/
  // inter:获取当前节点ID(从属于哪个轨迹id中的哪个submapId)
  const mapping::NodeId node_id{
      matching_id.trajectory_id,
      static_cast<size_t>(matching_id.trajectory_id) <
              optimization_problem_.node_data().size()
          ? static_cast<int>(optimization_problem_.node_data()
                                 .at(matching_id.trajectory_id)
                                 .size())
          : 0};

// inter：通过nodeID获取点云(点云存储在_TrajectoryNode_中)
  const mapping::TrajectoryNode::ConstantData* const scan_data =
      trajectory_nodes_.at(node_id).constant_data; //点云
  CHECK_EQ(scan_data->trajectory_id, matching_id.trajectory_id);

  // inter:为优化问题_ 添加节点(time,POS)
  optimization_problem_.AddTrajectoryNode(matching_id.trajectory_id,
                                          scan_data->time, optimized_pose);
  /****************************************************************************/


  /**********************************02_ 添加约束：constraints_*********************************/
  // inter:遍历最后两个submap
  for (const Submap* submap : insertion_submaps) {
    const mapping::SubmapId submap_id = GetSubmapId(submap); //获取submap轨迹ID和索引号
    CHECK(!submap_states_.at(submap_id).finished);
    //LOG(INFO) << submap_id.submap_index;

    submap_states_.at(submap_id).node_ids.emplace(node_id); //两个submap均添加node_id
    // Unchanged covariance as (submap <- map) is a translation.
    const transform::Rigid3d constraint_transform =
        submap->local_pose.inverse() * pose; //边：扫描匹配结果
    //LOG(INFO) <<  "constraint_transform: " << submap_id.submap_index <<": " << constraint_transform;
//测试证明：以submap的首帧POS作为整个submap的POS
    constraints_.push_back(
        Constraint{submap_id,
                   node_id,
                   {constraint_transform, //边的pose
                    common::ComputeSpdMatrixSqrtInverse( //信息矩阵 A^-(1/2)
                        covariance, options_.constraint_builder_options()
                                        .lower_covariance_eigenvalue_bound())},//当前状态的协方差矩阵
                   Constraint::INTRA_SUBMAP});
    //添加约束
  }

  //==回环检测，添加优化==
  // inter: 遍历所有的submap，添加约束(计算约束)
  for (int trajectory_id = 0; trajectory_id < submap_states_.num_trajectories();
       ++trajectory_id) {
      // inter：遍历所有轨迹
    for (int submap_index = 0;
         submap_index < submap_states_.num_indices(trajectory_id); //轨迹中包含的submap数目
         ++submap_index) {
        // inter:遍历轨迹的submap

      const mapping::SubmapId submap_id{trajectory_id, submap_index};
      if (submap_states_.at(submap_id).finished) { //submap已经完成
        CHECK_EQ(submap_states_.at(submap_id).node_ids.count(node_id), 0);
        // inter: 若当前submap已经完成，计算(node_id, submap_id)间的约束
        ComputeConstraint(node_id, submap_id);
      }
    }
    //submap已经完成，则计算约束
  }
//添加新完成的submap的约束
  if (finished_submap != nullptr) {
    const mapping::SubmapId finished_submap_id = GetSubmapId(finished_submap);
    SubmapState& finished_submap_state = submap_states_.at(finished_submap_id);
    CHECK(!finished_submap_state.finished);
    // We have a new completed submap, so we look into adding constraints for
    // old scans. //新的完成的submap，添加约束
    ComputeConstraintsForOldScans(finished_submap);
    finished_submap_state.finished = true;
  }
  /****************************************************************************/

   /*****************************03_ 执行图优化***********************************************/
  constraint_builder_.NotifyEndOfScan();
  ++num_scans_since_last_loop_closure_;
  // 节点数目大于多少时，执行优化
  if (options_.optimize_every_n_scans() > 0 &&
      num_scans_since_last_loop_closure_ > options_.optimize_every_n_scans()) {
    CHECK(!run_loop_closure_);
    run_loop_closure_ = true;
    // If there is a 'scan_queue_' already, some other thread will take care.
    if (scan_queue_ == nullptr) {
      scan_queue_ = common::make_unique<std::deque<std::function<void()>>>(); //执行绑定的函数
      HandleScanQueue(); //inter:处理扫描队列函数
    }
  }
}

  // inter:04_ 执行图优化
void SparsePoseGraph::HandleScanQueue() {
    constraint_builder_.WhenDone(
      [this](const sparse_pose_graph::ConstraintBuilder::Result& result) {
        //将result的约束放入constraints_中//后插
        //调用次函数处理
        {
          common::MutexLocker locker(&mutex_);
          constraints_.insert(constraints_.end(), result.begin(), result.end());
          //插入一些result的约束
        }
      // inter:05_执行优化//图优化流程
        RunOptimization();

        common::MutexLocker locker(&mutex_);
        num_scans_since_last_loop_closure_ = 0; //标识置为0//320个scan处理一次，后端
        run_loop_closure_ = false;
        while (!run_loop_closure_) {
          if (scan_queue_->empty()) {
            LOG(INFO) << "We caught up. Hooray!"; //处理成功//2个submap打印一次
            scan_queue_.reset();
            return;
          }
          scan_queue_->front()();
          scan_queue_->pop_front();
        }
        // We have to optimize again. //检查
        HandleScanQueue();
      });
}

// inter:清理工作
void SparsePoseGraph::WaitForAllComputations() {
  bool notification = false;
  common::MutexLocker locker(&mutex_);
  const int num_finished_scans_at_start =
      constraint_builder_.GetNumFinishedScans(); // 已经完成的scans

  while (!locker.AwaitWithTimeout(
      [this]() REQUIRES(mutex_) {
        return constraint_builder_.GetNumFinishedScans() ==
               num_trajectory_nodes_; //
      },
      common::FromSeconds(1.))) {
    std::ostringstream progress_info;
    progress_info << "Optimizing: " << std::fixed << std::setprecision(1)
                  << 100. *
                         (constraint_builder_.GetNumFinishedScans() -
                          num_finished_scans_at_start) /
                         (num_trajectory_nodes_ - num_finished_scans_at_start)
                  << "%..."; //完成了百分之多少
    std::cout << "\r\x1b[K" << progress_info.str() << std::flush;
  }
  std::cout << "\r\x1b[KOptimizing: Done.     " << std::endl;
  constraint_builder_.WhenDone(
      [this, &notification](
          const sparse_pose_graph::ConstraintBuilder::Result& result) {
      // inter:约束容器后插：(插入result的所有约束)
        common::MutexLocker locker(&mutex_);
        constraints_.insert(constraints_.end(), result.begin(), result.end());
        notification = true; //标识
      });
  locker.Await([&notification]() { return notification; }); //锁定-阻塞队列
}

void SparsePoseGraph::RunFinalOptimization() {
  WaitForAllComputations();
  optimization_problem_.SetMaxNumIterations(
      options_.max_num_final_iterations());
  RunOptimization();
  optimization_problem_.SetMaxNumIterations(
      options_.optimization_problem_options()
          .ceres_solver_options()
          .max_num_iterations());
}

// inter:key function // inter:05_执行优化 //优化流程
// inter:图优化流程
void SparsePoseGraph::RunOptimization() {
  if (optimization_problem_.submap_data().empty()) {
    return; //无submap数据
  }

  /********************************/
  //clock_t time_ttt;
  //time_ttt = clock(); //当前时刻系统时间
  /********************************/
  /**********************************************************/
  optimization_problem_.Solve(constraints_); //优化后获得新的submap的POS
  /**********************************************************/
  /********************************/
    const auto& node_data = optimization_problem_.node_data();
//  // 计算节点总数
//  int node_index_new = 0;
//  for (int trajectory_id = 0;
//       trajectory_id != static_cast<int>(node_data.size()); ++trajectory_id) {
//      // 遍历所有轨迹
//    const int num_nodes_new = trajectory_nodes_.num_indices(trajectory_id); // 轨迹ID的node数目
//    node_index_new += num_nodes_new;
//  }
//  LOG(INFO)<< "time:" << 1000*(clock() - time_ttt)/(double)CLOCKS_PER_SEC << "ms" << " node_num:" << node_index_new;; //时间间隔

  /********************************/
  common::MutexLocker locker(&mutex_);


  for (int trajectory_id = 0;
       trajectory_id != static_cast<int>(node_data.size()); ++trajectory_id) {
      // 遍历所有轨迹
    int node_index = 0;
    const int num_nodes = trajectory_nodes_.num_indices(trajectory_id); // 轨迹ID的node数目
    for (; node_index != static_cast<int>(node_data[trajectory_id].size());
         ++node_index) {
      const mapping::NodeId node_id{trajectory_id, node_index};
      trajectory_nodes_.at(node_id).pose =
          node_data[trajectory_id][node_index].point_cloud_pose; //节点的POS
    }
    // Extrapolate all point cloud poses that were added later. //提取所有的submap_POS//最后一个
    const auto new_submap_transforms = ExtrapolateSubmapTransforms(
        optimization_problem_.submap_data(), trajectory_id);

    const auto old_submap_transforms = ExtrapolateSubmapTransforms(
        optimized_submap_transforms_, trajectory_id); //老的

    CHECK_EQ(new_submap_transforms.size(), old_submap_transforms.size());
    //error
    const transform::Rigid3d extrapolation_transform =
        new_submap_transforms.back() * old_submap_transforms.back().inverse(); //新的最后一个与老的最后一个submap的差别

    for (; node_index < num_nodes; ++node_index) {
        //遍历所有的node
      const mapping::NodeId node_id{trajectory_id, node_index};
      trajectory_nodes_.at(node_id).pose =
          extrapolation_transform * trajectory_nodes_.at(node_id).pose; //调整轨迹所有节点的POS
    }
  }
  optimized_submap_transforms_ = optimization_problem_.submap_data();
  connected_components_ = trajectory_connectivity_.ConnectedComponents();
  reverse_connected_components_.clear();
  for (size_t i = 0; i != connected_components_.size(); ++i) {
    for (const int trajectory_id : connected_components_[i]) {
      reverse_connected_components_.emplace(trajectory_id, i);
    }
  }
}

// inter:获取轨迹所有的节点
std::vector<std::vector<mapping::TrajectoryNode>>
SparsePoseGraph::GetTrajectoryNodes() {
  common::MutexLocker locker(&mutex_);
  return trajectory_nodes_.data(); //所有轨迹的所有节点数据
}

std::vector<SparsePoseGraph::Constraint> SparsePoseGraph::constraints() {
  common::MutexLocker locker(&mutex_);
  return constraints_;
}

// inter：返回当前轨迹最后一个submap与global坐标系间的关系
// 算法//获得当前POS与优化后POS的差距
transform::Rigid3d SparsePoseGraph::GetLocalToGlobalTransform(
    const int trajectory_id) {
  common::MutexLocker locker(&mutex_);
  // inter:无当前轨迹 or 当前轨迹的submap数目为0，返回单位阵
  if (submap_states_.num_trajectories() <= trajectory_id ||
      submap_states_.num_indices(trajectory_id) == 0) {
    return transform::Rigid3d::Identity();
  }
  // 提取所有优化后的submap pose
  const auto extrapolated_submap_transforms =
      ExtrapolateSubmapTransforms(optimized_submap_transforms_, trajectory_id);

  // 所有优化后的POS*其对应的local坐标系下的POS的逆，获得global和local坐标系间的位姿关系
  return extrapolated_submap_transforms.back() *
         submap_states_
             .at(mapping::SubmapId{   //这一串表示submap的索引号
                 trajectory_id,
                 static_cast<int>(extrapolated_submap_transforms.size()) - 1})
             .submap->local_pose.inverse(); //最后一个submap的逆
}

std::vector<std::vector<int>> SparsePoseGraph::GetConnectedTrajectories() {
  common::MutexLocker locker(&mutex_);
  return connected_components_;
}

std::vector<transform::Rigid3d> SparsePoseGraph::GetSubmapTransforms(
    const int trajectory_id) {
  common::MutexLocker locker(&mutex_);
  // 获取所有优化后submap的POS(Local坐标系下)：图优化之后
  return ExtrapolateSubmapTransforms(optimized_submap_transforms_,
                                     trajectory_id);
}
// 所有的submap数据均可通过submap_states获取
std::vector<transform::Rigid3d> SparsePoseGraph::ExtrapolateSubmapTransforms(
    const std::vector<std::vector<sparse_pose_graph::SubmapData>>&
        submap_transforms,
    const int trajectory_id) const {
  if (trajectory_id >= submap_states_.num_trajectories()) {
    return {transform::Rigid3d::Identity()};
  }

  // Submaps for which we have optimized poses.
  std::vector<transform::Rigid3d> result; //用于存储一系列 submap pose
  for (int submap_index = 0;
       submap_index != submap_states_.num_indices(trajectory_id); //submap的数目
       ++submap_index) {
      // 遍历所有的submap
    const mapping::SubmapId submap_id{trajectory_id, submap_index};
    const auto& state = submap_states_.at(submap_id); //
    if (static_cast<size_t>(trajectory_id) < submap_transforms.size() &&
        result.size() < submap_transforms.at(trajectory_id).size()) {
      // Submaps for which we have optimized poses.
      result.push_back(
          submap_transforms.at(trajectory_id).at(result.size()).pose); //按照序号插入
    } else if (result.empty()) {
      result.push_back(transform::Rigid3d::Identity());
    } else {
      // Extrapolate to the remaining submaps. Accessing 'local_pose' in Submaps
      // is okay, since the member is const.
      const mapping::SubmapId previous_submap_id{
          trajectory_id, static_cast<int>(result.size()) - 1}; //上一个submap_id
      result.push_back(
          result.back() *
          submap_states_.at(previous_submap_id).submap->local_pose.inverse() *
          state.submap->local_pose); //倒数第2个subamp与倒数第一个submap间的边。
    }
  }

  if (result.empty()) {
    result.push_back(transform::Rigid3d::Identity());
  }
  return result;
}

}  // namespace mapping_3d
}  // namespace cartographer
