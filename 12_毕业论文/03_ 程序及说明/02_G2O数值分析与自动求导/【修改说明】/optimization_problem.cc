/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "cartographer/mapping_3d/sparse_pose_graph/optimization_problem.h"

#include <algorithm>
#include <array>
#include <cmath>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "Eigen/Core"
#include "cartographer/common/ceres_solver_options.h"
#include "cartographer/common/make_unique.h"
#include "cartographer/common/math.h"
#include "cartographer/common/time.h"
#include "cartographer/mapping_3d/acceleration_cost_function.h"
#include "cartographer/mapping_3d/ceres_pose.h"
#include "cartographer/mapping_3d/rotation_cost_function.h"
#include "cartographer/mapping_3d/sparse_pose_graph/spa_cost_function.h"
#include "cartographer/transform/transform.h"
#include "ceres/ceres.h"
#include "ceres/jet.h"
#include "ceres/rotation.h"
#include "glog/logging.h"

namespace cartographer {
namespace mapping_3d {
namespace sparse_pose_graph {

namespace {

// inter：绕z轴旋转，在完成的map中不改变沿z轴的数值
struct ConstantYawQuaternionPlus {
  template <typename T>
  bool operator()(const T* x, const T* delta, T* x_plus_delta) const {
      // delta转化为四元数，调用函数计算x_plus_delta
    const T delta_norm =
        ceres::sqrt(common::Pow2(delta[0]) + common::Pow2(delta[1]));
    const T sin_delta_over_delta =
        delta_norm < 1e-6 ? T(1.) : ceres::sin(delta_norm) / delta_norm;
    T q_delta[4];
    q_delta[0] = delta_norm < 1e-6 ? T(1.) : ceres::cos(delta_norm);
    q_delta[1] = sin_delta_over_delta * delta[0];
    q_delta[2] = sin_delta_over_delta * delta[1];
    q_delta[3] = T(0.);
    // We apply the 'delta' which is interpreted as an angle-axis rotation
    // vector in the xy-plane of the submap frame. This way we can align to // inter:绕z轴旋转将不改变重力方向
    // gravity because rotations around the z-axis in the submap frame do not
    // change gravity alignment, while disallowing random rotations of the map // inter: 重力方向不改变
    // that have nothing to do with gravity alignment (i.e. we disallow steps
    // just changing "yaw" of the complete map). //例如：在完成的map中，我们不改变z轴方向(运动约束)
    ceres::QuaternionProduct(x, q_delta, x_plus_delta);
    return true;
  }
};

}  // namespace

OptimizationProblem::OptimizationProblem(
    const mapping::sparse_pose_graph::proto::OptimizationProblemOptions&
        options,
    FixZ fix_z) //固定z轴
    : options_(options), fix_z_(fix_z) {}

OptimizationProblem::~OptimizationProblem() {}

// inter:添加imu数据
void OptimizationProblem::AddImuData(const int trajectory_id,
                                     const common::Time time,
                                     const Eigen::Vector3d& linear_acceleration,
                                     const Eigen::Vector3d& angular_velocity) {
  CHECK_GE(trajectory_id, 0); //first 轨迹为0号
  // inter:防止内存溢出，重置为轨迹条数
  imu_data_.resize(
      std::max(imu_data_.size(), static_cast<size_t>(trajectory_id) + 1));
  // 第i条轨迹添加imu数据
  imu_data_[trajectory_id].push_back(
      ImuData{time, linear_acceleration, angular_velocity});
}
// inter:添加轨迹Node
void OptimizationProblem::AddTrajectoryNode(
    const int trajectory_id, const common::Time time,
    const transform::Rigid3d& point_cloud_pose) {
  CHECK_GE(trajectory_id, 0);
    // inter:防止内存溢出，重置为轨迹数目
  node_data_.resize(
      std::max(node_data_.size(), static_cast<size_t>(trajectory_id) + 1));
  node_data_[trajectory_id].push_back(NodeData{time, point_cloud_pose});
}

// inter: 添加submap的pose
void OptimizationProblem::AddSubmap(const int trajectory_id,
                                    const transform::Rigid3d& submap_pose) {
  CHECK_GE(trajectory_id, 0);
  submap_data_.resize(
      std::max(submap_data_.size(), static_cast<size_t>(trajectory_id) + 1));
  submap_data_[trajectory_id].push_back(SubmapData{submap_pose});
}

void OptimizationProblem::SetMaxNumIterations(const int32 max_num_iterations) {
  options_.mutable_ceres_solver_options()->set_max_num_iterations(
      max_num_iterations);//max_num_iterations=10
}
// inter:求解，参数为所有约束
void OptimizationProblem::Solve(const std::vector<Constraint>& constraints) {
  if (node_data_.empty()) {
    // Nothing to optimize. //无节点
    return;
  }

  ceres::Problem::Options problem_options;
  ceres::Problem problem(problem_options);

  // inter:固定z
  const auto translation_parameterization =
      [this]() -> std::unique_ptr<ceres::LocalParameterization> {
    return fix_z_ == FixZ::kYes
               ? common::make_unique<ceres::SubsetParameterization>(
                     3, std::vector<int>{2}) //保存整个参数的子集//前2个
               : nullptr;
  };

  // Set the starting point. //submap_data_[0] //设置图优化起点
  CHECK(!submap_data_.empty());
  CHECK(!submap_data_[0].empty());

  // TODO(hrapp): Move ceres data into SubmapData.//CeresPose:translation + rotation //ceres
  std::vector<std::deque<CeresPose>> C_submaps(submap_data_.size()); //
  std::vector<std::deque<CeresPose>> C_nodes(node_data_.size()); //

  // inter：遍历轨迹的所有submap//MOVE to Ceres
  for (size_t trajectory_id = 0; trajectory_id != submap_data_.size();
       ++trajectory_id) {
      // inter:遍历所有轨迹
    for (size_t submap_index = 0;
         submap_index != submap_data_[trajectory_id].size(); ++submap_index) {
        // inter:遍历所有的submap
      if (trajectory_id == 0 && submap_index == 0) {
        // Tie the first submap of the first trajectory to the origin.
        C_submaps[trajectory_id].emplace_back(
            transform::Rigid3d::Identity(), translation_parameterization(),
            common::make_unique<ceres::AutoDiffLocalParameterization<
                ConstantYawQuaternionPlus, 4, 2>>(),
            &problem);

        problem.SetParameterBlockConstant(
            C_submaps[trajectory_id].back().translation());
      } else {
        C_submaps[trajectory_id].emplace_back(
            submap_data_[trajectory_id][submap_index].pose,
            translation_parameterization(),
            common::make_unique<ceres::QuaternionParameterization>(), &problem);
      } //MOVE to CeresPose
    }
  }

  // inter:遍历轨迹所有node// MOVE to Ceres
  for (size_t trajectory_id = 0; trajectory_id != node_data_.size();
       ++trajectory_id) {
    for (size_t node_index = 0; node_index != node_data_[trajectory_id].size();
         ++node_index) {
      C_nodes[trajectory_id].emplace_back(
          node_data_[trajectory_id][node_index].point_cloud_pose, //node_pose
          translation_parameterization(),
          common::make_unique<ceres::QuaternionParameterization>(), &problem);
    }
  }

  // Add cost functions for intra- and inter-submap constraints.
  // inter:内部和外部约束//传统误差函数：测量值和submap_和node的POS值
  for (const Constraint& constraint : constraints) {
      // cost function:估计结果：submap和node的POS
    problem.AddResidualBlock(
        new ceres::AutoDiffCostFunction<SpaCostFunction, 6, 4, 3, 4, 3>(
            new SpaCostFunction(constraint.pose)), //误差函数
        // Only loop closure constraints should have a loss function.// loss function
        constraint.tag == Constraint::INTER_SUBMAP
            ? new ceres::HuberLoss(options_.huber_scale()) //10
            : nullptr, //Huber函数
        C_submaps.at(constraint.submap_id.trajectory_id) //对应的submapId旋转_值
            .at(constraint.submap_id.submap_index)
            .rotation(), //参数1：维数为4
        C_submaps.at(constraint.submap_id.trajectory_id) //对应的submapId平移_值
            .at(constraint.submap_id.submap_index)
            .translation(), //参数2：维数为3
        C_nodes.at(constraint.node_id.trajectory_id) //对应的nodeId旋转_值
            .at(constraint.node_id.node_index)
            .rotation(), //参数3：维数为4
        C_nodes.at(constraint.node_id.trajectory_id) //对应的nodeId平移_值
            .at(constraint.node_id.node_index)
            .translation());//参数4：维数为3
  }

  // Add constraints based on IMU observations of angular velocities and
  // linear acceleration. //添加IMU约束
  // 相邻两个节点不要太远//平移和旋转//定义两个比例因子//增强鲁棒性
  for (size_t trajectory_id = 0; trajectory_id != node_data_.size();
       ++trajectory_id) {
    const std::deque<ImuData>& imu_data = imu_data_.at(trajectory_id); //轨迹的所有IMU数据
    CHECK(!imu_data.empty());
    // TODO(whess): Add support for empty trajectories.
    const auto& node_data = node_data_[trajectory_id];
    CHECK(!node_data.empty());

    // Skip IMU data before the first node of this trajectory.
    auto it = imu_data.cbegin();
    while ((it + 1) != imu_data.cend() && (it + 1)->time <= node_data[0].time) {
      ++it;
    }

    for (size_t node_index = 1; node_index < node_data.size(); ++node_index) {
        //遍历轨迹所有的node
      auto it2 = it;
      const IntegrateImuResult<double> result =
          IntegrateImu(imu_data, node_data[node_index - 1].time,
                       node_data[node_index].time, &it); //计算相邻两个imu的变化

      if (node_index + 1 < node_data.size()) {
        const common::Time first_time = node_data[node_index - 1].time;
        const common::Time second_time = node_data[node_index].time;
        const common::Time third_time = node_data[node_index + 1].time; //中间时刻

        const common::Duration first_duration = second_time - first_time;
        const common::Duration second_duration = third_time - second_time;
        const common::Time first_center = first_time + first_duration / 2; //first时间和second时间_中间时刻
        const common::Time second_center = second_time + second_duration / 2; //second时间和third时间_中间时刻

        const IntegrateImuResult<double> result_to_first_center =
            IntegrateImu(imu_data, first_time, first_center, &it2);

        const IntegrateImuResult<double> result_center_to_center =
            IntegrateImu(imu_data, first_center, second_center, &it2);

        // 'delta_velocity' is the change in velocity from the point in time
        // halfway between the first and second poses to halfway between second
        // and third pose. It is computed from IMU data and still contains a
        // delta due to gravity. The orientation of this vector is in the IMU
        // frame at the second pose.

        const Eigen::Vector3d delta_velocity =
            (result.delta_rotation.inverse() *
             result_to_first_center.delta_rotation) *
            result_center_to_center.delta_velocity;

        problem.AddResidualBlock(
            new ceres::AutoDiffCostFunction<AccelerationCostFunction, 3, 4, 3,
                                            3, 3, 1>(
                new AccelerationCostFunction(
                    options_.acceleration_weight(), delta_velocity,
                    common::ToSeconds(first_duration),
                    common::ToSeconds(second_duration))),
            nullptr, C_nodes[trajectory_id].at(node_index).rotation(), //
            C_nodes[trajectory_id].at(node_index - 1).translation(),
            C_nodes[trajectory_id].at(node_index).translation(),
            C_nodes[trajectory_id].at(node_index + 1).translation(),
            &gravity_constant_);
      }
      problem.AddResidualBlock(
          new ceres::AutoDiffCostFunction<RotationCostFunction, 3, 4, 4>(
              new RotationCostFunction(options_.rotation_weight(),//3e6大
                                       result.delta_rotation)), //旋转值和权重
          nullptr, C_nodes[trajectory_id].at(node_index - 1).rotation(), //相邻两个节点的pose
          C_nodes[trajectory_id].at(node_index).rotation());
    }
  }

  // Solve.
  ceres::Solver::Summary summary;
  ceres::Solve(
      common::CreateCeresSolverOptions(options_.ceres_solver_options()),
      &problem, &summary);

  if (options_.log_solver_summary()) {
    LOG(INFO) << summary.FullReport();
    LOG(INFO) << "Gravity was: " << gravity_constant_;
  }

  // Store the result.
  // 存储优化后结果
  for (size_t trajectory_id = 0; trajectory_id != submap_data_.size();
       ++trajectory_id) {
    for (size_t submap_index = 0;
         submap_index != submap_data_[trajectory_id].size(); ++submap_index) {
      submap_data_[trajectory_id][submap_index].pose =
          C_submaps[trajectory_id][submap_index].ToRigid();
    }
  }
  for (size_t trajectory_id = 0; trajectory_id != node_data_.size();
       ++trajectory_id) {
    for (size_t node_index = 0; node_index != node_data_[trajectory_id].size();
         ++node_index) {
      node_data_[trajectory_id][node_index].point_cloud_pose =
          C_nodes[trajectory_id][node_index].ToRigid();
    }
  }
}

// inter: 返回所有的node数据
const std::vector<std::vector<NodeData>>& OptimizationProblem::node_data()
    const {
  return node_data_;
}
// iter: 返回所有的submap数据//POS
const std::vector<std::vector<SubmapData>>& OptimizationProblem::submap_data()
    const {
  return submap_data_; //优化问题的所有submap的POS
}
}  // namespace sparse_pose_graph
}  // namespace mapping_3d
}  // namespace cartographer


//optimization_problem = { --优化问题参数
//   huber_scale = 1e1,
//   acceleration_weight = 1e4,
//   rotation_weight = 3e6,
//   consecutive_scan_translation_penalty_factor = 1e5,
//   consecutive_scan_rotation_penalty_factor = 1e5,
//   log_solver_summary = false,
//   ceres_solver_options = {
//     use_nonmonotonic_steps = false,
//     max_num_iterations = 50,
//     num_threads = 7,
//   }
