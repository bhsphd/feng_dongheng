/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CARTOGRAPHER_MAPPING_3D_SPARSE_POSE_GRAPH_H_
#define CARTOGRAPHER_MAPPING_3D_SPARSE_POSE_GRAPH_H_

#include <deque>
#include <functional>
#include <limits>
#include <map>
#include <unordered_map>
#include <vector>

#include "Eigen/Core"
#include "Eigen/Geometry"
#include "cartographer/common/fixed_ratio_sampler.h"
#include "cartographer/common/mutex.h"
#include "cartographer/common/thread_pool.h"
#include "cartographer/common/time.h"
#include "cartographer/kalman_filter/pose_tracker.h"
#include "cartographer/mapping/sparse_pose_graph.h"
#include "cartographer/mapping/trajectory_connectivity.h"
#include "cartographer/mapping_3d/sparse_pose_graph/constraint_builder.h"
#include "cartographer/mapping_3d/sparse_pose_graph/optimization_problem.h"
#include "cartographer/mapping_3d/submaps.h"
#include "cartographer/sensor/point_cloud.h"
#include "cartographer/transform/rigid_transform.h"
#include "cartographer/transform/transform.h"

namespace cartographer {
namespace mapping_3d {

// Implements the loop closure method called Sparse Pose Adjustment (SPA) from
// Konolige, Kurt, et al. "Efficient sparse pose adjustment for 2d mapping."
// Intelligent Robots and Systems (IROS), 2010 IEEE/RSJ International Conference
// on (pp. 22--29). IEEE, 2010.
//
// It is extended for submapping in 3D:
// Each scan has been matched against one or more submaps (adding a constraint
// for each match), both poses of scans and of submaps are to be optimized.
// All constraints are between a submap i and a scan j. //scan与submap匹配(边)
class SparsePoseGraph : public mapping::SparsePoseGraph {
 public:
  SparsePoseGraph(const mapping::proto::SparsePoseGraphOptions& options,
                  common::ThreadPool* thread_pool);
  ~SparsePoseGraph() override;

  SparsePoseGraph(const SparsePoseGraph&) = delete;
  SparsePoseGraph& operator=(const SparsePoseGraph&) = delete;

  // Adds a new 'range_data_in_tracking' observation at 'time', and a 'pose'
  // that will later be optimized. The 'pose' was determined by scan matching
  // against the 'matching_submap' and the scan was inserted into the
  // 'insertion_submaps'.
  void AddScan(common::Time time,
               const sensor::RangeData& range_data_in_tracking,
               const transform::Rigid3d& pose,
               const kalman_filter::PoseCovariance& pose_covariance,
               int trajectory_id, const Submap* matching_submap,
               const std::vector<const Submap*>& insertion_submaps)
      EXCLUDES(mutex_);

  // Adds new IMU data to be used in the optimization.
  void AddImuData(int trajectory_id, common::Time time,
                  const Eigen::Vector3d& linear_acceleration,
                  const Eigen::Vector3d& angular_velocity);

  void RunFinalOptimization() override;
  std::vector<std::vector<int>> GetConnectedTrajectories() override;
  std::vector<transform::Rigid3d> GetSubmapTransforms(int trajectory_id)
      EXCLUDES(mutex_) override;
  transform::Rigid3d GetLocalToGlobalTransform(int trajectory_id)
      EXCLUDES(mutex_) override;
  std::vector<std::vector<mapping::TrajectoryNode>> GetTrajectoryNodes()
      override EXCLUDES(mutex_);
  std::vector<Constraint> constraints() override EXCLUDES(mutex_);

 private:
  struct SubmapState {
    const Submap* submap = nullptr;

    // IDs of the scans that were inserted into this map together with
    // constraints for them. They are not to be matched again when this submap
    // becomes 'finished'. // 当前submap中per scan(node)数目,if finish=true,则不再匹配
    std::set<mapping::NodeId> node_ids; //包含的nodeId

    // Whether in the current state of the background thread this submap is
    // finished. When this transitions to true, all scans are tried to match
    // against this submap. Likewise, all new scans are matched against submaps
    // which are finished.
    bool finished = false;
  };

  // Handles a new work item. //inter:处理新的工作item
  void AddWorkItem(std::function<void()> work_item) REQUIRES(mutex_);

  mapping::SubmapId GetSubmapId(const mapping::Submap* submap) const
      REQUIRES(mutex_) {
    const auto iterator = submap_ids_.find(submap); //key是否存在，返回值
    CHECK(iterator != submap_ids_.end());
    return iterator->second;
  }

  // Grows the optimization problem to have an entry for every element of
  // 'insertion_submaps'.
  void GrowSubmapTransformsAsNeeded(
      const std::vector<const Submap*>& insertion_submaps) REQUIRES(mutex_);

  // Adds constraints for a scan, and starts scan matching in the background.
  // inter:后端线程
  void ComputeConstraintsForScan(
      const Submap* matching_submap,
      std::vector<const Submap*> insertion_submaps,
      const Submap* finished_submap, const transform::Rigid3d& pose,
      const kalman_filter::PoseCovariance& covariance) REQUIRES(mutex_);

  // Computes constraints for a scan and submap pair.
  void ComputeConstraint(const mapping::NodeId& node_id,
                         const mapping::SubmapId& submap_id) REQUIRES(mutex_);

  // Adds constraints for older scans whenever a new submap is finished.
  void ComputeConstraintsForOldScans(const Submap* submap) REQUIRES(mutex_);

  // Registers the callback to run the optimization once all constraints have
  // been computed, that will also do all work that queue up in 'scan_queue_'.
  void HandleScanQueue() REQUIRES(mutex_);

  // Waits until we caught up (i.e. nothing is waiting to be scheduled), and
  // all computations have finished.
  // inter:清理工作
  void WaitForAllComputations() EXCLUDES(mutex_);

  // Runs the optimization. Callers have to make sure, that there is only one
  // optimization being run at a time.
  void RunOptimization() EXCLUDES(mutex_);

  // Adds extrapolated transforms, so that there are transforms for all submaps.
  std::vector<transform::Rigid3d> ExtrapolateSubmapTransforms(
      const std::vector<std::vector<sparse_pose_graph::SubmapData>>&
          submap_transforms,
      int trajectory_id) const REQUIRES(mutex_);

  const mapping::proto::SparsePoseGraphOptions options_; //包含优化问题参数
  common::Mutex mutex_;

  // If it exists, further scans must be added to this queue, and will be
  // considered later. //inter：工作队列，存储回调函数
  std::unique_ptr<std::deque<std::function<void()>>> scan_queue_
      GUARDED_BY(mutex_);

  // How our various trajectories are related.
  mapping::TrajectoryConnectivity trajectory_connectivity_ GUARDED_BY(mutex_);

  // We globally localize a fraction of the scans from each trajectory.
  // inter:确定当前是否已经采样
  std::unordered_map<int, std::unique_ptr<common::FixedRatioSampler>>
      global_localization_samplers_ GUARDED_BY(mutex_);

  // Number of scans added since last loop closure.
  int num_scans_since_last_loop_closure_ GUARDED_BY(mutex_) = 0;

  // Whether the optimization has to be run before more data is added.
  bool run_loop_closure_ GUARDED_BY(mutex_) = false;

  // Current optimization problem. //inter:03_
  sparse_pose_graph::OptimizationProblem optimization_problem_; //优化问题描述
  // 构建整个图优化问题
  sparse_pose_graph::ConstraintBuilder constraint_builder_ GUARDED_BY(mutex_);
  std::vector<Constraint> constraints_ GUARDED_BY(mutex_); // 管理图的所有边(约束定义：哪个submap和哪个node，pose，信息矩阵(协方差矩阵的逆))

  // Submaps get assigned an ID and state as soon as they are seen, even
  // before they take part in the background computations.
  std::map<const mapping::Submap*, mapping::SubmapId> submap_ids_ //存储所有的【submap&，submapId】键值对; 若有新的submap则添加一个
      GUARDED_BY(mutex_);
  mapping::NestedVectorsById<SubmapState, mapping::SubmapId> submap_states_ //存储【submapState， submapId】键值对
      GUARDED_BY(mutex_);

  // Connectivity structure of our trajectories by IDs.
  std::vector<std::vector<int>> connected_components_;
  // Trajectory ID to connected component ID.
  std::map<int, size_t> reverse_connected_components_;

  // Data that are currently being shown.
  //
  // Deque to keep references valid for the background computation when adding
  // new data. // inter:POS+点云
  std::deque<mapping::TrajectoryNode::ConstantData> constant_node_data_;

  // inter:trajectory_nodes_也就是NestedVectorsById：管理轨迹id和node_id的逻辑,包括点云的数据
  mapping::NestedVectorsById<mapping::TrajectoryNode, mapping::NodeId>
      trajectory_nodes_ GUARDED_BY(mutex_);
  int num_trajectory_nodes_ GUARDED_BY(mutex_) = 0; //轨迹节点的数目

  // Current submap transforms used for displaying data.
  std::vector<std::vector<sparse_pose_graph::SubmapData>> //pose
      optimized_submap_transforms_ GUARDED_BY(mutex_); //优化后的所有submap的POS
};

}  // namespace mapping_3d
}  // namespace cartographer

#endif  // CARTOGRAPHER_MAPPING_3D_SPARSE_POSE_GRAPH_H_
