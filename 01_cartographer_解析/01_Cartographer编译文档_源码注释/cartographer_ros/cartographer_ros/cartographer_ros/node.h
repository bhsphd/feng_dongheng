/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CARTOGRAPHER_ROS_NODE_H_
#define CARTOGRAPHER_ROS_NODE_H_

#include <memory>
#include <vector>

#include "cartographer/common/mutex.h"
#include "map_builder_bridge.h"
#include "node_options.h"
#include "cartographer_ros_msgs/FinishTrajectory.h"
#include "cartographer_ros_msgs/SubmapEntry.h"
#include "cartographer_ros_msgs/SubmapList.h"
#include "cartographer_ros_msgs/SubmapQuery.h"
#include "cartographer_ros_msgs/TrajectorySubmapList.h"
#include "ros/ros.h"
#include "tf2_ros/transform_broadcaster.h"

namespace cartographer_ros {

//constexpr编译时期常量
// Default topic names; expected to be remapped as needed.// 根据需要remap
constexpr char kLaserScanTopic[] = "scan";   // 2Dlaser Topic //per scan
constexpr char kMultiEchoLaserScanTopic[] = "echoes"; // 2D多Echo Topic(use less)
constexpr char kPointCloud2Topic[] = "points2";
constexpr char kImuTopic[] = "imu";
constexpr char kOdometryTopic[] = "odom";
constexpr char kFinishTrajectoryServiceName[] = "finish_trajectory";
constexpr char kOccupancyGridTopic[] = "map"; //占用网格地图
constexpr char kScanMatchedPointCloudTopic[] = "scan_matched_points2";
constexpr char kSubmapListTopic[] = "submap_list";
constexpr char kSubmapQueryServiceName[] = "submap_query"; //客户端名称,客户端订阅，调用回调函数

// Wires up ROS topics to SLAM. //ROS与SLAMTopic间连接 --发布3个topic
// (1)发布点云--尚未发送（在其他地方）
// (2)occupancy_grid_publisher_: 句柄定时发送--根据接收的速度
// (3)submap_list_publisher_：定时发送
// (4) 另外：发送轨迹状态
class Node {
 public:
  Node(const NodeOptions& options, tf2_ros::Buffer* tf_buffer); //构造函数：参数+tf2_ros::Buffer
  ~Node();

  Node(const Node&) = delete;
  Node& operator=(const Node&) = delete;

  // 初始化节点（发布各种topic）
  void Initialize();

  ::ros::NodeHandle* node_handle();
  MapBuilderBridge* map_builder_bridge();


//  // --TODO修改源码
//  // 发布原始点云
//  ::ros::Publisher getScan_matched_publisher(){
//    return &scan_matched_point_cloud_publisher_;
//  }

 private:
  bool HandleSubmapQuery(
      cartographer_ros_msgs::SubmapQuery::Request& request,
      cartographer_ros_msgs::SubmapQuery::Response& response);

  void PublishSubmapList(const ::ros::WallTimerEvent& timer_event);
  void PublishTrajectoryStates(const ::ros::WallTimerEvent& timer_event);
  void SpinOccupancyGridThreadForever();

  const NodeOptions options_;

  tf2_ros::TransformBroadcaster tf_broadcaster_; //广播坐标系变换

  cartographer::common::Mutex mutex_;
  MapBuilderBridge map_builder_bridge_ GUARDED_BY(mutex_); //

  int trajectory_id_ = -1;
  std::unordered_set<string> expected_sensor_ids_;

  ::ros::NodeHandle node_handle_; //句柄
  ::ros::Publisher submap_list_publisher_; //All_submap——publisher
  ::ros::ServiceServer submap_query_server_; //服务

  //—————————————————————————向话题发送点云--此处并没有————————————————————————————————//
  ::ros::Publisher scan_matched_point_cloud_publisher_; //组合点云--publisher-
  //————————————————————————————————————————————————————————————————————//

  cartographer::common::Time last_scan_matched_point_cloud_time_ =
      cartographer::common::Time::min(); //测量时间

  ::ros::Publisher occupancy_grid_publisher_; //占用网格--publisher
  std::thread occupancy_grid_thread_;
  bool terminating_ = false GUARDED_BY(mutex_);

  // We have to keep the timer handles of ::ros::WallTimers around, otherwise
  // they do not fire. // 必须保留定时器handle，否则不会触发回调函数
  std::vector<::ros::WallTimer> wall_timers_;
};

}  // namespace cartographer_ros

#endif  // CARTOGRAPHER_ROS_NODE_H_
