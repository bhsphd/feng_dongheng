# README #
### 一、文件夹 01_cartographer_解析：
该目录下存在以下四个文件夹
* [01_Cartographer编译文档_源码注释](feature) Google-cartographer及ROS的源码 编译文档；Google-cartographer及ROS的源码注释。
* [02_Cartographer相关论文](demo) Cartographer相关论文。
* [03_源码解析文档(更新)](quick-start) 源码解析文档:Cartographer的源码解析文档，此文件夹将不断更新。
* [04_汇报](manual) 阶段性汇报整理放在这个文件夹。

### 二、文件夹 02_ROS(更新)
该目录下存在以下四个文件夹
* [01_开发环境搭建_qt](feature) 包含ros+qt插件的编译安装文档及相应的setup.sh。
* [02_简单程序测试](demo) 包括ROS简单的测试程序；ROS文档
* [03_ROS基本概念和系统架构--待整理](quick-start) 未来将把所有的ROS基本概念和系统架构整理，放进去

### 三、文件夹 最优化理论
* 相关内容已经整理完毕，待整理到该文件夹
### 四、文件夹 04_相关库_Ceres_Eigen_g2o_VTK_PCL_OpenCV（更新）
* 涉及 Ceres Eigen G2O VTK PCL OpenCV
* 源码+编译文档+测试程序+总结(部分：待更新)
### 五、文件夹 05_cuda_JetsonTX1(汇报)
* 陈焕剑同学负责，建议更新备份到这里
### 六、文件夹 06_cmake_文档_示例代码--更新
* cmake学习教程
*涉及CMakeLists文件编写，构建工程，编译源码等等。

### 七、SLAM学习资料整理
* 相关SLAM学习资料，包括书籍等电子版整理到这里
### 八、论文
* 论文正稿
* 相关程序
### 九、VsursPos源码及说明文档
### 十二、毕业论文
* 论文正文
* 答辩PPT
* 程序及说明
* 参考文献
* 论文流程图与图片